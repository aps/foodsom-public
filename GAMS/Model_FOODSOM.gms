*FOODSOM: Food System Optimisation Model
*Created by Ben van Selm and Thomas Maindl
*Contact: ben.vanselm@wur.nl

$onsymxref
$eolcom //

** Model iteration controls
*Set maximum number of iterations
$setglobal niter 10

*Set fixed distance or distance besed on regions.
*$Setglobal FixedDistance                 "On";

*Set a fixed grazing proportion 
*$Setglobal FixedGrazingProportion        "On";


SET
* Country and region sets and combinations
Country                                                                          "Countries"
Region                                                                           "Regions"
CR                               (Country,Region)                                "Two dimensional set linking countries to regions"
RegionNeighbour                  (Country,Region,Country,Region)                 "Four dimensional set linking neigbouring countries and regions"
CRCRA                            (Country,Region,Country,Region)                 "Four dimensional set linking identical country/region and country/region combinations"

* Product list sets and combinations
AllProd                                                                          "Product list containing all products in the model"
ProcIn                           (AllProd)                                       "Product list containing unprocessed or raw products"
Process                          (AllProd)                                       "Procuct list containing different processes"
ProcOut                          (AllProd)                                       "Product list containing processed products"
FertProd                         (AllProd)                                       "Product list containing all products applied to soil/crop as fertiliser"
FeedProd                         (AllProd)                                       "Product list containing all products fed to animals"
APWaste                          (AllProd)                                       "Product list containing all food loss products that can be fed to animals or composted"
AllWaste                         (AllProd)                                       "Product list containing all food loss and waste products that can be fed to animals or composted"
Waste                            (AllProd)                                       "Product list containing all foo waste products that can be fed to animals or composted"
Compost                          (AllProd)                                       "Product list containing all compost products"
ProcOutH1                        (AllProd)                                       "Product list containing all processed all food products that can be consumed by humans, subset of AllProd"
Crop                             (ProcIn)                                        "Product list containing all crops"
GrassCrop                        (Crop)                                          "Product list containing all grass crops"
NoGrassCrop                      (Crop)                                          "Product list of all crops except grass crops"
MeatMilkEggs                     (ProcIn)                                        "Product list containing unprocessed or raw animal products"
FishSpecies                      (ProcIn)                                        "Product list containing unporcessed or raw marine fish products"
ProcInProcess                    (ProcIn,Process)                                "Two dimensional set linking unprocessed or raw products to different processing options"
ProcOutH                         (ProcOut)                                       "Product list containing all processed all food products that can be consumed by humans, subset of ProcOut"
ProcOutHPSF                      (ProcOutH)                                      "Product list containing all processed plant products that can be consumed by humans"
ProcOutHASF                      (ProcOutH)                                      "Product list containing all processed animal products that can be consumed by humans"
ProcOutCP                        (ProcOut)                                       "Product list containing all co-products that can be fed to animals or composted "
ProcOutFF                        (ProcOut)                                       "Product list containing all feed-food crops that can be fed to animals"
ProcOutG                         (ProcOut)                                       "Product list containing all grass products (e.g., fresh grass, silage) that can be fed to animals"
ProcOutAA                        (AllProd)                                       "Product list containing all synthetic amino acids that can be fed to animals"
FreshGrass                       (ProcOut)                                       "Product list containg all fresh grass products that can be fed to animals"

* Land and soil sets and combinations
LandType                                                                         "Types of land"
SoilType                                                                         "Types of Soil"
LandSoil                         (LandType,SoilType)                             "Two dimensional set linking land types to soil types"
LandSoilCrop                     (LandType,SoilType,Crop)                        "Three dimensional set linking land types to soil types to crops"
LandCrop                         (LandType,Crop)                                 "Three dimensional set linking land types to crops"
CRCrop                           (Country,Region,ProcIn)                         "Three dinemsional set linking countries and regions to crops"
CRLandSoil                       (Country,Region,LandType,SoilType)              "Four dimensional set linking countries and regions to land types and soil types"
RotFam                                                                           "List of crop rotation families"
CropRotFam                       (Crop,*,RotFam)                                 "Three dimensional set linking crops, crop type (e.g., permanent or temporary), and rotation family"
Derogation                                                                       "Derogation included or excluded"

* Category sets and combinations
AllCat                                                                           "Category list containing all categories in the model"
FeedCat                          (AllCat)                                        "Category list containing all feed categories"
FertCat                          (AllCat)                                        "Category list containing fertiliser categories"
WasteCat                         (AllCat)                                        "Category list containing all waste categories"
FoodCat                          (AllCat)                                        "Category list containing all food categories"
FeedCatAllProd                   (AllCat,AllProd)                                "Two dimensional set linking feed categories to feed products"
WasteTypeCatProd                 (AllCat,AllProd,AllProd)                        "Three dimensional set linking waste categories and waste (feed) products to food products (mixes food into food loss and waste products)"
CompostTypeCatProd               (AllCat,AllProd,AllProd)                        "Three dimensional set linking waste categories and compost products to food products (mixes food loss and waste products into compost)"
FWCatWaste                       (AllCat,AllProd)                                "Two dimensional set linking food waste categories to food waste products"
FLCatAPWaste                     (AllCat,AllProd)                                "Two dimensional set linking food loss categories to food loss products"
FertCatAllProd                   (AllCat,AllProd)                                "Two dimensional set linking fertiliser categories to fertiliser products"
AllNutr                                                                          "Nutrient list containing all nutrients"
AniNutr                          (AllNutr)                                       "Nutrient list containing all animal related nutrients"
AniPE                            (AniNutr)                                       "Nutrient list containing all animal related protein and energy nutrients"
FertNutr                         (AllNutr)                                       "Nutrient list containing all fertiliser related nutrients"
HumNutr                          (AllNutr)                                       "Nutrient list containing all human related nutrients"
HumNutrR                         (HumNutr)                                       "Nutrient list containing all human related nutrients with a requirement "

* Animal sets and cominations
APS                                                                              "Animal production system"
Level                                                                            "Animal productivity level"
HerdType                                                                         "Animal herd types"
APSHerdType                      (APS,Level,HerdType)                            "Three dimensional set linking animal production system to productivity level and herd type"
APSLevel                         (APS,Level)                                     "Two dimensional set linking animal production system to productivity level "

* Other sets 
MinMax                                                                           "Minimum and maximum"
Gender                                                                           "Human gender: male or female"
Age                                                                              "Human age categories"
Month                                                                            "Months in the year"
Fam                                                                              "Family list containing all families in the model"
FamAllProd                       (FoodCat,Fam,AllProd)                           "Three dimensional set linking all fodd categories to food families and ffod products"
FamProcOutH                      (Fam,AllProd)                                   "Two dimensional set linking all food families to food products"
ImportExportProdFam              (*,Fam,AllProd)                                 "Three dimensional set linking ProcIn/ProcOutH products to families and products"
ProdTot                                                                          "Product or total intake of food for humans"
GHG                                                                              "GHG emission list (e.g., CH4)"
ConFRV                                                                           "Nutrient content and fertiliser replacement value"
ImportExport                                                                     "Import or export"
EnIm                                                                             "Environmental Impacts" /LU,GHG/
NewOld                                                                           "New or Old values"
Scenario                                                                         "Scenario list containing scenarios to be run"
Controls                                                                         "Scenario control list"
;
ALIAS
(Country,CountryA) 
(Region,RegionA)
(CR,CRA)
(Crop,CropA)
(RotFam,RotFamA)
(HerdType,HerdTypeA)
(Month,MonthA)
;
PARAMETER
* Import parameters used for different scenarios
ImportExportSuitability          (*,ImportExport,*,AllProd)                      "Include or exclude which products can be imported and exported, 1=yes or 0 =no"
ImportHumNutrReq                 (*,Country,Gender,Age,MinMax,HumNutrR)          "Human nutrient requirments based on different diets, various units per capita day"
ImportHumIntakeReq               (*,Country,ProdTot,MinMax,Fam)                  "Human intake constraints based on different diets, grasm per cpaita per day"
ImportAniFeedSuitability         (*,AllCat,AllProd,APS)                          "Include or exclude different feed ingredients per APS, 1= yes or 0= no"
ImportFRVByProduct               (AllCat,AllProd,FertNutr)                       "Fertiliser replacement values per fertiliser product, (%)"
ImportFertSuitability            (*,Crop,AllCat)                                 "Include or exclude different fertiliser categories per crop, 1= yes 0= no"
ImportAniData                    (APS,Level,HerdType,*)                          "Animal data e.g., nutrient requirements, herd types per production unit, various units"
ImportImportExport               (*,*,Country,Region,AllProd,ImportExport)       "Import and export of raw materials in Mg"
ImportCropYieldha                (Country,Region,LandType,SoilType,Crop)         "Crop yields imported Mg ha during pre solve"
ImportCropFertManureha           (Country,Region,SoilType,Derogation,NewOld)     "Maximum manure fertilisation in kg ha converted to Mg ha during pre solve"

* Land, crop and fertilisation related parameters
LandAreaha                       (Country,Region,LandType,SoilType)              "Land area available per land type and soil type, ha"
CropYieldha                      (Country,Region,LandType,SoilType,Crop)         "Crop yield per land type, soil type and crop, initially in kg ha but converted to Mg per ha per year at presolve"
CropFertha                       (Country,Region,LandType,SoilType,Crop,FertNutr)"Crop fertilisation requirements per land type, soil type and crop, initially in kg ha but converted to Mg per ha per year at presolve"
CropFertNReferenceha             (Country,Region,LandType,SoilType,Crop,FertNutr)"Crop fertilisation requirements per land type, soil type and crop, initially in kg ha but converted to Mg per ha per year at presolve" 
CropFertManureha                 (Country,Region,SoilType,*)                     "Maximum manure application per soil type and derogation (included or excluded), Mg per hectare per year"
CropSuitability                  (Country,Region,LandType,SoilType,ProcIn)       "$ condition for combination of country, region, land type, soil type and crop, 1= yes or 0= no"
CropFertSuitability              (Crop,AllCat)                                   "$ condition for application of fertiliser category (e.g., compost) to crops, 1= yes or 0= no"
CropOther                        (Crop,*)                                        "Other crop parameters (e.g., rotation, fuel use), various units per ha crop per year"
CropRotation                     (RotFam)                                        "Minimum frequency a crop can appear in a crop rotation, years"
CropReferenceha                  (Country,Region,Crop)                           "Area of crop in the reference scenario, ha per year"
ReferenceGrassAreaha             (Country)                                       "Area of grassland in the reference scenario, he per year"
CropHarvestedNha                 (Country,Region,LandType,SoilType,Crop,FertNutr)"Harvested nitrogen in crop yield and residues, Mg per ha per year"
FRVByProduct                     (AllCat,AllProd,FertNutr)                       "Mineral fertiliser replacement values (FRV) of all fertiliser products, (%)"
FRVReference                     (AllCat,AllProd,*,*)                            "Mineral fertiliser replacement values of fertiliser products for the reference scenraio, (%)"

* Fish related parameters
MarineFish                       (Country,Region,ProcIn)                         "Marine fish captured from the sea, Mg per year"

* Other parameters
Processing                       (AllProd,AllProd)                               "Processing fractions of ProcOut products relative to process products, ratio"
FertNutrByProductMgMg            (AllCat,AllProd,AllNutr)                        "Nutrient content (FertNutr) of all products in the model, Mg per Mg"
FoodLossWasteFrac                (FoodCat,AllCat,AllProd)                        "Food loss & waste fraction, %"
TransDistkm                      (Country,Region,CountryA,RegionA)               "Transport distance between countries and regions, km"

* Human related parameters
HumNutrReq                       (Country,Gender,Age,MinMax,HumNutrR)            "Nutrient requiremenst of humans per gender and age group, various units per capita per day"
HumIntakeReq                     (Country,ProdTot,MinMax,Fam)                    "Human intake constraints, Mg per capita per day"
HumNutrByProduct                 (ProcOutH,AllNutr)                              "Nutrients in food products, various units per 100g"
HumFoodMonth                     (ProcOutH,Month)                                "$ condition to determine if food is available for a given month, 1= yes or 0= no"
Population                       (Country,Region,Gender,Age)                     "Population by region and age"

* Animal related parameters
AniNutrByProduct                 (AllCat,AllProd,AllNutr)                        "Nutrients in animal feed various units kg per Mg or (%)"
AniNutrReq                       (APS,Level,HerdType,AniNutr)                    "Animal nutrient requirements, various units per animal place per year"
AniOther                         (APS,*)                                         "Other animal parameters, various units"
AniFeedSuitability               (APS,AllCat,AllProd)                            "$ condition for feed suitability per APS, 1= yes or 0= no"
AniHerdTypePerPU                 (APS,Level,HerdType)                            "Herd types per producer unit"
AniYield                         (APS,Level,HerdType,MeatMilkEggs)               "Animal yield of meat, milk, and eggs, initially in kg ha but converted to Mg per ha per year at presolve"
AniRetained                      (APS,Level,HerdType,MeatMilkEggs)               "Growth and yield of milk and eggs, initially in kg ha but converted to Mg per ha per year at presolve"
AniEnergy                        (APS,level,HerdType,*)                          "Energy use per APS level and herdtype, kwh per animal place per year"
AniFeedLossFrac                  (AllCat,AllCat,AllProd)                         "Rearranged feed loss fractions, (%)"
AniNutrRetMg                     (APS,Level,HerdType,FertNutr)                   "Fertiliser related nutrients (NPK) retained in growth, milk and eggs, Mg per animal place per year"
AniReference                     (Country,Region,APS)                            "Reference animal numbers per country and region"
AniNutrByProductCR               (Country,Region,AllCat,AllProd,AllNutr)         "Animal related nutrients in distribution and consumption food waste per region, various units per Mg"
AniFeedProcEF                    (AllCat,AllProd)                                "Emissions from processing feed items, Mg CO2eq per Mg feed"
GrazingProportionCR              (Country,Region,APS,Level,HerdType)             "Grazing proportion, (%)"

* GHG emissions related parameters
EFH                              (Country,HerdType,*)                            "Emission factors for housing, (%)"
EFS                              (Country,LandType,SoilType,ALlCat,*)            "Emission factors for soils and crops, (%)"
EFC                              (Country,Compost,*)                             "Emission factors for compost, (%)"
GHGOther                         (GHG,*)                                         "GHG emission conversion factors and transport emissions, various units"
NMineralha                       (Country,SoilType)                              "Annual mineralised N from peat soils, initially in kg ha but converted to Mg per ha per year at presolve"
NDepositionha                    (Country,Region)                                "Nitrogen deposition per country and region, initially in kg ha but converted to Mg per ha per year at presolve"
NPKRatioCR                       (Country,Region,AllCat,AllProd,FertNutr)        "Ratio of NPK in different fertiliser products, (%)"
ManureTanCR                      (Country,Region,AllCat,AllProd)                 "TAN excretion in animal manure, (%)"

* Impport and export related parameters
ProcInImportExportMg             (Country,Region,ImportExport,ProcIn)            "Unprocessed or raw materials imported and exported per country and region, Mg per year"
ProcOutHImportExportMg           (Country,Region,ImportExport,ProcOutH)          "Processed products imported and exported per country and region, Mg per year"
ProcInImportExportSuitability    (Country,Region,ImportExport,ProcIn)            "Suitability to determine which unprocessed or raw materials can be imported or exported, 1=yes, 0=no"
ProcOutHImportExportSuitability  (Country,Region,ImportExport,ProcOutH)          "Suitability to determine which processed products can be imported or exported, 1=yes, 0=no"

* Other parameters
ReferenceNExcretion              (Country)                                       "Reference nitrogen intake for animals in the reference scenarion, Mg per year"
WeightedAverage                  (AllCat,AllProd)                                "Weighted average parameter (dummy value)"
ScenarioControls                 (Scenario,Controls)                             "Scenario controls of all scenarios"
ScenarioControlsModel            (Controls)                                      "Scenario controls of scenario being run"
AniAminoAcidLU                   (AllProd)                                       "LU afrom amino acid production, ha per Mg"
ReferenceManure                  (FertNutr)                                      "Percentage of fertiliser nutrients in manure applied to the soil for reference scenario, (%)"
ImportBoundary                   (Country,Region,CountryA,RegionA,AllCat)        "Control which products can be imported and exported between regions"               
;
$GDXIN GAMS_Data_FOODSOM.gdx
$LOADDCM AllNutr=AniNutr AllNutr=HumNutr AllNutr=FertNutr
$LOADDCM AllCat=FertCat AllCat=FeedCat AllCat=FoodCat AllCat=WasteCat AllCat=FertCat
$LOADDC Country Region CR RegionNeighbour AllProd ProcIn Process Crop MeatMilkEggs FishSpecies ProcInProcess ProcOut ProcOutH SoilType LandType LandSoil LandSoilCrop LandCrop RotFam CropRotFam 
$LOADDC FeedCat FeedProd WasteTypeCatProd CompostTypeCatProd Waste Compost FeedCatAllProd FoodCat WasteCat FertNutr AniNutr HumNutr HumNutrR APS Level HerdType APSHerdType APSLevel MinMax FertCat
$LOADDC Age Gender Month Fam ProdTot FamAllProd ImportExportProdFam GHG ImportExport Scenario Controls NewOld Derogation

$LOADDC HumNutrByProduct AniNutrByProduct ImportHumNutrReq ImportAniData LandAreaha ImportCropYieldha CropFertNReferenceha CropOther CropRotation CropReferenceha CropSuitability MarineFish Processing ImportHumIntakeReq Population
$LOADDC HumFoodMonth  AniYield AniRetained AniOther AniEnergy AniReference FoodLossWasteFrac AniFeedLossFrac ImportAniFeedSuitability EFH EFC GHGOther AniAminoAcidLU AniFeedProcEF ImportFertSuitability ScenarioControls NDepositionha
$LOADDC ImportExportSuitability TransDistkm ImportImportExport EFS FertProd FertNutrByProductMgMg ImportFRVByProduct FRVReference ImportCropFertManureha
$GDXIN
;

SET
Rumi                             (APS)                                           "Ruminant production systems"                                    /Dairy,Beef/
Mono                             (APS)                                           "Mongastric producton systems"                                   /Pig,Broiler,"Laying_Hen"/
APSManure                        (APS)                                           "APS with manure management system"                              /Dairy,Beef,Pig,Broiler,"Laying_Hen"/
APSManureProd                    (AllProd,APS)                                   "Two dimensional set linking APS with manure products"           /Dairy_Manure.Dairy,Beef_Manure.Beef,Broiler_Manure.Broiler,Layer_Manure."Laying_Hen",Pig_Manure.Pig/
AniPE                            (AniNutr)                                       "Protein and energy nutrients related to animals"                /NEP,DVLysP,DVMetP,MEL,DVLysL,DVMetL,MEBr,DVLysBr,DVMetBr,VEM,VEVI,DVE,OEB/
ManureProd                       (AllProd)                                       "Product list containing all animal manure products"             /Pig_Manure,Dairy_Manure,Beef_Manure,Broiler_Manure,Layer_Manure/
Producer                         (APS,HerdType)                                  "Two dimensional set linking APS to HerdType producers"          /Pig.PProducer,Dairy.DProducer,Beef.BProducer,Broiler.BrProducer,"Laying_Hen".LProducer/
FWCat                            (AllCat)                                        "Category list containing food waste categories"                 /DLoss,ConLoss/
FLCat                            (AllCat)                                        "Category list containing food loss categories"                  /PHLoss,PPLoss/
FertProdCat                      (AllCat)                                        "Category list containing fertiliser product categories"         /Residue,Artificial/
FertOthCat                       (AllCat)                                        "Category list containing grazing manure and other categories"   /FLoss,GrazingManure/
FertManCat                       (AllCat)                                        "Category list containing grazing manure and other categories"   /Manure,HumanExcreta,Compost/
FertNutrNP                       (FertNutr)                                      "Nutrient list containing in N & P nutrients only"               /N,P/
FertNutrPK                       (FertNutr)                                      "Nutrient list containing P & K nutrients only"                  /P,K/
ResidueN                                                                         "Above and below ground residue N"                               /AGN,BGN/
ConFRV                                                                           "NPK content or FRV"                                             /Content,FRV/
DerogationCrop                   (Crop)                                          "Crop list containing crops eligible for derogation"             /Grass_Perm_Grazing,Grass_Perm_Mowing,Maize_Feed_Silage/
RegionIE                         (Region)                                        "List of boarder regions that can import and export"             /NL11,NL13,NL21,NL33,NL41,NL42/
OilFeed                          (AllProd)                                       "Product list containing oil by-products"                        /Beef_Tallow,Cod_Oil,Flatfish_Oil,Herring_Oil,Mackeral_Oil,Pork_Lard/
GrassCrop                        (Crop)                                                                                                           /Grass_Perm_Mowing,Grass_Perm_Grazing/
;
SINGLETON SET
FeedLoss                         (FertOthCat)                                    "Feeding losses"                                                 /FLoss/
FeedWaste                        (AllProd)                                       "Feding waste category"                                          /Feedwaste/
DWaste                           (WasteCat)                                      "Distribution waste category"                                    /DLoss/
CWaste                           (WasteCat)                                      "Consumption waste category"                                     /ConLoss/
PHLoss                           (WasteCat)                                      "Post harvest loss category"                                     /PHLoss/
PPLoss                           (WasteCat)                                      "Processing a& packaging loss category"                          /PPLoss/

; //Add additional sets
ProcOutFF                        (ProcOut)$FeedCatAllProd('FoodFeed',ProcOut)                                                                                              = yes;
ProcOutCP                        (ProcOut)$FeedCatAllProd('CoProduct',ProcOut)                                                                                             = yes; 
ProcOutG                         (Procout)$(FeedCatAllProd('GrassFresh',ProcOut) or FeedCatAllProd('GrassCon',ProcOut))                                                    = yes;
ProcOutAA                        (AllProd)$FeedCatAllProd('AminoAcid',AllProd)                                                                                             = yes;
FamProcOutH                      (Fam,ProcOutH)$FamAllProd('ProcOutH',Fam,ProcOutH)                                                                                        = yes;
FreshGrass                       (ProcOut)$FeedCatAllProd('GrassFresh',ProcOut)                                                                                            = yes;
NoGrassCrop                      (Crop)                                                                                                                                    = Crop(Crop) - GrassCrop(Crop);
ProcOutHASF                      (ProcOutH)$(FamProcOutH('Dairy',ProcOutH) Or FamProcOutH('Meat',ProcOutH) Or FamProcOutH('Fish',ProcOutH) Or FamProcOutH('Egg',ProcOutH)) = yes;
ProcOutHPSF                      (ProcOutH)                                                                                                                                = ProcOutH(ProcOutH) - ProcOutHASF(ProcOutH);
ProcOutH1                        (ProcOut)                                                                                                                                 = ProcOutH(ProcOut);
APWaste                          (AllProd)                                                                                                                                 = ProcOutH1(AllProd) + ProcIn(AllProd);
ProcOutHPSF                      (ProcOutH)                                                                                                                                = ProcOutH(ProcOutH) - ProcOutHASF(ProcOutH);
AllWaste                         (AllProd)                                                                                                                                 = APWaste(AllProd) + Waste(AllProd);
FWCatWaste                       (FWCat,Waste)$SUM(Compost, CompostTypeCatProd(FWCat,Waste,Compost))                                                                       = yes;
FLCatAPWaste                     (FLCat,APWaste)$SUM(Compost, CompostTypeCatProd(FLCat,APWaste,Compost))                                                                   = yes;
FertCatAllProd                   (AllCat,AllProd)$SUM(FertNutr, ImportFRVByProduct(AllCat,AllProd,FertNutr))                                                               = yes;
CRCRA(CR,CR)                                                                                                                                                               = CR(CR) + CR(CR);
; //Scale Parameters from kg to Mg
CropOther                        (Crop,'NFix')                                                           = CropOther(Crop,'NFix') * 0.001;
CropOther                        (Crop,ResidueN)                                                         = CropOther(Crop,ResidueN) * 0.001;
CropOther                        (Crop,'Diesel')                                                         = CropOther(Crop,'Diesel') * 0.001;
CropOther                        (Crop,'GlassCO2')                                                       = CropOther(Crop,'GlassCO2') * 0.001;
NDepositionha                    (CR)                                                                    = NDepositionha(CR) * 0.001;
NMineralha                       ('NL','Peat')                                                           = 233.5 * 0.001;
AniYield                         (APSHerdType,MeatMilkEggs)                                              = AniYield(APSHerdType,MeatMilkEggs) * 0.001;
AniRetained                      (APSHerdType,MeatMilkEggs)                                              = AniRetained(APSHerdType,MeatMilkEggs) * 0.001;
AniNutrReq                       (APSHerdType,AniNutr)                                                   = ImportAniData(APSHerdType,AniNutr) * 0.001;
GHGOther                         (GHG,'Road_Trans')                                                      = GHGOther(GHG,'Road_Trans') * 0.001;
GHGOther                         (GHG,'Kwh')                                                             = GHGOther(GHG,'Kwh') * 0.001;
; //Split import Parameters
ReferenceNExcretion              ('NL')                                                                  = 503400;//Sets the minimum N excretion for livestock to ensure reference lines up with statistics
ReferenceGrassAreaha             ('NL')                                                                  = 645000;//Originally 968357 but reduced to account for the fact we don't include goats, sheep and horses
AniNutrRetMg                     (APSHerdType,FertNutr)                                                  = SUM((MeatMilkEggs), AniRetained(APSHerdType,MeatMilkEggs) * FertNutrByProductMgMg('PHLoss',MeatMilkEggs,FertNutr));
CropFertha                       (CR,LandSoilCrop,'N')$CropSuitability(CR,LandSoilCrop)                  = CropFertNReferenceha(CR,LandSoilCrop,'N') * 0.001;
CropFertha                       (CR,LandSoil,Crop,FertNutrPK)$CropSuitability(CR,LandSoil,Crop)         = ((ImportCropYieldha(CR,LandSoil,Crop) * FertNutrByProductMgMg('PHLoss',Crop,FertNutrPK)) +  (ImportCropYieldha(CR,LandSoil,Crop) * CropOther(Crop,'Residue') * FertNutrByProductMgMg('Residue',Crop,FertNutrPK))) * 1.125;
CropHarvestedNha                 (CR,LandSoil,Crop,'N')$CropSuitability(CR,LandSoil,Crop)                = ((ImportCropYieldha(CR,LandSoil,Crop) * FertNutrByProductMgMg('PHLoss',Crop,'N'))        +  (ImportCropYieldha(CR,LandSoil,Crop) * CropOther(Crop,'Residue') * FertNutrByProductMgMg('Residue',Crop,'N')));
AniHerdTypePerPU                 (APSHerdType)                                                           = ImportAniData(APSHerdType,'HerdTypesPerPu');
CropSuitability                  (CR,LandSoil,Crop)                                                      = CropSuitability(CR,LandSoil,Crop)$(LandAreaha(CR,LandSoil) AND ImportCropYieldha(CR,LandSoil,Crop)); // This ensures crops are only suitable if the land area and crop yield exists in the country/region.
CRCrop                           (CR,Crop)                                                               = SUM((LandSoil), CropSuitability(CR,LandSoil,Crop));
CRLandSoil                       (CR,LandSoil)                                                           = SUM((Crop), CropSuitability(CR,LandSoil,Crop));
;
POSITIVE VARIABLES //Units are all Mg FM unless otherwise stated

* Land, crop and fertilisation related variables
vNumha                           (Country,Region,LandType,SoilType,ProcIn)                                  "Number of hectares per country, region, land type, soil type and crop (ProcIn) in ha per year"
vLandUseRegionha                 (Country,Region)                                                           "Total land use per country and region in ha per year"
vCropFertMg                      (Country,Region,LandType,SoilType,Crop,FertNutr)                           "Crop fertilisation per country, region, land type, soil type, crop and nutrient in Mg per year"
vCropFertManureMg                (Country,Region,LandType,SoilType,Crop,FertNutr)                           "Crop fertilisation with animal manure per country, region, land type, soil type and crop in Mg per year"
vCropFertNMg                     (Country,Region,LandType,SoilType,Crop)                                    "Nitrogen fertilisation requirement per country, region, land type, soil type, and crop in Mg per year"
vFertProdMg                      (Country,Region,LandType,SoilType,Crop,AllCat,AllProd)                     "Product category fertilisation per country, region, land type, soil type, and crop in Mg per year"
vFertOtherNPKMg                  (Country,Region,LandType,SoilType,Crop,AllCat,AllProd)                     "Other category fertilisation per country, region, land type, soil type, crop, other fertilier category and fertiliser product in Mg NPK per year"
vFertManureCompostNPKMg          (Country,Region,CountryA,RegionA,LandType,SoilType,Crop,AllCat,AllProd)    "Manure and compost category fertilisation per country, region, land type, soil type, crop, manure and compost fertilisation category and fertilisaer product in Mg NPK per year"
vCropProdMg                      (Country,Region,ProcIn)                                                    "Quantity of crop grown in per country, region and crop in Mg per year"

* Animal related variables
vNumHerdType                     (Country,Region,APS,Level,HerdType)                                        "Livestock numbers per country, region, animal production system, productivity level and HerdType"
vAniFoodWasteIntakeHerdTypeMgDM  (Country,Region,CountryA,RegionA,APS,Level,HerdType,AllCat,AllProd)        "Intake of food waste products per country, region, animal production system, productivity level, HerdType, food waste category and food waste product in Mg DM per year"
vAniFeedIntakeHerdTypeMgDM       (Country,Region,APS,Level,HerdType,AllCat,AllProd)                         "Intake of all feed products (except food waste) per country, region, animal production system, productivity level, HerdType, feed category and feed product in Mg DM per year"
vAminoAcidMg                     (Country,Region,AllCat,AllProd)                                            "Quantity of synthetic amino acids consumed by livestock per country, region, feed category and amino acid in Mg per year"
vAniNutrIntakeMg                 (Country,Region,APS,Level,HerdType,FertNutr)                               "Quantity of nutrients consumed by livestock per country, region, animal productiuon system, productivity level, HerdType and fertiliser nutrient in Mg per year"
vAniNutrRetMg                    (Country,Region,APS,Level,HerdType,FertNutr)                               "Quantity of nutrients retained in livestock per country, region, animal productiuon system, productivity level, HerdType and fertiliser nutrient in Mg per year"
vAniNutrExcMg                    (Country,Region,APS,Level,HerdType,FertNutr)                               "Quantity of nutrients excreted by livestock per country, region, animal productiuon system, productivity level, HerdType and fertiliser nutrient in Mg per year"
vAniManureMMMg                   (Country,Region,APS,Level,HerdType,FertNutr)                               "Quantity of nutrients excreted by livestock in a manure management system per country, region, animal productiuon system, productivity level, HerdType and fertiliser nutrient in Mg per year"
vAniManureGMg                    (Country,Region,Rumi,FertNutr)                                             "Quantity of nutrients excreted by livestock while grazing per country, region, animal productiuon system, productivity level, HerdType and fertiliser nutrient in Mg per year"
vMeatMilkEggsMg                  (Country,Region,AllProd)                                                   "Quantity of meat, milk and eggs produced per country, region, and livestock product in Mg per year"

* Product processing related variables
vProcessMg                       (Country,Region,AllProd)                                                   "Quantity of product for each process per country, region and process in Mg per year"
vProcOutMg                       (Country,Region,ProcOut)                                                   "Quantity of processed procuct per country, region and product in Mg per year"
vProcOutHMg                      (Country,Region,ProcOutH)                                                  "Quantity of processed products for human consumption per country, region and human food product in Mg per year"

* Product availability related variables
vMarineFishMg                    (Country,Region,ProcIn)                                                    "Quantity of marine fish per country, region and fish species (ProcIn) in Mg per year"
vProcoutHMonthMg                 (Country,Region,ProcOutH,Month)                                            "Quantity of products consumed by humans monthly per country, region, food product and month in Mg per month"
vFoodWasteMg                     (Country,Region,AllCat,AllProd)                                            "Quantity of unmixed food waste products (e.g., flour) available per country, region, food waste category and food product in Mg per year"
vFoodLossMg                      (Country,Region,AllCat,AllProd)                                            "Quantity of food loss products available per country, region, food loss category and food loss product in Mg per year"

* Compost related variables
vCompostFoodWasteMg              (Country,Region,AllCat,AllProd)                                            "Quantity of food waste products composted per country, region, food waste category, and food waste product in Mg per year"
vCompostFoodLossMg               (Country,Region,AllCat,AllProd)                                            "Quantity of food loss products composted per country, region, food waste category, and food waste product in Mg per year"
vCompostCoProductMg              (Country,Region,AllCat,AllProd)                                            "Quantity of by-products products composted per country, region, category, and co-product in Mg per year"

* Internal import and export (between regions) related variables
vProcInRegionImportExportMg      (Country,Region,CountryA,RegionA,ImportExport,ProcIn)                      "Quantity of national ProcIn import product imported and exported regionally per export country, export region, import country, import region, ImportExport, and procin product in Mg per year"
vProcOutRegionImportExportMg     (Country,Region,CountryA,RegionA,ProcOut)                                  "Quantity of ProcOut imported and exported regionally per export country, export region, import country, import region and ProcOut product in Mg per year"
vProcOutRegionImportMg           (Country,Region,ProcOut)                                                   "Quantity of ProcOut imported into each region per country, region and ProcOut in Mg per year"
vManureRegionImportExportMg      (Country,Region,CountryA,RegionA,APS)                                      "Quantity of manure imported and exported regionally per export country, export region, import country, import region and animal production system in Mg NPK per year"
vFLWFeedRegionImportExportMg     (Country,Region,CountryA,RegionA,AllCat,AllProd)                           "Quantity of food loss and waste imported and exported regionally per export country, export region, import country, import region, food loss and waste category and food loss and waste product in Mg per year"
vCompostRegionImportExportMg     (Country,Region,CountryA,RegionA,Compost)                                  "Quantity of compost imported and exported regionally per export country, export region, import country, import region and compost product in Mg NPK per year"                               

* External import and export related variables
vProcInNationalImportExportMg    (Country,Region,ImportExport,ProcIn)                                       "Quantity of ProcIn products nationally imported and export per country, region, ImportExport and ProcIn product in Mg per year"
vProcOutHNationalImportExportMg  (Country,Region,ImportExport,ProcOutH)                                     "Quantity of ProcOut products nationally imported and export per country, region, ImportExport and human food product in Mg per year"

* Human diet related variables
vHumNutrRIntake                  (Country,Region,HumNutrR,Month)                                            "Quantity of human nutrients consumed per country, region, human nutrient and month in various units per month"
vHumFoodFamilyIntakeMg           (Country,Region,Fam,Month)                                                 "Quantity of food products and families consumed per country, region, food family and month in Mg per month"
vHumanExcMg                      (Country,Region,FertNutr)                                                  "Human excreter availabe NOT USED CURRENTLY"

* Animal related GHG variables 
vAniUrineNExcMMMg                (Country,Region,APS,Level,HerdType)                                        "Urine N excretion in manure management systems per country, region, animal production system, productivity level, and HerdType in Mg per year"
vAniUrineNExcGMg                 (Country,Region,APS,Level,HerdType)                                        "Urine N excretion during grazing per country, region, animal production system, productivity level, and HerdType in Mg per year"
vAniTANExcMMMg                   (Country,Region,APS,Level,HerdType)                                        "TAN excretion from manure management per country, region, animal production system, productivity level, and HerdType in Mg per year"
vAniTANExcGMg                    (Country,Region,APS,Level,HerdType)                                        "TAN excretion from grazing per country, region, animal production system, productivity level, and HerdType in Mg per year"
vAniN2ONDMMMg                    (Country,Region,APS,Level,HerdType)                                        "Direct N2O-N from manure management systems per country, region, animal production system, productivity level, and HerdType in Mg per year"
vAniNOXNMMMg                     (Country,Region,APS,Level,HerdType)                                        "Direct NOx-N from manure management systems per country, region, animal production system, productivity level, and HerdType in Mg per year"
vAniNH3NMMMg                     (Country,Region,APS,Level,HerdType)                                        "Direct NH3-N from manure management systems per country, region, animal production system, productivity level, and HerdType in Mg per year"
vAniN2NMMMg                      (Country,Region,APS,Level,HerdType)                                        "Direct N2-N from manure management systems per country, region, animal production system, productivity level, and HerdType in Mg per year"
vAniN2ONIDMMMg                   (Country,Region,APS,Level,HerdType)                                        "Indirect N2O-N from manure management systems per country, region, animal production system, productivity level, and HerdType in Mg per year"
vAniN2ONDeNitrMMMg               (Country,Region,APS,Level,HerdType)                                        "N2O-N from aquaculture excretion per country, region, animal production system, productivity level, and HerdType in Mg per year"
vAniN2ONDeNitrFeedMg             (Country,Region,APS,Level,HerdType)                                        "N2O-N from aquaculture feed losses per country, region, animal production system, productivity level, and HerdType in Mg per year"
vManureNLossesMg                 (Country,Region,APS,Level,HerdType,FertNutr)                               "N losses from manure management systems per country, region, animal production system, productivity level, and HerdType in Mg per year"
vAniVSExcMMkg                    (Country,Region,APS,Level,HerdType)                                        "Volatile solid excretion in manure management systems per country, region, animal production system, productivity level, and HerdType in Mg per year"
vAniVSExcGMg                     (Country,Region,APS,Level,HerdType)                                        "Volatile solid excretion during grazing per country, region, animal production system, productivity level, and HerdType in Mg per year"
vAniCH4ManureMg                  (Country,Region,APS,Level,HerdType)                                        "CH4 emissions from manure management systems per country, region, animal production system, productivity level, and HerdType in Mg per year"
vAnivCH4EntericMg                (Country,Region,APS,Level,HerdType)                                        "CH4 emissions from enteric fermentation per country, region, animal production system, productivity level, and HerdType in Mg per year"
vAniCO2EnergyMg                  (Country,Region,APS,Level,HerdType)                                        "Energy emission from housing (CO2e) per country, region, animal production system, productivity level, and HerdType in Mg per year"
vAniCO2FeedProcMg                (Country,Region,APS,level,HerdType)                                        "CO2 emissions from animal feed processing per country, region, animal production system, productivity level, and HerdType in Mg per year"
vAniCO2eqTotalMg                 (Country,Region,APS,Level,HerdType)                                        "Total animal emissions per country, region, animal production system, productivity level, and HerdType in Mg per year"

* Compost related GHG variables
vCompostN2ONDMg                  (Country,Region,Compost)                                                   "N2O-N emissions from compost per country, region and compost in Mg per year"
vCompostCH4Mg                    (Country,Region,Compost)                                                   "CH4 emissions from compost per country, region and compost in Mg per year"
vCompostCO2eqMg                  (Country,Region,Compost)                                                   "Total CO2e emissions from compost per country, region, compost in Mg per year"

* Crop related GHG variables
vCropN2ONDMg                     (Country,Region,LandType,SoilType,Crop)                                    "Direct N2O-N from cropland per country, region, land type, soil type, and crop in Mg per year"
vCropNOXNMg                      (Country,Region,LandType,SoilType,Crop)                                    "Direct NOx-N from cropland per country, region, land type, soil type, and crop in Mg per year"
vCropNH3NMg                      (Country,Region,LandType,SoilType,Crop)                                    "Direct NH3-N from cropland per country, region, land type, soil type, and crop in Mg per year"
vCropNLeachMg                    (Country,Region,LandType,SoilType,Crop)                                    "N leaching from cropland per country, region, land type, soil type, and crop in Mg per year"
vCropN2Mg                        (Country,Region,LandType,SoilType,Crop)                                    "N2 losses from cropland per country, region, land type, soil type, and crop in Mg per year"
vCropN2ONLMg                     (Country,Region,LandType,SoilType,Crop)                                    "Indirect N2O-N from leaching per country, region, land type, soil type, and crop in Mg per year"
vCropN2ONVMg                     (Country,Region,LandType,SoilType,Crop)                                    "Indirect N2O-N from volatilisation per country, region, land type, soil type, and crop in Mg per year"
vCropCO2FuelMg                   (Country,Region,LandType,SoilType,Crop)                                    "Fuel emissions (CO2e) from cropland per country, region, land type, soil type, and crop in Mg per year"
vCropGlassCO2eqMg                (Country,Region,LandType,SoilType,Crop)                                    "Energy emissions (CO2e) from cropland per country, region, land type, soil type, and crop in Mg per year"
vCropCO2ArtFertMg                (Country,Region,LandType,SoilType,Crop)                                    "Fertiliser production emissions (CO2e) per country, region, land type, soil type, and crop in Mg per year"
vCropCO2eqTotalMg                (Country,Region,LandType,SoilType,Crop)                                    "Total cropland emissions per country, region, land type, soil type, and crop in Mg per year"

* Transport related variables 
vTransportMg                     (Country,Region,CountryA,RegionA)                                          "Quantity of product transported per export country, export region, import country, import region in Mg per year"
vTransCO2Mg                      (Country,Region)                                                           "Transport emissions (CO2e) per country and region in Mg per year"

* Other variables
vMarineFishCO2Mg                 (Country,Region)                                                           "CO2e emissions from capture fish per country and region in Mg per year"
vGHGRegionCO2eqMg                (Country,Region)                                                           "Total greenhouse gas emissions (CO2e) per country and region in Mg per year"
vPosDev                          (Fam)                                                                      "Positive deviation reference objective function per food family in Mg per year"
vNegDev                          (Fam)                                                                      "Negative deviation reference objective function per food family in Mg per year"
;
FREE VARIABLES // Objective function variables
vTotalLandUse                                                                                               "Total land use in ha"
vTotalCO2eq                                                                                                 "Total GHG emissions in Mg"
vPenaltyReference                                                                                           "Reference objective function minimise difference in Mg"
;
****** Objective Equations ******

EQUATION eTotalLandUse "Objective function to minimise land use";
         eTotalLandUse..
         vTotalLandUse =E=
         SUM(CR, vLandUseRegionha(CR))
;
EQUATION eTotalCO2eq "Objective function to minimise GHG emissions";
         eTotalCO2eq..
         vTotalCO2eq =E= SUM(CR, vGHGRegionCO2eqMg(CR))
;
EQUATION eIdentity(Fam) "Objective function for reference scenario, calculate difference between current diet and modelled diet";
         eIdentity(Fam)..
         SUM((CR,Month), vHumFoodFamilyIntakeMg(CR,Fam,Month)) - SUM((Country,Region,Gender,Age)$CR(Country,Region), Population(Country,Region,Gender,Age) * HumIntakeReq(Country,'Total','Min',Fam) * 0.001 * 365) =E= vPosDev(Fam) - vNegDev(Fam)
;
EQUATION ePenalty "Objective function for reference scenario, minimise difference between current diet and modelled diet";
         ePenalty..
         vPenaltyReference =E= SUM((Fam), vPosDev(Fam) + vNegDev(Fam))
;
****** Land Equations ******

EQUATION eLandUseRegion(CountryA,RegionA) "Calculate the total land use per country and region in ha per year";
         eLandUseRegion(CRA)..
         vLandUseRegionha(CRA) =E=
            SUM((LandSoilCrop)$CropSuitability(CRA,LandSoilCrop), vNumha(CRA,LandSoilCrop))          +
            SUM(ProcOutAA, vAminoAcidMg(CRA,'AminoAcid',ProcOutAA) * AniAminoAcidLU(ProcOutAA))
;
EQUATION eReferenceCroplandArea(Country,Region,Crop) "Area of each crop in the reference scenario per country, region and crop in ha per year" ;
         eReferenceCroplandArea(CR,NoGrassCrop)$CRCrop(CR,NoGrassCrop)..
         CropReferenceha(CR,NoGrassCrop)  =E= SUM((LandSoil)$CropSuitability(CR,LandSoil,NoGrassCrop), vNumha(CR,LandSoil,NoGrassCrop))
;
EQUATION eReferenceGrasslandArea(Country) "Area of grassland in the reference scenario per country in ha per year" ;
         eReferenceGrasslandArea(Country)..
         ReferenceGrassAreaha(Country) =E= SUM((Region,LandSoil,GrassCrop)$(CR(Country,Region) AND CropSuitability(Country,Region,LandSoil,GrassCrop)), vNumha(Country,Region,LandSoil,GrassCrop)) 
;
EQUATION eLandAvailable(Country,Region,LandType,SoilType) "Constraint, area of land is equal to or less than land available per country, region, land type and soil type in ha per year" ;
         eLandAvailable(CR,LandSoil)$CRLandSoil(CR,LandSoil)..
         LandAreaha(CR,LandSoil) =G= SUM((Crop)$CropSuitability(CR,LandSoil,Crop), vNumha(CR,LandSoil,Crop))
;
EQUATION eCropAvailable(Country,Region,Crop) "Calculate the quantity of crop grown per country, region and crop in Mg per year" ;
         eCropAvailable(CR,Crop)$CRCrop(CR,Crop)..
         vCropProdMg(CR,Crop) =E= SUM((LandSoil)$CropSuitability(CR,LandSoil,Crop), vNumha(CR,LandSoil,Crop) * CropYieldha(CR,LandSoil,Crop))
;
EQUATION eCropRotationFamily(Country,Region,LandType,SoilType,RotFam) "Constraint, crop rotation at a crop family level, one crop cannot cover more then XX% of arable land" ;
         eCropRotationFamily(CR,'Arable',SoilType,RotFam)..
         SUM((Crop,RotFamA)$(CropSuitability(CR,'Arable',SoilType,Crop) AND CropRotFam(Crop,'Temporary',RotFamA)), vNumha(CR,'Arable',SoilType,Crop)) =G=
            SUM((Crop)$(CropSuitability(CR,'Arable',SoilType,Crop) AND CropRotFam(Crop,'Temporary',RotFam)), vNumha(CR,'Arable',SoilType,Crop)) * CropRotation(RotFam)
;
EQUATION eCropRotationCrop(Country,Region,LandType,SoilType,Crop) "Constraint, crop rotation at a crop level, one crop cannot cover more then XX% of arable land" ;
         eCropRotationCrop(CR,'Arable',SoilType,Crop)$(SUM(RotFam, CropRotFam(Crop,'Temporary',RotFam)) AND CropSuitability(CR,'Arable',SoilType,Crop))..
         SUM((CropA)$(SUM(RotFam, CropRotFam(Crop,'Temporary',RotFam)) AND CropSuitability(CR,'Arable',SoilType,CropA)), vNumha(CR,'Arable',SoilType,CropA)) =G= vNumha(CR,'Arable',SoilType,Crop) * CropOther(Crop,'Rotation')
;
EQUATION eCropFert(CountryA,RegionA,LandType,SoilType,Crop,FertNutr) "Calculate fertiliser and organic amendements applied per country, region, land type, soil type, crop and fertiliser nutrient in Mg per year" ;
         eCropFert(CRA,LandSoil,Crop,FertNutr)$CropSuitability(CRA,LandSoil,Crop)..
         vCropFertMg(CRA,LandSoil,Crop,FertNutr) =E=
                 SUM((FertProdCat,FertProd)$(CropFertSuitability(Crop,FertProdCat) AND FertCatAllProd(FertProdCat,FertProd)),                                       vFertProdMg(CRA,LandSoil,Crop,FertProdCat,FertProd)                 * FertNutrByProductMgMg(FertProdCat,FertProd,FertNutr)      * FRVByProduct(FertProdCat,FertProd,FertNutr)) +
                 SUM((FertOthCat,FertProd)$(CropFertSuitability(Crop,FertOthCat) AND FertCatAllProd(FertOthCat,FertProd)),                                          vFertOtherNPKMg(CRA,LandSoil,Crop,FertOthCat,FertProd)              * NPKRatioCR(CRA,FertOthCat,FertProd,FertNutr)              * FRVByProduct(FertOthCat,FertProd,FertNutr))  +
                 SUM((CR,FertManCat,FertProd)$(CropFertSuitability(Crop,FertManCat) AND FertCatAllProd(FertManCat,FertProd) AND ImportBoundary(CR,CRA,FertManCat)), vFertManureCompostNPKMg(CR,CRA,LandSoil,Crop,FertManCat,FertProd)   * NPKRatioCR(CR,FertManCat,FertProd,FertNutr)               * FRVByProduct(FertManCat,FertProd,FertNutr))
;
EQUATION eCropFertManure(CountryA,RegionA,LandType,SoilType,Crop,FertNutr) "Calculate animal manure applied per country, region, land type, soil type, crop and fertiliser nutrient in Mg per year" ;
         eCropFertManure(CRA,LandSoil,Crop,FertNutr)$CropSuitability(CRA,LandSoil,Crop)..
         vCropFertManureMg(CRA,LandSoil,Crop,FertNutr) =E=
                 SUM((ManureProd)$(CropFertSuitability(Crop,'GrazingManure') AND FertCatAllProd('GrazingManure',ManureProd)),                          vFertOtherNPKMg(CRA,LandSoil,Crop,'GrazingManure',ManureProd)         * NPKRatioCR(CRA,'GrazingManure',ManureProd,FertNutr))  +
                 SUM((CR,ManureProd)$(CropFertSuitability(Crop,'Manure') AND FertCatAllProd('Manure',ManureProd) AND ImportBoundary(CR,CRA,'Manure')), vFertManureCompostNPKMg(CR,CRA,LandSoil,Crop,'Manure',ManureProd)     * NPKRatioCR(CR,'Manure',ManureProd,FertNutr))
;
EQUATION eCropFertManureNoDerogation(Country,Region,LandType,SoilType,Crop) "Constraint, maximum quantity of animal manure that can be applied without derogation per country, region, land type, soil type and crop in Mg per year" ;
         eCropFertManureNoDerogation(CR,LandType,SoilType,Crop)$(CropSuitability(CR,LandType,SoilType,Crop) AND (NOT DerogationCrop(Crop)))..
         vCropFertManureMg(CR,LandType,SoilType,Crop,'N') =L= CropFertManureha(CR,SoilType,'Standard') * vNumha(CR,LandType,SoilType,Crop)
;
EQUATION eCropFertManureDerogation(Country,Region,LandType,SoilType,DerogationCrop) "Constraint, maximum quantity of animal manure that can be applied with derogation per country, region, land type, soil type and crop in Mg per year" ;
         eCropFertManureDerogation(CR,LandType,SoilType,DerogationCrop)$CropSuitability(CR,LandType,SoilType,DerogationCrop)..
          vCropFertManureMg(CR,LandType,SoilType,DerogationCrop,'N') =L= CropFertManureha(CR,SoilType,'Derogation')  * vNumha(CR,LandType,SoilType,DerogationCrop)
;
EQUATION eCropFertResidue(Country,Region,LandType,SoilType,Crop) "Constraint, nitrogen in crop residues cannot exceed 50 kg/ha to ensure an even spread of crop residues amongst the different crops";
         eCropFertResidue(CR,LandSoil,Crop)$CropSuitability(CR,LandSoil,Crop)..
         SUM((FertProd)$(CropFertSuitability(Crop,'Residue') AND FertCatAllProd('Residue',FertProd)),  vFertProdMg(CR,LandSoil,Crop,'Residue',FertProd) * FertNutrByProductMgMg('Residue',FertProd,'N')) =L=
                vNumha(CR,LandSoil,Crop) * (50/1000)       
;
EQUATION eCropFertCompost(CountryA,RegionA,LandType,SoilType,Crop) "Constraint, nitrogen in compost cannot exceed 50 kg/ha to ensure an even spread of compost amongst the different crops";
         eCropFertCompost(CRA,LandSoil,Crop)$CropSuitability(CRA,LandSoil,Crop)..
         SUM((CR,FertProd)$(CropFertSuitability(Crop,'Compost') AND FertCatAllProd('Compost',FertProd) AND ImportBoundary(CR,CRA,'Compost')), vFertManureCompostNPKMg(CR,CRA,LandSoil,Crop,'Compost',FertProd) * NPKRatioCR(CR,'Compost',FertProd,'N')) =L=
                vNumha(CRA,LandSoil,Crop) * (50/1000)                
;
EQUATION eCropFertReference(Country,Region,LandType,SoilType,Crop,FertNutrNP) "Constraint, nitrogen and phosphorus to be applied in the reference scenario per country, region, land type, soil type, crop and fertiliser nutrient in Mg per year" ;
         eCropFertReference(CR,LandSoilCrop,FertNutrNP)$CropSuitability(CR,LandSoilCrop)..
         vCropFertMg(CR,LandSoilCrop,FertNutrNP) =E= CropFertha(CR,LandSoilCrop,FertNutrNP) * vNumha(CR,LandSoilCrop) * 1.075
;
EQUATION eCropFertPKMax(Country,Region,LandType,SoilType,Crop,FertNutr) "Constraint, maximum phosphorus and potassium to be applied in all other scenarios per country, region, land type, soil type, crop and fertiliser nutrient in Mg per year" ;
         eCropFertPKMax(CR,LandSoilCrop,FertNutrPK)$CropSuitability(CR,LandSoilCrop)..
         vCropFertMg(CR,LandSoilCrop,FertNutrPK) =L= CropFertha(CR,LandSoilCrop,FertNutrPK) * vNumha(CR,LandSoilCrop) * 1.05
;
EQUATION eCropFertPKMin(Country,Region,LandType,SoilType,Crop,FertNutr) "Constraint, minimum phosphorus and potassium to be applied in all other scenarios per country, region, land type, soil type, crop and fertiliser nutrient in Mg per year" ;
         eCropFertPKMin(CR,LandSoilCrop,FertNutrPK)$CropSuitability(CR,LandSoilCrop)..
         vCropFertMg(CR,LandSoilCrop,FertNutrPK) =G= CropFertha(CR,LandSoilCrop,FertNutrPK) * vNumha(CR,LandSoilCrop)
;
EQUATION eCropFertNMax(Country,Region,LandType,SoilType,Crop) "Constraint, maximum nitrogen to be applied in all other scenarios per country, region, land type, soil type, crop and fertiliser nutrient in Mg per year" ;
         eCropFertNMax(CR,LandSoilCrop)$CropSuitability(CR,LandSoilCrop)..
         vCropFertMg(CR,LandSoilCrop,'N') =L= vCropFertNMg(CR,LandSoilCrop) * 1.05
;
EQUATION eCropFertNMin(Country,Region,LandType,SoilType,Crop) "Constraint, minimum nitrogen to be applied in all other scenarios per country, region, land type, soil type, crop and fertiliser nutrient in Mg per year" ;
         eCropFertNMin(CR,LandSoilCrop)$CropSuitability(CR,LandSoilCrop)..
         vCropFertMg(CR,LandSoilCrop,'N') =G= vCropFertNMg(CR,LandSoilCrop)
;
EQUATION eCropNFert(Country,Region,LandType,SoilType,Crop) "Calculate balanced nitrogen fertilisation requirements per country, region, land type, soil type and crop in Mg per year";
         eCropNFert(Country,Region,LandType,SoilType,Crop)$(CR(Country,Region) AND CropSuitability(Country,Region,LandType,SoilType,Crop))..
         vCropFertNMg(Country,Region,LandType,SoilType,Crop) =E=      
              ((vNumha(Country,Region,LandType,SoilType,Crop) * CropHarvestedNha(Country,Region,LandType,SoilType,Crop,'N'))      +
                vCropNOXNMg(Country,Region,LandType,SoilType,Crop)                                                                +
                vCropNLeachMg(Country,Region,LandType,SoilType,Crop)                                                              +
                vCropNH3NMg(Country,Region,LandType,SoilType,Crop)                                                                +
                vCropN2Mg(Country,Region,LandType,SoilType,Crop)                                                                  +
                vCropN2ONDMg(Country,Region,LandType,SoilType,Crop))                                                              -
              ((vNumha(Country,Region,LandType,SoilType,Crop) * CropOther(Crop,'NFix'))$CropOther(Crop,'NFix')                    +
               (vNumha(Country,Region,LandType,SoilType,Crop) * NMineralha(Country,Soiltype) * 0.75)$NMineralha(Country,Soiltype) +
               (vNumha(Country,Region,LandType,SoilType,Crop) * NDepositionha(Country,Region) * 0.75))                              
;
vFertManureCompostNPKMg.FX(CR,CRA,LandSoil,Crop,'HumanExcreta',ManureProd)=0;
****** Fish Equations ******
EQUATION eCaptureFishAv(Country,Region,FishSpecies) "Constraint, marine fish is equal or less then marine fish available per country, region and fish species in Mg per year";
         eCaptureFishAv(CR,FishSpecies)$MarineFish(CR,FishSpecies)..
         vMarineFishMg(CR,FishSpecies) =L=  MarineFish(CR,FishSpecies) * ScenarioControlsModel('Util.MFish')
;
****** Processing and import/export equations ******
EQUATION eProcInInput(Country,Region,AllProd) "Calculate the quantity of product (intermediate processing step, see wiki) per country, region and process in Mg per year" ;
         eProcInInput(CR,ProcIn)..
         SUM((Process)$ProcInProcess(ProcIn,Process), vProcessMg(CR,Process)) =E=
                 (vCropProdMg(CR,ProcIn)     * (1 - FoodLossWasteFrac('Crop',PHLoss,ProcIn))         - vProcInNationalImportExportMg(CR,'Export',ProcIn))$CRCrop(CR,ProcIn)     +
                 (vMeatMilkEggsMg(CR,ProcIn) * (1 - FoodLossWasteFrac('MeatMilkEggs',PHLoss,ProcIn)) - vProcInNationalImportExportMg(CR,'Export',ProcIn))$MeatMilkEggs(ProcIn)  +
                 (vMarineFishMg(CR,ProcIn)   * (1 - FoodLossWasteFrac('FishSpecies',PHLoss,ProcIn))  - vProcInNationalImportExportMg(CR,'Export',ProcIn))$MarineFish(CR,ProcIn) +
                 (vProcInNationalImportExportMg(CR,'Import',ProcIn))
;
EQUATION eProcInRegionalImportExport(CountryA,RegionA,ImportExport,ProcIn) "Calculate ProcIn products imported and exported at a national level imported into the region per country, region, ImportExport and ProcIn product in Mg per year";
         eProcInRegionalImportExport(CRA,ImportExport,ProcIn)..
         vProcInNationalImportExportMg(CRA,ImportExport,ProcIn) =E=
         SUM((CR)$ProcInImportExportSuitability(CR,ImportExport,ProcIn), vProcInRegionImportExportMg(CR,CRA,ImportExport,ProcIn))
;
EQUATION eProcInNationalImportExport(Country,Region,ImportExport,ProcIn) "Constraint, quantity of ProcIn products imported and exported at a national level and then export to region per country, region, ImportExport and ProcIn product in Mg per year";
         eProcInNationalImportExport(CR,ImportExport,ProcIn)$ProcInImportExportSuitability(CR,ImportExport,ProcIn)..
         SUM(CRA, vProcInRegionImportExportMg(CR,CRA,ImportExport,ProcIn)) =E=
                ProcInImportExportMg(CR,ImportExport,ProcIn)
;
EQUATION eProcessing(Country,Region,ProcOut) "Calcualte ProcOut products based on Process products and processing fractions per country, region and ProcOut product in Mg per year";
         eProcessing(CR,ProcOut)..
         vProcOutMg(CR,ProcOut) =E= SUM(Process, vProcessMg(CR,Process) * Processing(Process,ProcOut))
;
EQUATION eProcOutHRegionExport(Country,Region,ProcOut) "Calculate the quantity of processed human food products availabe for regional export including processed national import products per country, region and ProcOut product in Mg per year";
         eProcOutHRegionExport(CR,ProcOutH)..
         SUM((CRA)$ImportBoundary(CR,CRA,'ProcOutH'), vProcOutRegionImportExportMg(CR,CRA,ProcOutH)) =E= (vProcOutMg(CR,ProcOutH) * (1 - FoodLossWasteFrac('ProcOutH',PPLoss,ProcOutH))) + vProcOutHNationalImportExportMg(CR,'Import',ProcOutH)$ProcOutHImportExportSuitability(CR,'Import',ProcOutH)
;
EQUATION eProcOutCPRegionExport(Country,Region,ProcOut) "Calculate the quantity of processed co-products for regional export per country, region and ProcOut product in Mg per year";
         eProcOutCPRegionExport(CR,ProcOutCP)..
         vProcOutMg(CR,ProcOutCP) * ScenarioControlsModel('Util.CoProduct') =E=
                  SUM((CRA)$ImportBoundary(CR,CRA,'CoProduct'), vProcOutRegionImportExportMg(CR,CRA,ProcOutCP)) +
                  vCompostCoProductMg(CR,'CoProduct',ProcOutCP)            
;
EQUATION eProcOutCPRegionExportReference(Country,Region,ProcOut) "Calculate the quantity of processed co-products for regional export per country, region and ProcOut product in Mg per year";
         eProcOutCPRegionExportReference(CR,ProcOutCP)$SUM(APS, AniFeedSuitability(APS,'CoProduct',ProcOutCP))..
         vProcOutMg(CR,ProcOutCP) * ScenarioControlsModel('Util.CoProduct') =E=
                  SUM((CRA)$ImportBoundary(CR,CRA,'CoProduct'), vProcOutRegionImportExportMg(CR,CRA,ProcOutCP)) +
                  vCompostCoProductMg(CR,'CoProduct',ProcOutCP)            
;
EQUATION eProcOutCPRegionExportReferenceSW(Country,Region,ProcOut) "Calculate the quantity of processed co-products for regional export per country, region and ProcOut product in Mg per year";
         eProcOutCPRegionExportReferenceSW(CR,ProcOutCP)$(NOT SUM(APS, AniFeedSuitability(APS,'CoProduct',ProcOutCP)))..
         vProcOutMg(CR,ProcOutCP) * ScenarioControlsModel('Util.CoProduct') * 0.125 =E=
                  SUM((CRA)$ImportBoundary(CR,CRA,'CoProduct'), vProcOutRegionImportExportMg(CR,CRA,ProcOutCP)) +
                  vCompostCoProductMg(CR,'CoProduct',ProcOutCP)            
;
EQUATION eProcOutFFRegionExport(Country,Region,ProcOut) "Calculate the quantity of processed food-feed products for regional export per country, region and ProcOut product in Mg per year";
         eProcOutFFRegionExport(CR,ProcOutFF)..
         SUM((CRA)$ImportBoundary(CR,CRA,'FoodFeed'), vProcOutRegionImportExportMg(CR,CRA,ProcOutFF)) =E= vProcOutMg(CR,ProcOutFF)
;
EQUATION eProcOutHRegionImport(CountryA,RegionA,ProcOut) "Calculate the quantity of processed human food products for regional export per country, region and ProcOut product in Mg per year";
         eProcOutHRegionImport(CRA,ProcOutH)..
         vProcOutRegionImportMg(CRA,ProcOutH) =E= SUM((CR)$ImportBoundary(CR,CRA,'ProcOutH'), vProcOutRegionImportExportMg(CR,CRA,ProcOutH))
;
EQUATION eProcOutCPRegionImport(CountryA,RegionA,ProcOut) "Calculate the quantity of processed co-products products for regional export per country, region and ProcOut product in Mg per year";
         eProcOutCPRegionImport(CRA,ProcOutCP)..
         vProcOutRegionImportMg(CRA,ProcOutCP) =E= SUM((CR)$ImportBoundary(CR,CRA,'CoProduct'), vProcOutRegionImportExportMg(CR,CRA,ProcOutCP))
;
EQUATION eProcOutFFRegionImport(CountryA,RegionA,ProcOut) "Calculate the quantity of processed food-feed products for regional import per country, region and ProcOut product in Mg per year";
         eProcOutFFRegionImport(CRA,ProcOutFF)..
         vProcOutRegionImportMg(CRA,ProcOutFF) =E= SUM((CR)$ImportBoundary(CR,CRA,'FoodFeed'), vProcOutRegionImportExportMg(CR,CRA,ProcOutFF))
;
EQUATION eProcOutH(Country,Region,ProcOutH) "Calculate the quantity of processed food available before losses per country, region and ProcOut product in Mg per year";
         eProcOutH(CR,ProcOutH)..
         vProcOutRegionImportMg(CR,ProcOutH) - vProcOutHNationalImportExportMg(CR,'Export',ProcOutH)$ProcOutHImportExportSuitability(CR,'Export',ProcOutH) =E= vProcOutHMg(CR,ProcOutH)
;
EQUATION eProcOutHNationalImportExport(Country,Region,ImportExport,ProcOutH) "Constraint, quantity of processed human food products imported and exported at a national level per country, region, ImportExport and ProcOut product in Mg per year";
         eProcOutHNationalImportExport(CR,ImportExport,ProcOutH)$ProcOutHImportExportSuitability(CR,ImportExport,ProcOutH)..
         vProcOutHNationalImportExportMg(CR,ImportExport,ProcOutH)  =E=
                 ProcOutHImportExportMg(CR,ImportExport,ProcOutH)
;
EQUATION eNutrBalancePSF(Country,FertNutr) "Calculate and balance fertiliser nutrients in imported and exported plant-sourced foods per country and fertiliser nutrient in Mg per year";
         eNutrBalancePSF(Country,FertNutr)..
         SUM((Region,ProcOutHPSF)$(CR(Country,Region) AND ProcOutHImportExportSuitability(Country,Region,'Export',ProcOutHPSF)), vProcOutHNationalImportExportMg(Country,Region,'Export',ProcOutHPSF) * FertNutrByProductMgMg('ProcOutH',ProcOutHPSF,FertNutr)) +
         SUM((Region,CRA,ProcIn)$(CR(Country,Region) AND ProcInImportExportSuitability(Country,Region,'Export',ProcIn)),         vProcInRegionImportExportMg(Country,Region,CRA,'Export',ProcIn)      * FertNutrByProductMgMg('ProcOutH',ProcIn,FertNutr))     =E=
         SUM((Region,ProcOutHPSF)$(CR(Country,Region) AND ProcOutHImportExportSuitability(Country,Region,'Import',ProcOutHPSF)), vProcOutHNationalImportExportMg(Country,Region,'Import',ProcOutHPSF) * FertNutrByProductMgMg('ProcOutH',ProcOutHPSF,FertNutr)) +
         SUM((Region,CRA,ProcIn)$(CR(Country,Region) AND ProcInImportExportSuitability(Country,Region,'Import',ProcIn)),         vProcInRegionImportExportMg(Country,Region,CRA,'Import',ProcIn)      * FertNutrByProductMgMg('ProcOutH',ProcIn,FertNutr))
;
EQUATION eNutrBalanceASF(Country,FertNutr) "Calculate and balance fertiliser nutrients in imported and exported anaiml-sourced foods per country and fertiliser nutrient in Mg per year";
         eNutrBalanceASF(Country,FertNutr)..
         SUM((Region,ProcOutHASF)$(CR(Country,Region) AND ProcOutHImportExportSuitability(Country,Region,'Export',ProcOutHASF)), vProcOutHNationalImportExportMg(Country,Region,'Export',ProcOutHASF) * FertNutrByProductMgMg('ProcOutH',ProcOutHASF,FertNutr)) =E=
         SUM((Region,ProcOutHASF)$(CR(Country,Region) AND ProcOutHImportExportSuitability(Country,Region,'Import',ProcOutHASF)), vProcOutHNationalImportExportMg(Country,Region,'Import',ProcOutHASF) * FertNutrByProductMgMg('ProcOutH',ProcOutHASF,FertNutr))  
;
EQUATION eHumNutrImportMax(Country,HumNutrR) "Constraint, nutrients in imported food cannot exceed XX% of nutrients consumed in the total diet";
         eHumNutrImportMax(Country,HumNutrR)..
         SUM((Region,Month)$CR(Country,Region), vHumNutrRIntake(Country,Region,HumNutrR,Month)) * ScenarioControlsModel('National.ImportPerc') =G= 
         SUM((Region,ProcOutH)$(CR(Country,Region) AND ProcOutHImportExportSuitability(Country,Region,'Import',ProcOutH)), vProcOutHNationalImportExportMg(Country,Region,'Import',ProcOutH) * ((1 - FoodLossWasteFrac('ProcOutH',DWaste,ProcOutH)) * (1 -  FoodLossWasteFrac('ProcOutH',CWaste,ProcOutH)) * HumNutrByProduct(ProcOutH,HumNutrR)))
;
****** Loss equations ******
EQUATION ePostHarvestlossCrop(Country,Region,AllCat,ProcIn) "Calculate post harvest losses of crop, livestock and fish products in Mg per year";
         ePostHarvestlossCrop(CR,PHLoss,ProcIn)..
         vFoodLossMg(CR,PHLoss,ProcIn) =E=
                 (vCropProdMg(CR,ProcIn)     * FoodLossWasteFrac('Crop',PHLoss,ProcIn))$CRCrop(CR,ProcIn)            +
                 (vMeatMilkEggsMg(CR,ProcIn) * FoodLossWasteFrac('MeatMilkEggs',PHLoss,ProcIn))$MeatMilkeggs(ProcIn) +
                 (vMarineFishMg(CR,ProcIn)   * FoodLossWasteFrac('FishSpecies',PHLoss,ProcIn))$MarineFish(CR,ProcIn)
;
EQUATION eProcessingPackagingloss(Country,Region,AllCat,ProcOutH) "Calculate processing an packaging losses of processed food products per country, region, food loss category and food product in Mg per year";
         eProcessingPackagingloss(CR,PPLoss,ProcOutH)..
         vFoodLossMg(CR,PPLoss,ProcOutH) =E= vProcOutMg(CR,ProcOutH) * FoodLossWasteFrac('ProcOutH',PPLoss,ProcOutH)
;
EQUATION eDistributionWaste(Country,Region,AllCat,ProcOutH) "Calculate distribution waste of processed food products per country, region, food waste category and food product in Mg per year";
         eDistributionWaste(CR,DWaste,ProcOutH)..
         vFoodWasteMg(CR,DWaste,ProcOutH) =E= vProcOutHMg(CR,ProcOutH) *  FoodLossWasteFrac('ProcOutH',DWaste,ProcOutH)
;
EQUATION eConsumptionWaste (Country,Region,AllCat,ProcOutH) "Calculate consumption waste of processed food products per country, region, food waste category and food product in Mg per year";
         eConsumptionWaste(CR,CWaste,ProcOutH)..
         vFoodWasteMg(CR,CWaste,ProcOutH) =E= vProcOutHMg(CR,ProcOutH) * ((1 - FoodLossWasteFrac('ProcOutH',DWaste,ProcOutH)) * FoodLossWasteFrac('ProcOutH',CWaste,ProcOutH))
;
****** Product Availability Equations ******
EQUATION eFoodAvailable(Country,Region,ProcOutH) "Calculate food available for consumption by subtracting Food at consumption level, processed food products minus food losses (losses after processing)";
         eFoodAvailable(CR,ProcOutH)..
         SUM((Month)$HumFoodMonth(ProcOutH,Month), vProcoutHMonthMg(CR,ProcOutH,Month)) =E=
                vProcOutHMg(CR,ProcOutH) * ((1 - FoodLossWasteFrac('ProcOutH',DWaste,ProcOutH)) * (1 - FoodLossWasteFrac('ProcOutH',CWaste,ProcOutH)))
;
EQUATION eFoodFeedAvailable(Country,Region,ProcOut) "Calculate food-feed crops required for livestock consumption and convert from FM to DM per country, region and ProcOut product in Mg per year";
         eFoodFeedAvailable(CR,ProcOutFF)..
         vProcOutRegionImportMg(CR,ProcOutFF) =E=
                 SUM((APS,level,HerdType)$(APSHerdType(APS,Level,HerdType) AND AniFeedSuitability(APS,'FoodFeed',ProcOutFF)), vAniFeedIntakeHerdTypeMgDM(CR,APS,Level,HerdType,'FoodFeed',ProcOutFF) * (1 + AniFeedLossFrac('FoodFeed',FeedLoss,ProcOutFF)) * (AniNutrByProduct('FoodFeed',ProcOutFF,'FM') * 0.001))
;
EQUATION eCoProductFeed(Country,Region,ProcOut) "Calculate co-products required for livestock consumption and convert from FM to DM per country, region and ProcOut product in Mg per year";
         eCoProductFeed(CR,ProcOutCP)..
         vProcOutRegionImportMg(CR,ProcOutCP)  =E=
                 SUM((APS,level,HerdType)$(APSHerdType(APS,Level,HerdType) AND AniFeedSuitability(APS,'CoProduct',ProcOutCP)), vAniFeedIntakeHerdTypeMgDM(CR,APS,Level,HerdType,'CoProduct',ProcOutCP) * (1 + AniFeedLossFrac('CoProduct',FeedLoss,ProcOutCP)) * (AniNutrByProduct('CoProduct',ProcOutCP,'FM') * 0.001))
;
EQUATION eGrassAvailable(Country,Region,ProcOut) "Calculate grassland required for livestock consumption and convert from FM to DM per country, region and ProcOut product in Mg per year";
         eGrassAvailable(CR,ProcOutG)..
         vProcOutMg(CR,ProcOutG) =E=
                 SUM((APS,level,HerdType,FeedCat)$(APSHerdType(APS,Level,HerdType) AND AniFeedSuitability(APS,FeedCat,ProcOutG)) , vAniFeedIntakeHerdTypeMgDM(CR,APS,Level,HerdType,FeedCat,ProcOutG) * (1 + AniFeedLossFrac(FeedCat,FeedLoss,ProcOutG)) * (AniNutrByProduct(FeedCat,ProcOutG,'FM') * 0.001))
;
EQUATION eResidueAvailable(Country,Region,CropA) "Calculate crop residues available for fertilisation per country, region and crop in Mg per year";
         eResidueAvailable(CR,CropA)..
                 SUM((LandSoil)$CropSuitability(CR,LandSoil,CropA),  vNumha(CR,LandSoil,CropA) * CropYieldha(CR,LandSoil,CropA) * CropOther(CropA,'Residue')) * ScenarioControlsModel('Util.Residue') =E=
                 SUM((LandSoil,Crop)$(CropSuitability(CR,LandSoil,Crop) AND CropFertSuitability(Crop,'Residue') AND  FertCatAllProd('Residue',CropA)), vFertProdMg(CR,LandSoil,Crop,'Residue',CropA))
;
EQUATION eFoodWasteAvailable(Country,Region,FWCat,Waste) "Calcualte food waste mix products to be composted or exported to another region for livestock consumption per country, region, food waste category and food waste product in Mg per year";
         eFoodWasteAvailable(CR,FWCat,Waste)$FWCatWaste(FWCat,Waste)..
         SUM((ProcOutH)$WasteTypeCatProd(FWCat,ProcOutH,Waste), vFoodWasteMg(CR,FWCat,ProcOutH)) * ScenarioControlsModel('Util.FoodWaste') =E=
            SUM((CRA)$ImportBoundary(CR,CRA,FWCat), vFLWFeedRegionImportExportMg(CR,CRA,FWCat,Waste)) +
            vCompostFoodWasteMg(CR,FWCat,Waste)          
;
EQUATION eFoodLossAvailable(Country,Region,FLCat,AllProd) "Calculate food loss products to be composted or exported to another region for livestock consumption per country, region, food loss category and food loss product in Mg per year";
         eFoodLossAvailable(CR,FLCat,APWaste)$FLCatAPWaste(FLCat,APWaste)..
         vFoodLossMg(CR,FLCat,APWaste) * ScenarioControlsModel('Util.FoodLoss') =E=
            SUM((CRA)$ImportBoundary(CR,CRA,FLCat), vFLWFeedRegionImportExportMg(CR,CRA,FLCat,APWaste)) +
            vCompostFoodLossMg(CR,FLCat,APWaste)
;
EQUATION eAniFoodWasteIntakeFM(Country,Region,CountryA,RegionA,FWCat,Waste) "Calculate food waste products required for livestock consumption and convert from FM to DM per country, region, food waste category and food waste product in Mg per year";
         eAniFoodWasteIntakeFM(CR,CRA,FWCat,Waste)$(FWCatWaste(FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat))..
         vFLWFeedRegionImportExportMg(CR,CRA,FWCat,Waste) =E= SUM((APS,Level,HerdType)$(APSHerdType(APS,Level,HerdType) AND AniFeedSuitability(APS,FWCat,Waste)),  vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,APS,Level,HerdType,FWCat,Waste) *  (1 + AniFeedLossFrac(FWCat,FeedLoss,Waste)) * (AniNutrByProductCR(CR,FWCat,Waste,'FM') * 0.001))
;
EQUATION eMaxFoodWasteFeed(Country,Region,FWCat,Waste) "Constraint, maximum utilisiation of food waste products (based on exporting region) for livestock feed per country, region, food waste category and food waste product in Mg per year";
         eMaxFoodWasteFeed(CR,FWCat,Waste)$FWCatWaste(FWCat,Waste)..
         SUM((ProcOutH)$WasteTypeCatProd(FWCat,ProcOutH,Waste), vFoodWasteMg(CR,FWCat,ProcOutH)) * ScenarioControlsModel('Util.FoodWasteAni') =G= SUM((CRA)$ImportBoundary(CR,CRA,FWCat), vFLWFeedRegionImportExportMg(CR,CRA,FWCat,Waste))
;
EQUATION eAniFoodLossIntakeFM(CountryA,RegionA,FLCat,AllProd) "Calculate food loss products required for livestock consumption and convert from FM to DM per country, region, food loss category and food loss product in Mg per year";
         eAniFoodLossIntakeFM(CRA,FLCat,APWaste)$FLCatAPWaste(FLCat,APWaste)..
         SUM((CR)$ImportBoundary(CR,CRA,FLCat), vFLWFeedRegionImportExportMg(CR,CRA,FLCat,APWaste)) =E= SUM((APS,Level,HerdType)$(APSHerdType(APS,Level,HerdType) AND AniFeedSuitability(APS,FLCat,APWaste)),  vAniFeedIntakeHerdTypeMgDM(CRA,APS,Level,HerdType,FLCat,APWaste) *  (1 + AniFeedLossFrac(FLCat,FeedLoss,APWaste)) * (AniNutrByProduct(FLCat,APWaste,'FM') * 0.001))
;
EQUATION eCompost(Country,Region,Compost) "Calculate nutrients in compost and export to another region per country, region and compost in Mg NPK per year";
         eCompost(Country,Region,Compost)$CR(Country,Region)..
         SUM((CRA)$ImportBoundary(Country,Region,CRA,'Compost'), vCompostRegionImportExportMg(Country,Region,CRA,Compost)) =E=
                  SUM(FertNutr,
                 (SUM((FLCat,APWaste)$CompostTypeCatProd(FLCat,APWaste,Compost),           vCompostFoodLossMg(Country,Region,FLCat,APWaste)          * (AniNutrByProduct(FLCat,APWaste,'DM') * 0.001)                         * (AniNutrByProduct(FLCat,APWaste,FertNutr) * 0.001 )) +
                  SUM((FWCat,Waste)$CompostTypeCatProd(FWCat,Waste,Compost),               vCompostFoodWasteMg(Country,Region,FWCat,Waste)           * (AniNutrByProductCR(Country,Region,FWCat,Waste,'DM') * 0.001)          * (AniNutrByProductCR(Country,Region,FWCat,Waste,FertNutr) * 0.001)) +
                  SUM((ProcOutCP)$CompostTypeCatProd('CoProduct',ProcOutCP,Compost),       vCompostCoProductMg(Country,Region,'CoProduct',ProcOutCP) * (AniNutrByProduct('CoProduct',ProcOutCP,'DM') * 0.001)                 * (AniNutrByProduct('CoProduct',ProcOutCP,FertNutr) * 0.001))) *
                  (1 - EFC(Country,Compost,FertNutr)))
;
EQUATION eCompostAvailable(Country,Region,CountryA,RegionA,Compost) "Calculate compost nutrients available for fertilisation per country, region and crop in Mg NPK per year";
         eCompostAvailable(CR,CRA,Compost)$ImportBoundary(CR,CRA,'Compost')..
         vCompostRegionImportExportMg(CR,CRA,Compost) =E=
            SUM((LandSoil,Crop)$(CropSuitability(CRA,LandSoil,Crop) AND CropFertSuitability(Crop,'Compost') AND FertCatAllProd('Compost',Compost)), vFertManureCompostNPKMg(CR,CRA,LandSoil,Crop,'Compost',Compost))
;
EQUATION eFeedingWasteAvailable(CountryA,RegionA) "Calculate feeding losses from livestock available for fertilisation per country and region in Mg NPK per year";
         eFeedingWasteAvailable(CRA)..
         SUM((APS,Level,HerdType,FertNutr)$APSHerdType(APS,Level,HerdType),
         SUM((FeedCat,FeedProd)$AniFeedSuitability(APS,FeedCat,FeedProd),                                       vAniFeedIntakeHerdTypeMgDM(CRA,APS,Level,HerdType,FeedCat,FeedProd)        * AniFeedLossFrac(FeedCat,FeedLoss,FeedProd)  * (AniNutrByProduct(FeedCat,FeedProd,FertNutr) * 0.001))   +
         SUM((CR,FWCat,Waste)$(AniFeedSuitability(APS,FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)),           vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,APS,Level,HerdType,FWCat,Waste)     * AniFeedLossFrac(FWCat,FeedLoss,Waste)       * (AniNutrByProductCR(CR,FWCat,Waste,FertNutr) * 0.001))) =E=
                  SUM((LandSoil,Crop)$(CropSuitability(CRA,LandSoil,Crop) AND CropFertSuitability(Crop,FeedLoss) AND FertCatAllProd(FeedLoss,FeedWaste)),    vFertOtherNPKMg(CRA,LandSoil,Crop,FeedLoss,FeedWaste))
;
EQUATION eManureMMAvailable(Country,Region,CountryA,RegionA,ManureProd) "Calculate manure (manure management system) imports and therefore manure available for fertilisation per export country, export region, import country, import region, and manure product in Mg NPK per year";
         eManureMMAvailable(CR,CRA,ManureProd)$ImportBoundary(CR,CRA,'Manure')..
         SUM((APSManure)$APSManureProd(ManureProd,APSManure), vManureRegionImportExportMg(CR,CRA,APSManure)) =E=
                 SUM((LandSoil,Crop)$(CropSuitability(CRA,LandSoil,Crop) AND CropFertSuitability(Crop,'Manure') AND FertCatAllProd('Manure',ManureProd)), vFertManureCompostNPKMg(CR,CRA,LandSoil,Crop,'Manure',ManureProd))
;
EQUATION eManureGAvailable(Country,Region,ManureProd) "Calculate grazing manure excreted in grassland per country, region and manure product in Mg NPK per year";
         eManureGAvailable(CR,ManureProd)..
         SUM((Rumi,FertNutr)$APSManureProd(ManureProd,Rumi), vAniManureGMg(CR,Rumi,FertNutr)) =E=
                SUM((LandSoil,Crop)$(CropSuitability(CR,LandSoil,Crop) AND CropFertSuitability(Crop,'GrazingManure') AND FertCatAllProd('GrazingManure',ManureProd)), vFertOtherNPKMg(CR,LandSoil,Crop,'GrazingManure',ManureProd))
;
EQUATION eTransportQuantity(Country,Region,CountryA,RegionA) "Calaculate the quantity of products imported and exported between regions per export country, export region, import country, import region in Mg per year";
         eTransportQuantity(CR,CRA)..
         vTransportMg(CR,CRA) =E=
                 SUM((ProcOutH)$ImportBoundary(CR,CRA,'ProcOutH'),                                 vProcOutRegionImportExportMg(CR,CRA,ProcOutH))                                                                               +
                 SUM((ProcOutCP)$ImportBoundary(CR,CRA,'CoProduct'),                               vProcOutRegionImportExportMg(CR,CRA,ProcOutCP))                                                                              +
                 SUM((ProcOutFF)$ImportBoundary(CR,CRA,'FoodFeed'),                                vProcOutRegionImportExportMg(CR,CRA,ProcOutFF))                                                                              +
                 SUM((FWCat,Waste)$(ImportBoundary(CR,CRA,FWCat) AND FLCatAPWaste(FWCat,Waste)),   vFLWFeedRegionImportExportMg(CR,CRA,FWCat,Waste))                                                                            +
                 SUM((FLCat,APWaste)$(ImportBoundary(CR,CRA,FLCat) AND FWCatWaste(FLCat,APWaste)), vFLWFeedRegionImportExportMg(CR,CRA,FLCat,APWaste))                                                                          +
                 SUM((Compost)$ImportBoundary(CR,CRA,'Compost'),                                   vCompostRegionImportExportMg(CR,CRA,Compost)                 *  (NPKRatioCR(CR,'Compost',Compost,'P')  / 0.035))             +
                 SUM((ImportExport,ProcIn)$ProcInImportExportSuitability(CR,ImportExport,ProcIn),  vProcInRegionImportExportMg(CR,CRA,ImportExport,ProcIn))                                                                     +
                 SUM((APSManure)$ImportBoundary(CR,CRA,'Manure'),                                  vManureRegionImportExportMg(CR,CRA,APSManure)                *  (SUM((ManureProd)$APSManureProd(ManureProd,APSManure), NPKRatioCR(CR,'Manure',ManureProd,'P')) / AniOther(APSManure,'PtoManure')))
; 
****** Animal Equations ******
EQUATION eAniNutrPEReq(CountryA,RegionA,APS,Level,HerdType,AniPE) "Constraint, ensure livestock (livestock place) protein and energy requirements are met per country, region, animal production system, productivity level, HerdType and animal nutrient in various units per year";
         eAniNutrPEReq(CRA,APS,Level,HerdType,AniPE)$(APSHerdType(APS,Level,HerdType) AND AniNutrReq(APS,Level,HerdType,AniPE))..
         SUM((FeedCat,FeedProd)$AniFeedSuitability(APS,FeedCat,FeedProd),                              vAniFeedIntakeHerdTypeMgDM(CRA,APS,Level,HerdType,FeedCat,FeedProd)       * AniNutrByProduct(FeedCat,FeedProd,AniPE))  +
         SUM((CR,FWCat,Waste)$(AniFeedSuitability(APS,FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)),  vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,APS,Level,HerdType,FWCat,Waste)    * AniNutrByProductCR(CR,FWCat,Waste,AniPE)) =G=
                 AniNutrReq(APS,Level,HerdType,AniPE) * vNumHerdType(CRA,APS,Level,HerdType) 
;
EQUATION eAniNutrFICReq(CountryA,RegionA,Mono,Level,HerdType) "Constraint, ensure monogastric (livestock place) feed intake caapcity is not exceeded per country, region, animal production system, productivity level and HerdType in Mg FM per year" ;
         eAniNutrFICReq(CRA,Mono,Level,HerdType)$(APSHerdType(Mono,Level,HerdType) AND AniNutrReq(Mono,Level,HerdType,'FIC'))..
         SUM((FeedCat,FeedProd)$AniFeedSuitability(Mono,FeedCat,FeedProd),                             vAniFeedIntakeHerdTypeMgDM(CRA,Mono,Level,HerdType,FeedCat,FeedProd)     * (AniNutrByProduct(FeedCat,FeedProd,'FM') * 0.001))  +
         SUM((CR,FWCat,Waste)$(AniFeedSuitability(Mono,FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)), vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,Mono,Level,HerdType,FWCat,Waste)  * (AniNutrByProductCR(CR,FWCat,Waste,'FM') * 0.001)) =L=
                 AniNutrReq(Mono,Level,HerdType,'FIC') * vNumHerdType(CRA,Mono,Level,HerdType) * 1.05
;
EQUATION eAniNutrVWReq(CountryA,RegionA,Rumi,Level,HerdType) "Constraint, ensure ruminant (livestock place) feed intake caapcity is not exceeded per country, region, animal production system, productivity level and HerdType in Mg VW per year";
         eAniNutrVWReq(CRA,Rumi,Level,HerdType)$(APSHerdType(Rumi,Level,HerdType) AND AniNutrReq(Rumi,Level,HerdType,'VW'))..
         SUM((FeedCat,FeedProd)$AniFeedSuitability(Rumi,FeedCat,FeedProd),                             vAniFeedIntakeHerdTypeMgDM(CRA,Rumi,Level,HerdType,FeedCat,FeedProd)     * AniNutrByProduct(FeedCat,FeedProd,'VW'))  +
         SUM((CR,FWCat,Waste)$(AniFeedSuitability(Rumi,FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)), vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,Rumi,Level,HerdType,FWCat,Waste)  * AniNutrByProductCR(CR,FWCat,Waste,'VW')) =L=
                AniNutrReq(Rumi,Level,HerdType,'VW') * vNumHerdType(CRA,Rumi,Level,HerdType) 
;
EQUATION eAniNutrSWReq(CountryA,RegionA,Rumi,Level,HerdType) "Constraint, ensure ruminant (livestock place) satiety is met per country, region, animal production system, productivity level and HerdType in Mg per Mg intake";
         eAniNutrSWReq(CRA,Rumi,Level,HerdType)$(APSHerdType(Rumi,Level,HerdType) AND AniNutrReq(Rumi,Level,HerdType,'SW'))..
         SUM((FeedCat,FeedProd)$AniFeedSuitability(Rumi,FeedCat,FeedProd),                             vAniFeedIntakeHerdTypeMgDM(CRA,Rumi,Level,HerdType,FeedCat,FeedProd)    * (AniNutrByProduct(FeedCat,FeedProd,'SW') - AniNutrReq(Rumi,Level,HerdType,'SW')))    +
         SUM((CR,FWCat,Waste)$(AniFeedSuitability(Rumi,FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)), vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,Rumi,Level,HerdType,FWCat,Waste) * (AniNutrByProductCR(CR,FWCat,Waste,'SW') - AniNutrReq(Rumi,Level,HerdType,'SW')))   =G=
            0
;
EQUATION eAniNutrOilCap(Country,Region,APS,Level,HerdType) "Constraint, ensure oil consumption does not exceed XX% per country, region, animal production system, productivity level and HerdType";
         eAniNutrOilCap(CRA,APS,Level,HerdType)$APSHerdType(APS,Level,HerdType)..
        (SUM((FeedCat,FeedProd)$AniFeedSuitability(APS,FeedCat,FeedProd),                              vAniFeedIntakeHerdTypeMgDM(CRA,APS,Level,HerdType,FeedCat,FeedProd))     +
         SUM((CR,FWCat,Waste)$(AniFeedSuitability(APS,FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)),  vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,APS,Level,HerdType,FWCat,Waste))) * 0.02 -
         SUM((FeedCat,OilFeed)$AniFeedSuitability(APS,FeedCat,OilFeed),                                vAniFeedIntakeHerdTypeMgDM(CRA,APS,Level,HerdType,FeedCat,OilFeed))     =G=
            0
;
EQUATION eAniNutrAACap(CountryA,RegionA,Mono,Level,HerdType) "Constraint, ensure synthetic amino acid consumption does not exceed XX% per country, region, animal production system, productivity level and HerdType";
         eAniNutrAACap(CRA,Mono,Level,HerdType)$APSHerdType(Mono,Level,HerdType)..
        (SUM((FeedCat,FeedProd)$AniFeedSuitability(Mono,FeedCat,FeedProd),                              vAniFeedIntakeHerdTypeMgDM(CRA,Mono,Level,HerdType,FeedCat,FeedProd))     +
         SUM((CR,FWCat,Waste)$(AniFeedSuitability(Mono,FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)),  vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,Mono,Level,HerdType,FWCat,Waste))) * 0.01 -
         SUM((ProcOutAA)$AniFeedSuitability(Mono,'AminoAcid',ProcOutAA),                                vAniFeedIntakeHerdTypeMgDM(CRA,Mono,Level,HerdType,'AminoAcid',ProcOutAA)) =G=
            0
;
EQUATION eAAFed(Country,Region,AllProd) "Calculate the quantity of synthetic amino acids consumed by livestock per country, region and sythetic amino acids in Mg per year";
         eAAFed(CR,ProcOutAA)..
         vAminoAcidMg(CR,'AminoAcid',ProcOutAA) =E= SUM((Mono,Level,HerdType)$(APSHerdType(Mono,Level,HerdType) AND AniFeedSuitability(Mono,'AminoAcid',ProcOutAA)), vAniFeedIntakeHerdTypeMgDM(CR,Mono,Level,HerdType,'AminoAcid',ProcOutAA))
;
EQUATION eNumHerdType(Country,Region,APS,Level,HerdType) "Calculate the number of HerdTypes (e.g., youngstock or parent stock) required per producer HerdType per year" ;
         eNumHerdType(CR,APS,Level,HerdType)$(APSHerdType(APS,Level,HerdType) and not Producer(APS,HerdType))..
         vNumHerdType(CR,APS,Level,HerdType) =E= SUM((HerdTypeA)$Producer(APS,HerdTypeA), vNumHerdType(CR,APS,Level,HerdTypeA)$APSHerdType(APS,Level,HerdTypeA)) * AniHerdTypePerPU(APS,Level,HerdType)
;
EQUATION eAniNumReference(Country,Region,APS) "Number of animals is equal to current animals in the reference scenario per country and region" ;
         eAniNumReference(CR,APS)..
         SUM((level,HerdType)$APSHerdType(APS,Level,HerdType), vNumHerdType(CR,APS,Level,HerdType)) =E= AniReference(CR,APS)
;
EQUATION eMMEAvailable(Country,Region,MeatMilkEggs) "Calculate the quantity of animal products available per country, region and animal product in Mg per year" ;
         eMMEAvailable(CR,MeatMilkEggs)..
         vMeatMilkEggsMg(CR,MeatMilkEggs) =E= SUM((APSHerdType)$AniYield(APSHerdType,MeatMilkEggs), AniYield(APSHerdType,MeatMilkEggs) * vNumHerdType(CR,APSHerdType))
;
EQUATION eAniNutrientIntake(CountryA,RegionA,APS,Level,HerdType,FertNutr) "Calculate the intake of fertiliser nutrients by livestock per country, region, animal production system, productivity level, HerdType and fertiliser nutrient in Mg per year" ;
         eAniNutrientIntake(CRA,APS,Level,HerdType,FertNutr)$APSHerdType(APS,Level,HerdType)..
         vAniNutrIntakeMg(CRA,APS,Level,HerdType,FertNutr) =E=
                 SUM((FeedCat,FeedProd)$AniFeedSuitability(APS,FeedCat,FeedProd),                             vAniFeedIntakeHerdTypeMgDM(CRA,APS,Level,HerdType,FeedCat,FeedProd)    * (AniNutrByProduct(FeedCat,FeedProd,FertNutr) * 0.001))  +
                 SUM((CR,FWCat,Waste)$(AniFeedSuitability(APS,FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)), vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,APS,Level,HerdType,FWCat,Waste) * (AniNutrByProductCR(CR,FWCat,Waste,FertNutr) * 0.001))
;
EQUATION eAniNutrRet(Country,Region,APS,Level,HerdType,FertNutr) "Calculate fertiliser nutrients retained in meat milk eggs per country, region, animal production system, productivity level, HerdType and fertiliser nutrient in Mg per year";
         eAniNutrRet(CR,APSHerdType,FertNutr)..
         vAniNutrRetMg(CR,APSHerdType,FertNutr) =E= AniNutrRetMg(APSHerdType,FertNutr) * vNumHerdType(CR,APSHerdType)
;
EQUATION eAniNutrExc(Country,Region,APS,Level,HerdType,FertNutr) "Calculate fertiliser nutrient excretion from livestock per country, region, animal production system, productivity level, HerdType and fertiliser nutrient in Mg per year";
         eAniNutrExc(CR,APSHerdType,FertNutr)..
         vAniNutrExcMg(CR,APSHerdType,FertNutr) =E= vAniNutrIntakeMg(CR,APSHerdType,FertNutr) - vAniNutrRetMg(CR,APSHerdType,FertNutr)
;
EQUATION eAniManureMM(Country,Region,APS,Level,HerdType,FertNutr) "Calculate fertilisier nutrients excreted from livestock in a manure management system per country, region, animal production system, productivity level, HerdType and fertiliser nutrient in Mg per year";
         eAniManureMM(CR,APSHerdType,FertNutr)..
         vAniNutrExcMg(CR,APSHerdType,FertNutr) * (1 - GrazingProportionCR(CR,APSHerdType)) =E= vAniManureMMMg(CR,APSHerdType,FertNutr)
;
EQUATION eAniManureG(Country,Region,Rumi,FertNutr) "Calculate fertilisier nutrients excreted from livestock during grazing per country, region, animal production system, productivity level, HerdType and fertiliser nutrient in Mg per year";
         eAniManureG(CR,Rumi,FertNutr)..
         SUM((Level,HerdType)$APSHerdType(Rumi,Level,HerdType), vAniNutrExcMg(CR,Rumi,Level,HerdType,FertNutr) - vAniManureMMMg(CR,Rumi,Level,HerdType,FertNutr)) =E= vAniManureGMg(CR,Rumi,FertNutr)
;
EQUATION eAniManureMMLosses(Country,Region,APS) "Calculate fertiliser nutrients available in manure management after subtracting niutrogen losses and export to region of fertilisation per country, region, and animal production system with manure management in Mg NPK per year";
         eAniManureMMLosses(CR,APSManure)..
         SUM((CRA)$ImportBoundary(CR,CRA,'Manure'), vManureRegionImportExportMg(CR,CRA,APSManure)) =E= SUM(FertNutr, SUM((Level,HerdType)$APSHerdType(APSManure,Level,HerdType), vAniManureMMMg(CR,APSManure,Level,HerdType,FertNutr) * ReferenceManure(FertNutr) - vManureNLossesMg(CR,APSManure,Level,HerdType,FertNutr))) * ScenarioControlsModel('Util.Manure')
;
$iftheni "FixedGrazingProportion" == "on" 
EQUATION eGrazingProportionFixed(CountryA,RegionA,Rumi,Level,HerdType) "Constraint, maximum quantity of fresh grass ruminant diets can contain per country, region, rumamnt production system, productivity level and HerdType in Mg DM per year";
         eGrazingProportionFixed(CRA,Rumi,Level,HerdType)$(APSHerdType(Rumi,Level,HerdType) AND (not sameas ('VealCalf',HerdType)))..
        (SUM((FeedCat,FeedProd)$AniFeedSuitability(Rumi,FeedCat,FeedProd),                             vAniFeedIntakeHerdTypeMgDM(CRA,Rumi,Level,HerdType,FeedCat,FeedProd))     +
         SUM((CR,FWCat,Waste)$(AniFeedSuitability(Rumi,FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)), vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,Rumi,Level,HerdType,FWCat,Waste))) * GrazingProportionCR(CRA,Rumi,Level,Herdtype) =E=
                SUM((FreshGrass)$AniFeedSuitability(Rumi,'GrassFresh',FreshGrass), vAniFeedIntakeHerdTypeMgDM(CRA,Rumi,Level,HerdType,'GrassFresh',FreshGrass))
;
$else
EQUATION eGrazingMax(CountryA,RegionA,Rumi,Level,HerdType) "Constraint, maximum quantity of fresh grass ruminant diets can contain per country, region, rumamnt production system, productivity level and HerdType in Mg DM per year";
         eGrazingMax(CRA,Rumi,Level,HerdType)$(APSHerdType(Rumi,Level,HerdType) AND (not sameas ('VealCalf',HerdType)))..
        (SUM((FeedCat,FeedProd)$AniFeedSuitability(Rumi,FeedCat,FeedProd),                             vAniFeedIntakeHerdTypeMgDM(CRA,Rumi,Level,HerdType,FeedCat,FeedProd))     +
         SUM((CR,FWCat,Waste)$(AniFeedSuitability(Rumi,FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)), vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,Rumi,Level,HerdType,FWCat,Waste))) * ScenarioControlsModel('MaxGrazingProportion') =G=
                SUM((FreshGrass)$AniFeedSuitability(Rumi,'GrassFresh',FreshGrass), vAniFeedIntakeHerdTypeMgDM(CRA,Rumi,Level,HerdType,'GrassFresh',FreshGrass))
;
EQUATION eGrazingMin(Country,Region,Rumi,Level,HerdType) "Constraint, minimum quantity of fresh grass ruminant diets can contain per country, region, rumamnt production system, productivity level and HerdType in Mg DM per year";
         eGrazingMin(CRA,Rumi,Level,HerdType)$(APSHerdType(Rumi,Level,HerdType) AND (not sameas ('VealCalf',HerdType)))..
        (SUM((FeedCat,FeedProd)$AniFeedSuitability(Rumi,FeedCat,FeedProd),                             vAniFeedIntakeHerdTypeMgDM(CRA,Rumi,Level,HerdType,FeedCat,FeedProd))     +
         SUM((CR,FWCat,Waste)$(AniFeedSuitability(Rumi,FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)), vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,Rumi,Level,HerdType,FWCat,Waste))) * ScenarioControlsModel('MinGrazingProportion') =L=
                SUM((FreshGrass)$AniFeedSuitability(Rumi,'GrassFresh',FreshGrass), vAniFeedIntakeHerdTypeMgDM(CRA,Rumi,Level,HerdType,'GrassFresh',FreshGrass))
;
$endif
****** Human Diet Equations ******
EQUATION eHumNutrRIntake(Country,Region,HumNutrR,Month) "Calculate the quantity of human nutrients contained in the human diet per country, region, human nutrient and mounth in various units per month" ;
         eHumNutrRIntake(CR,HumNutrR,Month)..
         vHumNutrRIntake(CR,HumNutrR,Month) =E= SUM((ProcOutH)$HumFoodMonth(ProcOutH,Month), vProcoutHMonthMg(CR,ProcOutH,Month) * HumNutrByProduct(ProcOutH,HumNutrR))
;
EQUATION eHumNutrRIntakeMin(Country,Region,HumNutrR,Month) "Constraint, minimum nutrients contained in the human diet per country, region, human nutrients and month in various units per month" ;
         eHumNutrRIntakeMin(Country,Region,HumNutrR,Month)$(CR(Country,Region) AND SUM((Gender,Age), HumNutrReq(Country,Gender,Age,'Min',HumNutrR)))..
         vHumNutrRIntake(Country,Region,HumNutrR,Month) =G= SUM((Gender,Age), Population(Country,Region,Gender,Age) * HumNutrReq(Country,Gender,Age,'Min',HumNutrR)) * 30.4167 * 0.1
;
EQUATION eHumNutrRIntakeMax(Country,Region,HumNutrR,Month) "Constraint, maximum nutrients contained in the human diet per country, region, human nutrients and month in various units per month" ;
         eHumNutrRintaKeMax(Country,Region,HumNutrR,Month)$(CR(Country,Region) AND SUM((Gender,Age), HumNutrReq(Country,Gender,Age,'Max',HumNutrR)))..
         vHumNutrRIntake(Country,Region,HumNutrR,Month) =L= SUM((Gender,Age), Population(Country,Region,Gender,Age) * HumNutrReq(Country,Gender,Age,'Max',HumNutrR)) * 30.4167 * 0.1
;
EQUATION eHumIntake(Country,Region,Fam,Month) "Calculate consumption of food products per food family and food product per country, region, food family, food product and month in Mg per month" ;
         eHumIntake(CR,Fam,Month)..
         vHumFoodFamilyIntakeMg(CR,Fam,Month) =E= SUM((ProcOutH)$(FamProcOutH(Fam,ProcOutH) AND HumFoodMonth(ProcOutH,Month)), vProcoutHMonthMg(CR,ProcOutH,Month))
;
EQUATION eHumIntakeMinTotal(Country,Region,Fam,Month) "Constraint, minimum intake of each food group per country, region, food family and month in Mg per month" ;
         eHumIntakeMinTotal(Country,Region,Fam,Month)$(CR(Country,Region) AND HumIntakeReq(Country,'Total','Min',Fam))..
         vHumFoodFamilyIntakeMg(Country,Region,Fam,Month) =G= SUM((Gender,Age), Population(Country,Region,Gender,Age)) * HumIntakeReq(Country,'Total','Min',Fam) * 30.4167 * 0.001
;
EQUATION eHumIntakeMaxTotal(Country,Region,Fam,Month) "Constraint, maximum intake of each food group per country, region, food family and month in Mg per month" ;
         eHumIntakeMaxTotal(Country,Region,Fam,Month)$CR(Country,Region)..
         vHumFoodFamilyIntakeMg(Country,Region,Fam,Month) =L= SUM((Gender,Age), Population(Country,Region,Gender,Age)) * HumIntakeReq(Country,'Total','Max',Fam) * 30.4167 * 0.001
;
****** GHG emission equations ******
EQUATION ePigUrineNExcMM(CountryA,RegionA,APS,Level,HerdType) "Calculate urine-N excretion from pigs per country, region, pig production system, productivity level and HerdType in Mg per year";
         ePigUrineNExcMM(CRA,'Pig',Level,HerdType)$APSHerdType('Pig',Level,HerdType)..
         vAniUrineNExcMMMg(CRA,'Pig',Level,HerdType) =E=
               ((SUM((FeedCat,FeedProd)$AniFeedSuitability('Pig',FeedCat,FeedProd),                             (vAniFeedIntakeHerdTypeMgDM(CRA,'Pig',Level,HerdType,FeedCat,FeedProd)     * AniNutrByProduct(FeedCat,FeedProd,'N') * 0.001 * AniNutrByProduct(FeedCat,FeedProd,'DCCPP') * 0.01))  +
                 SUM((CR,FWCat,Waste)$(AniFeedSuitability('Pig',FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)), (vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,'Pig',Level,HerdType,FWCat,Waste)  * AniNutrByProductCR(CR,FWCat,Waste,'N') * 0.001 * AniNutrByProductCR(CR,FWCat,Waste,'DCCPP') * 0.01))) - vAniNutrRetMg(CRA,'Pig',Level,HerdType,'N'))
;
EQUATION ePigTANExcMM(CountryA,RegionA,APS,Level,HerdType) "Calculate TAN excretion from pigs per country, region, pig production system, productivity level and HerdType in Mg per year";
         ePigTANExcMM(CRA,'Pig',Level,HerdType)$APSHerdType('Pig',Level,HerdType)..
         vAniTANExcMMMg(CRA,'Pig',Level,HerdType) =E=
               ((SUM((FeedCat,FeedProd)$AniFeedSuitability('Pig',FeedCat,FeedProd),                             vAniFeedIntakeHerdTypeMgDM(CRA,'Pig',Level,HerdType,FeedCat,FeedProd)     * AniNutrByProduct(FeedCat,FeedProd,'N') * 0.001)  +
                 SUM((CR,FWCat,Waste)$(AniFeedSuitability('Pig',FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)), vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,'Pig',Level,HerdType,FWCat,Waste)  * AniNutrByProductCR(CR,FWCat,Waste,'N') * 0.001)) - vAniNutrRetMg(CRA,'Pig',Level,HerdType,'N') - vAniUrineNExcMMMg(CRA,'Pig',Level,HerdType)) * 0.1 +
                 vAniUrineNExcMMMg(CRA,'Pig',Level,HerdType)
;
EQUATION eRumiUrineNExcMM(CountryA,RegionA,APS,Level,HerdType) "Calculate urine-N excretion from ruminant manure management per country, region, ruminant production system, productivity level and HerdType in Mg per year";
         eRumiUrineNExcMM(CRA,Rumi,Level,HerdType)$APSHerdType(Rumi,Level,HerdType)..
         vAniUrineNExcMMMg(CRA,Rumi,Level,HerdType) =E=
               ((SUM((FeedCat,FeedProd)$AniFeedSuitability(Rumi,FeedCat,FeedProd),                             (vAniFeedIntakeHerdTypeMgDM(CRA,Rumi,Level,HerdType,FeedCat,FeedProd)       * AniNutrByProduct(FeedCat,FeedProd,'N') * 0.001 * AniNutrByProduct(FeedCat,FeedProd,'DCCPR') * 0.01))  +
                 SUM((CR,FWCat,Waste)$(AniFeedSuitability(Rumi,FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)), (vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,Rumi,Level,HerdType,FWCat,Waste)    * AniNutrByProductCR(CR,FWCat,Waste,'N') * 0.001 * AniNutrByProductCR(CR,FWCat,Waste,'DCCPR') * 0.01))) - vAniNutrRetMg(CRA,Rumi,Level,HerdType,'N')) * (1 - GrazingProportionCR(CRA,Rumi,Level,HerdType))
;
EQUATION eRumiTANExcMM(CountryA,RegionA,APS,Level,HerdType) "Calculate TAN excretion from ruminant manure management per country, region, ruminant production system, productivity level and HerdType in Mg per year";
         eRumiTANExcMM(CRA,Rumi,Level,HerdType)$APSHerdType(Rumi,Level,HerdType)..
         vAniTANExcMMMg(CRA,Rumi,Level,HerdType) =E=
               ((SUM((FeedCat,FeedProd)$AniFeedSuitability(Rumi,FeedCat,FeedProd),                             vAniFeedIntakeHerdTypeMgDM(CRA,Rumi,Level,HerdType,FeedCat,FeedProd)       * AniNutrByProduct(FeedCat,FeedProd,'N') * 0.001)  +
                 SUM((CR,FWCat,Waste)$(AniFeedSuitability(Rumi,FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)), vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,Rumi,Level,HerdType,FWCat,Waste)    * AniNutrByProductCR(CR,FWCat,Waste,'N') * 0.001)) - vAniNutrRetMg(CRA,Rumi,Level,HerdType,'N') - vAniUrineNExcMMMg(CRA,Rumi,Level,HerdType)) * (1 - GrazingProportionCR(CRA,Rumi,Level,HerdType)) * 0.1 +
                 vAniUrineNExcMMMg(CRA,Rumi,Level,HerdType)
;
EQUATION eRumiUrineNExcG(Country,RegionA,APS,Level,HerdType) "Calculate urine-N excretion during ruminant grazing per country, region, ruminant production system, productivity level and HerdType in Mg per year";
         eRumiUrineNExcG(CRA,Rumi,Level,HerdType)$APSHerdType(Rumi,Level,HerdType)..
         vAniUrineNExcGMg(CRA,Rumi,Level,HerdType) =E=
               ((SUM((FeedCat,FeedProd)$AniFeedSuitability(Rumi,FeedCat,FeedProd),                             (vAniFeedIntakeHerdTypeMgDM(CRA,Rumi,Level,HerdType,FeedCat,FeedProd)       * AniNutrByProduct(FeedCat,FeedProd,'N') * 0.001 * AniNutrByProduct(FeedCat,FeedProd,'DCCPR') * 0.01))  +
                 SUM((CR,FWCat,Waste)$(AniFeedSuitability(Rumi,FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)), (vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,Rumi,Level,HerdType,FWCat,Waste)    * AniNutrByProductCR(CR,FWCat,Waste,'N') * 0.001 * AniNutrByProductCR(CR,FWCat,Waste,'DCCPR') * 0.01))) - vAniNutrRetMg(CRA,Rumi,Level,HerdType,'N')) * GrazingProportionCR(CRA,Rumi,Level,HerdType)
;
EQUATION eRumiTANExcG(Country,RegionA,APS,Level,HerdType) "Calculate TAN excretion during ruminant grazing per country, region, ruminant production system, productivity level and HerdType in Mg per year";
         eRumiTANExcG(CRA,Rumi,Level,HerdType)$APSHerdType(Rumi,Level,HerdType)..
         vAniTANExcGMg(CRA,Rumi,Level,HerdType) =E=
               ((SUM((FeedCat,FeedProd)$AniFeedSuitability(Rumi,FeedCat,FeedProd),                             vAniFeedIntakeHerdTypeMgDM(CRA,Rumi,Level,HerdType,FeedCat,FeedProd)       * AniNutrByProduct(FeedCat,FeedProd,'N') * 0.001)  +
                 SUM((CR,FWCat,Waste)$(AniFeedSuitability(Rumi,FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)), vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,Rumi,Level,HerdType,FWCat,Waste)    * AniNutrByProductCR(CR,FWCat,Waste,'N') * 0.001)) - vAniNutrRetMg(CRA,Rumi,Level,HerdType,'N') - vAniUrineNExcGMg(CRA,Rumi,Level,HerdType)) * GrazingProportionCR(CRA,Rumi,Level,HerdType) * 0.1 + 
                 vAniUrineNExcGMg(CRA,Rumi,Level,HerdType)
;
EQUATION eLayerUrineNExcMM(Country,RegionA,APS,Level,HerdType) "Calculate urine-N excretion from laying hens per country, region, laying hen production system, productivity level and HerdType in Mg per year";
         eLayerUrineNExcMM(CRA,'Laying_Hen',Level,HerdType)$APSHerdType('Laying_Hen',Level,HerdType)..
         vAniUrineNExcMMMg(CRA,'Laying_Hen',Level,HerdType) =E=
               ((SUM((FeedCat,FeedProd)$AniFeedSuitability('Laying_Hen',FeedCat,FeedProd),                             (vAniFeedIntakeHerdTypeMgDM(CRA,'Laying_Hen',Level,HerdType,FeedCat,FeedProd)    * AniNutrByProduct(FeedCat,FeedProd,'N') * 0.001 * AniNutrByProduct(FeedCat,FeedProd,'DCCPL') * 0.01))  +
                 SUM((CR,FWCat,Waste)$(AniFeedSuitability('Laying_Hen',FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)), (vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,'Laying_Hen',Level,HerdType,FWCat,Waste) * AniNutrByProductCR(CR,FWCat,Waste,'N') * 0.001 * AniNutrByProductCR(CR,FWCat,Waste,'DCCPL') * 0.01))) - vAniNutrRetMg(CRA,'Laying_Hen',Level,HerdType,'N'))
;
EQUATION eLayerTANExcMM(Country,RegionA,APS,Level,HerdType) "Calculate TAN excretion from laying hens per country, region, laying hen production system, productivity level and HerdType in Mg per year";
         eLayerTANExcMM(CRA,'Laying_Hen',Level,HerdType)$APSHerdType('Laying_Hen',Level,HerdType)..
         vAniTANExcMMMg(CRA,'Laying_Hen',Level,HerdType) =E=
               ((SUM((FeedCat,FeedProd)$AniFeedSuitability('Laying_Hen',FeedCat,FeedProd),                             vAniFeedIntakeHerdTypeMgDM(CRA,'Laying_Hen',Level,HerdType,FeedCat,FeedProd)    * AniNutrByProduct(FeedCat,FeedProd,'N') * 0.001)  +
                 SUM((CR,FWCat,Waste)$(AniFeedSuitability('Laying_Hen',FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)), vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,'Laying_Hen',Level,HerdType,FWCat,Waste) * AniNutrByProductCR(CR,FWCat,Waste,'N') * 0.001)) - vAniNutrRetMg(CRA,'Laying_Hen',Level,HerdType,'N') - vAniUrineNExcMMMg(CRA,'Laying_Hen',Level,HerdType)) * 0.1 + 
                 vAniUrineNExcMMMg(CRA,'Laying_Hen',Level,HerdType)
;
EQUATION eBroilerUrineNExcMM(Country,RegionA,APS,Level,HerdType) "Calculate urine-N excretion from broilers per country, region, broiler production system, productivity level and HerdType in Mg per year";
         eBroilerUrineNExcMM(CRA,'Broiler',Level,HerdType)$APSHerdType('Broiler',Level,HerdType)..
         vAniUrineNExcMMMg(CRA,'Broiler',Level,HerdType) =E=
               ((SUM((FeedCat,FeedProd)$AniFeedSuitability('Broiler',FeedCat,FeedProd),                             (vAniFeedIntakeHerdTypeMgDM(CRA,'Broiler',Level,HerdType,FeedCat,FeedProd)       * AniNutrByProduct(FeedCat,FeedProd,'N') * 0.001 * AniNutrByProduct(FeedCat,FeedProd,'DCCPBr') * 0.01))  +
                 SUM((CR,FWCat,Waste)$(AniFeedSuitability('Broiler',FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)), (vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,'Broiler',Level,HerdType,FWCat,Waste)    * AniNutrByProductCR(CR,FWCat,Waste,'N') * 0.001 * AniNutrByProductCR(CR,FWCat,Waste,'DCCPBr') * 0.01))) - vAniNutrRetMg(CRA,'Broiler',Level,HerdType,'N'))
;
EQUATION eBroilerTANExcMM(Country,RegionA,APS,Level,HerdType) "Calculate TAN excretion from broilers per country, region, broiler production system, productivity level and HerdType in Mg per year";
         eBroilerTANExcMM(CRA,'Broiler',Level,HerdType)$APSHerdType('Broiler',Level,HerdType)..
         vAniTANExcMMMg(CRA,'Broiler',Level,HerdType) =E=
               ((SUM((FeedCat,FeedProd)$AniFeedSuitability('Broiler',FeedCat,FeedProd),                             vAniFeedIntakeHerdTypeMgDM(CRA,'Broiler',Level,HerdType,FeedCat,FeedProd)       * AniNutrByProduct(FeedCat,FeedProd,'N') * 0.001)  +
                 SUM((CR,FWCat,Waste)$(AniFeedSuitability('Broiler',FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)), vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,'Broiler',Level,HerdType,FWCat,Waste)    * AniNutrByProductCR(CR,FWCat,Waste,'N') * 0.001)) - vAniNutrRetMg(CRA,'Broiler',Level,HerdType,'N') - vAniUrineNExcMMMg(CRA,'Broiler',Level,HerdType)) * 0.1 + 
                 vAniUrineNExcMMMg(CRA,'Broiler',Level,HerdType)
;
EQUATION eAniN2ONDMM(Country,Region,APSManure,Level,HerdType) "Caculate direct N2O-N emissions from manure management systems per country, region, animal production system, productivity level and HerdType in Mg per year";
         eAniN2ONDMM(Country,Region,APSManure,Level,HerdType)$(CR(Country,Region) AND APSHerdType(APSManure,Level,HerdType))..
         vAniN2ONDMMMg(Country,Region,APSManure,Level,herdType) =E= vAniManureMMMg(Country,Region,APSManure,Level,HerdType,'N') * EFH(Country,HerdType,'N2ODH')
;
EQUATION eAniNOXNMM(Country,Region,APSManure,Level,HerdType) "Calculate NOx-N emissions from manure management systems per country, region, animal production system, productivity level and HerdType in Mg per year";
         eAniNOXNMM(Country,Region,APSManure,Level,HerdType)$(CR(Country,Region) AND APSHerdType(APSManure,Level,HerdType))..
         vAniNOXNMMMg(Country,Region,APSManure,Level,HerdType) =E= vAniManureMMMg(Country,Region,APSManure,Level,HerdType,'N') * EFH(Country,HerdType,'NOXH')
;
EQUATION eAniNH3NMM(Country,Region,APSManure,Level,HerdType) "Calculate NH3-N emissions from manure management systems per country, region, animal production system, productivity level and HerdType in Mg per year";
         eAniNH3NMM(Country,Region,APSManure,Level,HerdType)$(CR(Country,Region) AND APSHerdType(APSManure,Level,HerdType))..
         vAniNH3NMMMg(Country,Region,APSManure,Level,HerdType) =E=  vAniTANExcMMMg(Country,Region,APSManure,Level,HerdType) * EFH(Country,HerdType,'NH3H')
;
EQUATION eAniN2NMM(Country,Region,APSManure,Level,HerdType) "Calculate N2 emissions from manure management systems per country, region, animal production system, productivity level and HerdType in Mg per year";
         eAniN2NMM(Country,Region,APSManure,Level,HerdType)$(CR(Country,Region) AND APSHerdType(APSManure,Level,HerdType))..
         vAniN2NMMMg(Country,Region,APSManure,Level,HerdType) =E=  vAniManureMMMg(Country,Region,APSManure,Level,HerdType,'N') * EFH(Country,HerdType,'N2H')
;
EQUATION eAniN2ONIDMM(Country,Region,APSManure,Level,HerdType) "Calculate in-direct N2O-N emissions from manure management systems per country, region, animal production system, productivity level and HerdType in Mg per year";
         eAniN2ONIDMM(Country,Region,APSManure,Level,HerdType)$(CR(Country,Region) AND APSHerdType(APSManure,Level,HerdType))..
         vAniN2ONIDMMMg(Country,Region,APSManure,Level,HerdType) =E=
                 (vAniNOXNMMMg(Country,Region,APSManure,Level,HerdType) + vAniNH3NMMMg(Country,Region,APSManure,Level,HerdType)) * EFH(Country,HerdType,'N2OIDH')
;
EQUATION eManureNLossesMM(Country,Region,APSManure,Level,HerdType) "Calculate total nitrogen losses from manure management systems per country, region, animal production system, productivity level in Mg per year";
         eManureNLossesMM(CR,APSManure,Level,HerdType)$APSHerdType(APSManure,Level,HerdType)..
         vManureNLossesMg(CR,APSManure,Level,HerdType,'N') =E=
         vAniN2ONDMMMg(CR,APSManure,Level,HerdType) + vAniNOXNMMMg(CR,APSManure,Level,HerdType) + vAniNH3NMMMg(CR,APSManure,Level,HerdType) + vAniN2NMMMg(CR,APSManure,Level,HerdType)
;
vManureNLossesMg.FX(CR,APSHerdType,FertNutrPK)=0;
;
EQUATION ePigVSExcMM(CountryA,RegionA,APS,Level,HerdType) "Calculate volatile solid excretion from pigs per country, region, pig production system, productivity level and HerdType in Mg per year";
         ePigVSExcMM(CRA,'Pig',Level,HerdType)$APSHerdType('Pig',Level,HerdType)..
         vAniVSExcMMkg(CRA,'Pig',Level,HerdType) =E=
                (SUM((CR,FWCat,Waste)$(AniFeedSuitability('Pig',FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)), (vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,'Pig',Level,HerdType,FWCat,Waste) * ((1000 - AniNutrByProductCR(CR,FWCat,Waste,'ASH')) - ((1000 - AniNutrByProductCR(CR,FWCat,Waste,'ASH')) * AniNutrByProductCR(CR,FWCat,Waste,'DCOMP') * 0.01))))  +
                 SUM((FeedCat,FeedProd)$AniFeedSuitability('Pig',FeedCat,FeedProd),                             (vAniFeedIntakeHerdTypeMgDM(CRA,'Pig',Level,HerdType,FeedCat,FeedProd)    * ((1000 - AniNutrByProduct(FeedCat,FeedProd,'ASH')) - ((1000 - AniNutrByProduct(FeedCat,FeedProd,'ASH')) * AniNutrByProduct(FeedCat,FeedProd,'DCOMP') * 0.01))))) * 0.001 +
                 vAniUrineNExcMMMg(CRA,'Pig',Level,Herdtype) * 28/60
;
EQUATION eRumiVSExcMM(CountryA,RegionA,APS,Level,HerdType) "Calculate volatile solid excretion from rumant manure management per country, region, ruminant production system, productivity level and HerdType in Mg per year";
         eRumiVSExcMM(CRA,Rumi,Level,HerdType)$APSHerdType(Rumi,Level,HerdType)..
         vAniVSExcMMkg(CRA,Rumi,Level,HerdType) =E=
                (SUM((CR,FWCat,Waste)$(AniFeedSuitability(Rumi,FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)), (vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,Rumi,Level,HerdType,FWCat,Waste) * ((1000 - AniNutrByProductCR(CR,FWCat,Waste,'ASH')) - ((1000 - AniNutrByProductCR(CR,FWCat,Waste,'ASH')) * AniNutrByProductCR(CR,FWCat,Waste,'DCOMR') * 0.01))))  +
                 SUM((FeedCat,FeedProd)$AniFeedSuitability(Rumi,FeedCat,FeedProd),                             (vAniFeedIntakeHerdTypeMgDM(CRA,Rumi,Level,HerdType,FeedCat,FeedProd)    * ((1000 - AniNutrByProduct(FeedCat,FeedProd,'ASH')) - ((1000 - AniNutrByProduct(FeedCat,FeedProd,'ASH')) * AniNutrByProduct(FeedCat,FeedProd,'DCOMR') * 0.01))))) * 0.001 * (1 - GrazingProportionCR(CRA,Rumi,Level,HerdType)) +
                 vAniUrineNExcMMMg(CRA,Rumi,Level,HerdType) * 28/60
;
EQUATION eRumiVSExcG(CountryA,RegionA,APS,Level,HerdType) "Calculate volatile solid excretion during ruminant grazing per country, region, ruminant production system, productivity level and HerdType in Mg per year";
         eRumiVSExcG(CRA,Rumi,Level,HerdType)$APSHerdType(Rumi,Level,HerdType)..
         vAniVSExcGMg(CRA,Rumi,Level,HerdType) =E=
                (SUM((CR,FWCat,Waste)$(AniFeedSuitability(Rumi,FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)), (vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,Rumi,Level,HerdType,FWCat,Waste) * ((1000 - AniNutrByProductCR(CR,FWCat,Waste,'ASH')) - ((1000 - AniNutrByProductCR(CR,FWCat,Waste,'ASH')) * AniNutrByProductCR(CR,FWCat,Waste,'DCOMR') * 0.01))))  +
                 SUM((FeedCat,FeedProd)$AniFeedSuitability(Rumi,FeedCat,FeedProd),                             (vAniFeedIntakeHerdTypeMgDM(CRA,Rumi,Level,HerdType,FeedCat,FeedProd)    * ((1000 - AniNutrByProduct(FeedCat,FeedProd,'ASH')) - ((1000 - AniNutrByProduct(FeedCat,FeedProd,'ASH')) * AniNutrByProduct(FeedCat,FeedProd,'DCOMR') * 0.01))))) * 0.001 * GrazingProportionCR(CRA,Rumi,Level,HerdType) +
                 vAniUrineNExcGMg(CRA,Rumi,Level,HerdType) * 28/60
;
EQUATION eLayerVSExcMM(CountryA,RegionA,APS,Level,HerdType) "Calculate volatile solid excretion from laying hens per country, region, laying hen production system, productivity level and HerdType in Mg per year";
         eLayerVSExcMM(CRA,'Laying_Hen',Level,HerdType)$APSHerdType('Laying_Hen',Level,HerdType)..
         vAniVSExcMMkg(CRA,'Laying_Hen',Level,HerdType) =E=
                (SUM((CR,FWCat,Waste)$(AniFeedSuitability('Laying_Hen',FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)), (vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,'Laying_Hen',Level,HerdType,FWCat,Waste) * ((1000 - AniNutrByProductCR(CR,FWCat,Waste,'ASH')) - ((AniNutrByProductCR(CR,FWCat,Waste,'CP') * AniNutrByProductCR(CR,FWCat,Waste,'DCCPL') * 0.01) + (AniNutrByProductCR(CR,FWCat,Waste,'CFAT') * AniNutrByProductCR(CR,FWCat,Waste,'DCFATL') * 0.01) + (AniNutrByProductCR(CR,FWCat,Waste,'NFE') * AniNutrByProductCR(CR,FWCat,Waste,'DCNFEL')  * 0.01)))))  +
                 SUM((FeedCat,FeedProd)$AniFeedSuitability('Laying_Hen',FeedCat,FeedProd),                             (vAniFeedIntakeHerdTypeMgDM(CRA,'Laying_Hen',Level,HerdType,FeedCat,FeedProd)    * ((1000 - AniNutrByProduct(FeedCat,FeedProd,'ASH')) - ((AniNutrByProduct(FeedCat,FeedProd,'CP') * AniNutrByProduct(FeedCat,FeedProd,'DCCPL') * 0.01) + (AniNutrByProduct(FeedCat,FeedProd,'CFAT') * AniNutrByProduct(FeedCat,FeedProd,'DCFATL') * 0.01) + (AniNutrByProduct(FeedCat,FeedProd,'NFE') * AniNutrByProduct(FeedCat,FeedProd,'DCNFEL')  * 0.01)))))) * 0.001 +
                 vAniUrineNExcMMMg(CRA,'Laying_Hen',Level,HerdType) * 56/168
;
EQUATION eBroilerVSExcMM(CountryA,RegionA,APS,Level,HerdType) "Calculate volatile solid excretion from broilers per country, region, broiler production system, productivity level and HerdType in Mg per year";
         eBroilerVSExcMM(CRA,'Broiler',Level,HerdType)$APSHerdType('Broiler',Level,HerdType)..
         vAniVSExcMMkg(CRA,'Broiler',Level,HerdType) =E=
                (SUM((CR,FWCat,Waste)$(AniFeedSuitability('Broiler',FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)), (vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,'Broiler',Level,HerdType,FWCat,Waste) * ((1000 - AniNutrByProductCR(CR,FWCat,Waste,'ASH')) - ((AniNutrByProductCR(CR,FWCat,Waste,'CP') * AniNutrByProductCR(CR,FWCat,Waste,'DCCPBr') * 0.01) + (AniNutrByProductCR(CR,FWCat,Waste,'CFAT') * AniNutrByProductCR(CR,FWCat,Waste,'DCFATBr') * 0.01) + (AniNutrByProductCR(CR,FWCat,Waste,'NFE') * AniNutrByProductCR(CR,FWCat,Waste,'DCNFEBr') * 0.01)))))  +
                 SUM((FeedCat,FeedProd)$AniFeedSuitability('Broiler',FeedCat,FeedProd),                             (vAniFeedIntakeHerdTypeMgDM(CRA,'Broiler',Level,HerdType,FeedCat,FeedProd)    * ((1000 - AniNutrByProduct(FeedCat,FeedProd,'ASH')) - ((AniNutrByProduct(FeedCat,FeedProd,'CP') * AniNutrByProduct(FeedCat,FeedProd,'DCCPBr') * 0.01) + (AniNutrByProduct(FeedCat,FeedProd,'CFAT') * AniNutrByProduct(FeedCat,FeedProd,'DCFATBr') * 0.01) + (AniNutrByProduct(FeedCat,FeedProd,'NFE') * AniNutrByProduct(FeedCat,FeedProd,'DCNFEBr') * 0.01)))))) * 0.001 +
                 vAniUrineNExcMMMg(CRA,'Broiler',Level,HerdType) * 56/168
;
EQUATION eAniCH4Manure(Country,Region,APSManure,Level,HerdType) "Calculate methane emissions from manure management and grazing per country, region, animal production system, productivity level and HerdType in Mg per year";
         eAniCH4Manure(Country,Region,APSManure,Level,HerdType)$(CR(Country,Region) AND APSHerdType(APSManure,Level,HerdType))..
         vAniCH4ManureMg(Country,Region,APSManure,Level,HerdType) =E=
                (vAniVSExcMMkg(Country,Region,APSManure,Level,HerdType)  * EFH(Country,HerdType,'MCFH')  * EFH(Country,HerdType,'B0H') * 0.67) +
                (vAniVSExcGMg(Country,Region,APSManure,Level,HerdType)   * EFH(Country,HerdType,'MCFGR') * EFH(Country,HerdType,'B0H') * 0.67)
;
EQUATION eAniCH4Enteric(CountryA,RegionA,Rumi,Level,HerdType) "Calculate methane emissions from enteric fermentation per country, region, animal production system, productivity level and HerdType in Mg per year";
         eAniCH4Enteric(CRA,Rumi,Level,HerdType)$APSHerdType(Rumi,Level,HerdType)..
         vAnivCH4EntericMg(CRA,Rumi,Level,HerdType) =E=
                (SUM((FeedCat,FeedProd)$AniFeedSuitability(Rumi,FeedCat,FeedProd),                             (vAniFeedIntakeHerdTypeMgDM(CRA,Rumi,Level,HerdType,FeedCat,FeedProd)    * AniNutrByProduct(FeedCat,FeedProd,'GE')  * 0.065) / 55.65)) +
                (SUM((CR,FWCat,Waste)$(AniFeedSuitability(Rumi,FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)), (vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,Rumi,Level,HerdType,FWCat,Waste) * AniNutrByProductCR(CR,FWCat,Waste,'GE') * 0.065) / 55.65))
;
EQUATION eAniCO2Energy(Country,Region,APS,Level,HerdType) "Calculate CO2e emissions from energy use on farm of livestock per country, region, animal production system, productivity level and HerdType in Mg per year";
         eAniCO2Energy(CR,APS,Level,HerdType)$APSHerdType(APS,Level,HerdType)..
         vAniCO2EnergyMg(CR,APS,Level,HerdType) =E= SUM(GHG, (vNumHerdType(CR,APS,Level,HerdType) * AniEnergy(APS,Level,HerdType,'kwh')) * GHGOther(GHG,'kwh') * GHGOther(GHG,'eqv'));
;
EQUATION eMarineFishCO2(Country,Region) "Calculate CO2e emissions from capture fisheries per country and region in Mg per year";
         eMarineFishCO2(CR)..
         vMarineFishCO2Mg(CR) =E= SUM((FishSpecies)$MarineFish(CR,FishSpecies), vMarineFishMg(CR,FishSpecies) * SUM(ProcOutH, Processing(FishSpecies,ProcoutH))) * 4.5
;
EQUATION eAniCO2FeedProc(CountryA,RegionA,APS,Level,HerdType) "Calculate CO2e emissions from feed processing per country, region, animal production system, productivity level and HerdType in Mg per year";
         eAniCO2FeedProc(CRA,APS,level,HerdType)$APSHerdType(APS,Level,HerdType)..
         vAniCO2FeedProcMg(CRA,APS,level,HerdType) =E=
                 SUM((FeedCat,FeedProd)$AniFeedSuitability(APS,FeedCat,FeedProd),                                      vAniFeedIntakeHerdTypeMgDM(CRA,APS,Level,HerdType,FeedCat,FeedProd)       * AniFeedProcEF(FeedCat,FeedProd))  +
                 SUM((CR,FWCat,Waste)$(AniFeedSuitability(APS,FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)),          vAniFoodWasteIntakeHerdTypeMgDM(CR,CRA,APS,Level,HerdType,FWCat,Waste)    * AniFeedProcEF(FWCat,Waste))
;
EQUATION eAniCO2eqTotal(Country,Region,APS,Level,HerdType) "Calculate total CO2e emissions from livestock for non-reference scenarios per country, region, animal production system, productivity level and HerdType in Mg per year";
         eAniCO2eqTotal(CR,APS,Level,HerdType)$APSHerdType(APS,Level,HerdType)..
         vAniCO2eqTotalMg(CR,APS,Level,HerdType) =E=
                 ((vAniCH4ManureMg(CR,APS,Level,HerdType)$APSManure(APS) + vAnivCH4EntericMg(CR,APS,Level,HerdType)$Rumi(APS)) * GHGOther('CH4','eqv')) +
                 ((vAniN2ONDMMMg(CR,APS,Level,HerdType) + vAniN2ONIDMMMg(CR,APS,Level,HerdType)) * 44/28 * GHGOther('N2O','eqv'))$APSManure(APS)  +
                   vAniCO2FeedProcMg(CR,APS,level,HerdType) + vAniCO2EnergyMg(CR,APS,Level,HerdType)
;
EQUATION eAniCO2eqTotalReferenece(Country,Region,APS,Level,HerdType) "Calculate total CO2e emissions from livestock for reference scenarios per country, region, animal production system, productivity level and HerdType in Mg per year";
         eAniCO2eqTotalReferenece(CR,APS,Level,HerdType)$APSHerdType(APS,Level,HerdType)..
         vAniCO2eqTotalMg(CR,APS,Level,HerdType) =E=
                 ((vAniCH4ManureMg(CR,APS,Level,HerdType)$APSManure(APS) + vAnivCH4EntericMg(CR,APS,Level,HerdType)$APSManure(APS)) * GHGOther('CH4','eqv')) +
                 ((vAniN2ONDMMMg(CR,APS,Level,HerdType)$APSManure(APS) + vAniN2ONIDMMMg(CR,APS,Level,HerdType)$APSManure(APS)) * 44/28 * GHGOther('N2O','eqv'))
;
EQUATION eCompostN2OND(Country,Region,Compost) "Calculate direct N2O-N emissions from compost per country, region and compost ion Mg per year";
         eCompostN2OND(Country,Region,Compost)$CR(Country,Region)..
         vCompostN2ONDMg(Country,Region,Compost) =E=
               (SUM((FLCat,APWaste)$CompostTypeCatProd(FLCat,APWaste,Compost),     vCompostFoodLossMg(Country,Region,FLCat,APWaste)          * (AniNutrByProduct(FLCat,APWaste,'DM')    * 0.001)                      * (AniNutrByProduct(FLCat,APWaste,'N')    * 0.001))            +
                SUM((FWCat,Waste)$CompostTypeCatProd(FWCat,Waste,Compost),         vCompostFoodWasteMg(Country,Region,FWCat,Waste)           * (AniNutrByProductCR(Country,Region,FWCat,Waste,'DM') * 0.001)          * (AniNutrByProductCR(Country,Region,FWCat,Waste,'N') * 0.001))            +
                SUM((ProcOutCP)$CompostTypeCatProd('CoProduct',ProcOutCP,Compost), vCompostCoProductMg(Country,Region,'CoProduct',ProcOutCP) * (AniNutrByProduct('CoProduct',ProcOutCP,'DM') * 0.001)                 * (AniNutrByProduct('CoProduct',ProcOutCP,'N') * 0.001))) *
                EFC(Country,Compost,'N') * EFC(Country,Compost,'N2OC')
;
EQUATION eCompostCH4(Country,Region,Compost) "Calculate methane emissions from compost per country, region and compost in Mg per year";
         eCompostCH4(Country,Region,Compost)$CR(Country,Region)..
         vCompostCH4Mg(Country,Region,Compost) =E=
               (SUM((FLCat,APWaste)$CompostTypeCatProd(FLCat,APWaste,Compost),      vCompostFoodLossMg(Country,Region,FLCat,APWaste)          * ((AniNutrByProduct(FLCat,APWaste,'DM')                    * 0.001 * 0.425) - (AniNutrByProduct(FLCat,APWaste,'DM')                    * 0.001 * AniNutrByProduct(FLCat,APWaste,'N')                    * 0.001 * (1 - EFC(Country,Compost,'N'))     * EFC(Country,Compost,'CNC'))))      +
                SUM((FWCat,Waste)$CompostTypeCatProd(FWCat,Waste,Compost),          vCompostFoodWasteMg(Country,Region,FWCat,Waste)           * ((AniNutrByProductCR(Country,Region,FWCat,Waste,'DM')     * 0.001 * 0.425) - (AniNutrByProductCR(Country,Region,FWCat,Waste,'DM')     * 0.001 * AniNutrByProductCR(Country,Region,FWCat,Waste,'N')     * 0.001 * (1 - EFC(Country,Compost,'N'))     * EFC(Country,Compost,'CNC'))))      +
                SUM((ProcOutCP)$CompostTypeCatProd('CoProduct',ProcOutCP,Compost),  vCompostCoProductMg(Country,Region,'CoProduct',ProcOutCP) * ((AniNutrByProduct('CoProduct',ProcOutCP,'DM')            * 0.001 * 0.425) - (AniNutrByProduct('CoProduct',ProcOutCP,'DM')            * 0.001 * AniNutrByProduct('CoProduct',ProcOutCP,'N')            * 0.001 * (1 - EFC(Country,'CPCompost','N')) * EFC(Country,'CPCompost','CNC'))))) *
                EFC(Country,Compost,'CH4C')
;
EQUATION eCompostCO2eq(Country,Region,Compost) "Calculate total CO2e emissions from compost per country, region and compost in Mg per year";
         eCompostCO2eq(CR,Compost)..
         vCompostCO2eqMg(CR,Compost) =E=
                (vCompostN2ONDMg(CR,Compost)  * 44/28 * GHGOther('N2O','eqv')) +
                (vCompostCH4Mg(CR,Compost) * 16/12 * GHGOther('CH4','eqv'))
;
EQUATION eCropN2OND(CountryA,RegionA,LandType,SoilType,Crop) "Calculate direct N2O-N emissions from cropland per country, region, land type, soil type and crop in Mg per year";
         eCropN2OND(CountryA,RegionA,LandType,SoilType,Crop)$(CRA(CountryA,RegionA) AND CropSuitability(CountryA,RegionA,LandType,SoilType,Crop) AND (not sameas ('Glass',SoilType)))..
         vCropN2ONDMg(CountryA,RegionA,LandType,SoilType,Crop) =E=
                 SUM((FertProdCat,FertProd)$(CropFertSuitability(Crop,FertProdCat) AND FertCatAllProd(FertProdCat,FertProd)),                                                    vFertProdMg(CountryA,RegionA,LandType,SoilType,Crop,FertProdCat,FertProd)                  * FertNutrByProductMgMg(FertProdCat,FertProd,'N')      * EFS(CountryA,LandType,SoilType,FertProdCat,'N2ODS'))                             +
                 SUM((FertOthCat,FertProd)$(CropFertSuitability(Crop,FertOthCat) AND FertCatAllProd(FertOthCat,FertProd)),                                                       vFertOtherNPKMg(CountryA,RegionA,LandType,SoilType,Crop,FertOthCat,FertProd)               * NPKRatioCR(CountryA,RegionA,FertOthCat,FertProd,'N') * EFS(CountryA,LandType,SoilType,FertOthCat,'N2ODS'))                              +
                 SUM((CR,FertManCat,FertProd)$(CropFertSuitability(Crop,FertManCat) AND FertCatAllProd(FertManCat,FertProd) AND ImportBoundary(CR,CountryA,RegionA,FertManCat)), vFertManureCompostNPKMg(CR,CountryA,RegionA,LandType,SoilType,Crop,FertManCat,FertProd)    * NPKRatioCR(CR,FertManCat,FertProd,'N')               * EFS(CountryA,LandType,SoilType,FertManCat,'N2ODS'))                              +
                 SUM((ResidueN)$CropOther(Crop,ResidueN),                                                                                                                        vNumha(CountryA,RegionA,LandType,SoilType,Crop)                                            * CropOther(Crop,ResidueN)                             * EFS(CountryA,LandType,SoilType,'Residue','N2ODS'))                               +
                (vNumha(CountryA,RegionA,LandType,SoilType,Crop)                                                                                                                                                                                                            * NMineralha(CountryA,Soiltype)                        * EFS(CountryA,LandType,SoilType,'Mineral','N2ODS'))$NMineralha(CountryA,Soiltype) 
;
EQUATION eCropNOXN(CountryA,RegionA,LandType,SoilType,Crop) "Calculate NOX-N emissions from cropland per country, region, land type, soil type and crop in Mg per year";
         eCropNOXN(CountryA,RegionA,LandType,SoilType,Crop)$(CRA(CountryA,RegionA) AND CropSuitability(CountryA,RegionA,LandType,SoilType,Crop) AND (not sameas ('Glass',SoilType)))..
         vCropNOXNMg(CountryA,RegionA,LandType,SoilType,Crop) =E=
                 SUM((FertProdCat,FertProd)$(CropFertSuitability(Crop,FertProdCat) AND FertCatAllProd(FertProdCat,FertProd)),                                                     vFertProdMg(CountryA,RegionA,LandType,SoilType,Crop,FertProdCat,FertProd)                   * FertNutrByProductMgMg(FertProdCat,FertProd,'N')      * EFS(CountryA,LandType,SoilType,FertProdCat,'NOXS'))                             +
                 SUM((FertOthCat,FertProd)$(CropFertSuitability(Crop,FertOthCat) AND FertCatAllProd(FertOthCat,FertProd)),                                                        vFertOtherNPKMg(CountryA,RegionA,LandType,SoilType,Crop,FertOthCat,FertProd)                * NPKRatioCR(CountryA,RegionA,FertOthCat,FertProd,'N') * EFS(CountryA,LandType,SoilType,FertOthCat,'NOXS'))                              +
                 SUM((CR,FertManCat,FertProd)$(CropFertSuitability(Crop,FertManCat) AND FertCatAllProd(FertManCat,FertProd)  AND ImportBoundary(CR,CountryA,RegionA,FertManCat)), vFertManureCompostNPKMg(CR,CountryA,RegionA,LandType,SoilType,Crop,FertManCat,FertProd)     * NPKRatioCR(CR,FertManCat,FertProd,'N')               * EFS(CountryA,LandType,SoilType,FertManCat,'NOXS'))                              +
                 SUM((ResidueN)$CropOther(Crop,ResidueN),                                                                                                                         vNumha(CountryA,RegionA,LandType,SoilType,Crop)                                             * CropOther(Crop,ResidueN)                             * EFS(CountryA,LandType,SoilType,'Residue','NOXS'))                               +
                (vNumha(CountryA,RegionA,LandType,SoilType,Crop)                                                                                                                                                                                                              * NMineralha(CountryA,Soiltype)                        * EFS(CountryA,LandType,SoilType,'Mineral','NOXS'))$NMineralha(CountryA,Soiltype) 
;
EQUATION eCropNH3N(CountryA,RegionA,LandType,SoilType,Crop) "Calculate NH3-N emissions from cropland per country, region, land type, soil type and crop in Mg per year";
         eCropNH3N(CountryA,RegionA,LandType,SoilType,Crop)$(CRA(CountryA,RegionA) AND CropSuitability(CountryA,RegionA,LandType,SoilType,Crop) AND (not sameas ('Glass',SoilType)))..
         vCropNH3NMg(CountryA,RegionA,LandType,SoilType,Crop) =E=
                 SUM((FertProdCat,FertProd)$(CropFertSuitability(Crop,FertProdCat) AND FertCatAllProd(FertProdCat,FertProd)),                                                         vFertProdMg(CountryA,RegionA,LandType,SoilType,Crop,FertProdCat,FertProd)                          * FertNutrByProductMgMg(FertProdCat,FertProd,'N')              * EFS(CountryA,LandType,SoilType,FertProdCat,'TAN')         * EFS(CountryA,LandType,SoilType,FertProdCat,'NH3S'))                             +
                 SUM((CR,FertProd)$(CropFertSuitability(Crop,'Compost') AND FertCatAllProd('Compost',FertProd)  AND ImportBoundary(CR,CountryA,RegionA,'Compost')),                   vFertManureCompostNPKMg(CR,CountryA,RegionA,LandType,SoilType,Crop,'Compost',FertProd)             * NPKRatioCR(CR,'Compost',FertProd,'N')                        * EFS(CountryA,LandType,SoilType,'Compost','TAN')           * EFS(CountryA,LandType,SoilType,'Compost','NH3S'))                               +
                 SUM((FertProd)$(CropFertSuitability(Crop,FeedLoss) AND FertCatAllProd(FeedLoss,FertProd)),                                                                           vFertOtherNPKMg(CountryA,RegionA,LandType,SoilType,Crop,FeedLoss,FertProd)                         * NPKRatioCR(CountryA,RegionA,FeedLoss,FertProd,'N')           * EFS(CountryA,LandType,SoilType,FeedLoss,'TAN')            * EFS(CountryA,LandType,SoilType,FeedLoss,'NH3S'))                                +
                 SUM((ManureProd)$(CropFertSuitability(Crop,'GrazingManure')AND FertCatAllProd('GrazingManure',ManureProd)),                                                          vFertOtherNPKMg(CountryA,RegionA,LandType,SoilType,Crop,'GrazingManure',ManureProd)                * NPKRatioCR(CountryA,RegionA,'GrazingManure',ManureProd,'N')  * ManureTanCR(CountryA,RegionA,'GrazingManure',ManureProd)  * EFS(CountryA,LandType,SoilType,'GrazingManure','NH3S'))                         +
                 SUM((CR,ManureProd)$(CropFertSuitability(Crop,'Manure') AND FertCatAllProd('Manure',ManureProd)  AND ImportBoundary(CR,CountryA,RegionA,'Manure')),                  vFertManureCompostNPKMg(CR,CountryA,RegionA,LandType,SoilType,Crop,'Manure',ManureProd)            * NPKRatioCR(CR,'Manure',ManureProd,'N')                       * ManureTanCR(CR,'Manure',ManureProd)                       * EFS(CountryA,LandType,SoilType,'Manure','NH3S'))                                +
                 SUM((CR,ManureProd)$(CropFertSuitability(Crop,'HumanExcreta')AND FertCatAllProd('HumanExcreta',ManureProd)  AND ImportBoundary(CR,CountryA,RegionA,'HumanExcreta')), vFertManureCompostNPKMg(CR,CountryA,RegionA,LandType,SoilType,Crop,'HumanExcreta',ManureProd)      * NPKRatioCR(CR,'HumanExcreta',ManureProd,'N')                 * EFS(CountryA,LandType,SoilType,'HumanExcreta','TAN')      * EFS(CountryA,LandType,SoilType,'HumanExcreta','NH3S'))                          +
                 SUM((ResidueN)$CropOther(Crop,ResidueN),                                                                                                                             vNumha(CountryA,RegionA,LandType,SoilType,Crop)                                                    * CropOther(Crop,ResidueN)                                                                                                 * EFS(CountryA,LandType,SoilType,'Residue','NH3S'))                               +
                (vNumha(CountryA,RegionA,LandType,SoilType,Crop)                                                                                                                                                                                                                         * NMineralha(CountryA,Soiltype)                                                                                            * EFS(CountryA,LandType,SoilType,'Mineral','NH3S'))$NMineralha(CountryA,Soiltype) 
;
EQUATION eCropNLeach(CountryA,RegionA,LandType,SoilType,Crop) "Calculate N leaching from cropland per country, region, land type, soil type and crop in Mg per year";
         eCropNLeach(CountryA,RegionA,LandType,SoilType,Crop)$(CRA(CountryA,RegionA) AND CropSuitability(CountryA,RegionA,LandType,SoilType,Crop) AND (not sameas ('Glass',SoilType)))..
         vCropNLeachMg(CountryA,RegionA,LandType,SoilType,Crop) =E=
                (SUM((FertProdCat,FertProd)$(CropFertSuitability(Crop,FertProdCat) AND FertCatAllProd(FertProdCat,FertProd)),                                                     vFertProdMg(CountryA,RegionA,LandType,SoilType,Crop,FertProdCat,FertProd)                   * FertNutrByProductMgMg(FertProdCat,FertProd,'N'))             +
                 SUM((FertOthCat,FertProd)$(CropFertSuitability(Crop,FertOthCat)  AND FertCatAllProd(FertOthCat,FertProd)),                                                       vFertOtherNPKMg(CountryA,RegionA,LandType,SoilType,Crop,FertOthCat,FertProd)                * NPKRatioCR(CountryA,RegionA,FertOthCat,FertProd,'N'))        +
                 SUM((CR,FertManCat,FertProd)$(CropFertSuitability(Crop,FertManCat) AND FertCatAllProd(FertManCat,FertProd)  AND ImportBoundary(CR,CountryA,RegionA,FertManCat)), vFertManureCompostNPKMg(CR,CountryA,RegionA,LandType,SoilType,Crop,FertManCat,FertProd)     * NPKRatioCR(CR,FertManCat,FertProd,'N'))                      +
                 SUM((ResidueN)$CropOther(Crop,ResidueN),                                                                                                                         vNumha(CountryA,RegionA,LandType,SoilType,Crop)                                             * CropOther(Crop,ResidueN))                                    +
                (vNumha(CountryA,RegionA,LandType,SoilType,Crop)                                                                                                                                                                                                              * NMineralha(CountryA,Soiltype))$NMineralha(CountryA,Soiltype)) *
                 EFS(CountryA,LandType,SoilType,'Other','Leach')
;
EQUATION eCropN2ONL(Country,Region,LandType,SoilType,Crop) "Calculate N2O-N leaching from cropland per country, region, land type, soil type and crop in Mg per year";
         eCropN2ONL(Country,Region,LandType,SoilType,Crop)$(CR(Country,Region) AND CropSuitability(Country,Region,LandType,SoilType,Crop) AND (not sameas ('Glass',SoilType)))..
         vCropN2ONLMg(Country,Region,LandType,SoilType,Crop) =E= vCropNLeachMg(Country,Region,LandType,SoilType,Crop) * EFS(Country,LandType,SoilType,'Other','N2OIDLS')
;
EQUATION eCropN2ONVol(Country,Region,LandType,SoilType,Crop) "Caclaulet N2O-N volatilisation from cropland per country, region, land type, soil type and crop in Mg per year";
         eCropN2ONVol(Country,Region,LandType,SoilType,Crop)$(CR(Country,Region) AND CropSuitability(Country,Region,LandType,SoilType,Crop) AND (not sameas ('Glass',SoilType)))..
         vCropN2ONVMg(Country,Region,LandType,SoilType,Crop) =E= (vCropNOXNMg(Country,Region,LandType,SoilType,Crop) + vCropNH3NMg(Country,Region,LandType,SoilType,Crop)) * EFS(Country,LandType,SoilType,'Other','N2OIDVS')
;
Equation eCropN2(CountryA,RegionA,LandType,SoilType,Crop) "Calculate N2 emissions from cropland per country, region, land type, soil type and crop in Mg per year";
         eCropN2(CountryA,RegionA,LandType,SoilType,Crop)$(CR(CountryA,RegionA) AND CropSuitability(CountryA,RegionA,LandType,SoilType,Crop) AND (not sameas ('Glass',SoilType))).. 
         vCropN2Mg(CountryA,RegionA,LandType,SoilType,Crop) =E= vCropN2ONDMg(CountryA,RegionA,LandType,SoilType,Crop) * EFS(CountryA,LandType,SoilType,'Other','N2_N2OS')
;
EQUATION eCropGlassCO2eq(Country,Region,LandType,SoilType,Crop) "Calculate CO2e emissions from energy use on farm crop per country, region, land type, soil type and crop in Mg per year";
         eCropGlassCO2eq(CR,'Fixed','Glass',Crop)$CropSuitability(CR,'Fixed','Glass',Crop)..
         vCropGlassCO2eqMg(CR,'Fixed','Glass',Crop) =E= vNumha(CR,'Fixed','Glass',Crop) * CropOther(Crop,'GlassCO2');
;
EQUATION eCropCO2Fuel(Country,Region,LandType,SoilType,Crop) "Calculate CO2e emissions from fuel use on farm crop per country, region, land type, soil type and crop in Mg per year";
         eCropCO2Fuel(CR,LandSoil,Crop)$CropSuitability(CR,LandSoil,Crop)..
         vCropCO2FuelMg(CR,LandSoil,Crop) =E= SUM(GHG, (vNumha(CR,LandSoil,Crop) * CropOther(Crop,'Diesel')) * GHGOther(GHG,'Diesel') * GHGOther(GHG,'eqv'))
;
EQUATION eCropCO2ArtFert(Country,Region,LandType,SoilType,Crop) "Calculate CO2e emissions from artificial fertiliser production per country, region, land type, soil type and crop in Mg per year";
         eCropCO2ArtFert(CR,LandSoil,Crop)$CropSuitability(CR,LandSoil,Crop)..
         vCropCO2ArtFertMg(CR,LandSoil,Crop) =E=
                SUM((FertProd,GHG)$CropFertSuitability(Crop,'Artificial'), vFertProdMg(CR,LandSoil,Crop,'Artificial',FertProd) * GHGOther(GHG,FertProd) * GHGOther(GHG,'eqv'))
;
EQUATION eCropCO2eqTotal(Country,Region,LandType,SoilType,Crop) "Calculate total CO2e emissions from cropland for non-reference scenarios per country, region, land type, soil type and crop in Mg per year";
         eCropCO2eqTotal(CR,LandSoilCrop)$CropSuitability(CR,LandSoilCrop)..
         vCropCO2eqTotalMg(CR,LandSoilCrop) =E= ((vCropN2ONDMg(CR,LandSoilCrop) + vCropN2ONLMg(CR,LandSoilCrop) + vCropN2ONVMg(CR,LandSoilCrop)) * 44/28 * GHGOther('N2O','eqv')) +
                                              vCropGlassCO2eqMg(CR,LandSoilCrop) + vCropCO2FuelMg(CR,LandSoilCrop) + vCropCO2ArtFertMg(CR,LandSoilCrop)
;
EQUATION eCropCO2eqTotalReference(Country,Region,LandType,SoilType,Crop) "Calculate total CO2e emissions from cropland for reference scenarios per country, region, land type, soil type and crop in Mg per year";
         eCropCO2eqTotalReference(CR,LandSoilCrop)$CropSuitability(CR,LandSoilCrop)..
         vCropCO2eqTotalMg(CR,LandSoilCrop) =E= ((vCropN2ONDMg(CR,LandSoilCrop) + vCropN2ONLMg(CR,LandSoilCrop) + vCropN2ONVMg(CR,LandSoilCrop)) * 44/28 * GHGOther('N2O','eqv')) +
                                              vCropGlassCO2eqMg(CR,LandSoilCrop)
;
EQUATION eTransCO2(CountryA,RegionA) "Calculate CO2e emissions from transport per country and region in Mg per year";
         eTransCO2(CRA)..
         vTransCO2Mg(CRA) =E= SUM(CR, SUM(GHG, GHGOther(GHG,'Road_Trans') * GHGOther(GHG,'eqv')) *
                vTransportMg(CR,CRA) *
$iftheni "FixedDistance" == "on"  200)
$else                             TransDistkm(CR,CRA))
$endif
;
EQUATION eMaxTransCO2 "Constraint, maximum CO2e emissions from stransport can be 10% of total emissions";
         eMaxTransCO2..
         SUM(CR, vTransCO2Mg(CR)) =L= SUM((CR,Gender,Age), Population(CR,Gender,Age)) * 0.5
;
EQUATION eGHGRegionCO2eq(Country,Region) "Calculate total CO2e emissions per country and region in Mg per year";
         eGHGRegionCO2eq(CR)..
         vGHGRegionCO2eqMg(CR) =E=
                 SUM((LandSoilCrop)$CropSuitability(CR,LandSoilCrop), vCropCO2eqTotalMg(CR,LandSoilCrop)) +
                 SUM((APSHerdType), vAniCO2eqTotalMg(CR,APSHerdType)) +
                 SUM((Compost), vCompostCO2eqMg(CR,Compost)) +
                 vTransCO2Mg(CR) +
                 vMarineFishCO2Mg(CR)
;
****** Model solve related declarations and statements ******
MODEL CFSReference     /all -eCropRotationCrop -eCropRotationFamily -eCropFertPKMin -eCropFertNMin -eCropFertPKMax -eCropFertNMax -eCropNFert -eProcOutCPRegionExport -eCropCO2eqTotal -eAniCO2eqTotal -eHumIntakeMinTotal -eHumIntakeMaxTotal -eHumNutrRIntakeMax -eHumNutrRIntakeMin -eNutrBalancePSF -eNutrBalanceASF -eHumNutrImportMax/
      CFS              /all -eReferenceCroplandArea -eReferenceGrasslandArea -eCropFertReference -eProcOutCPRegionExportReference -eProcOutCPRegionExportReferenceSW -eAniNumReference  -eCropCO2eqTotalReference -eAniCO2eqTotalReferenece -eProcOutHNationalImportExport -eProcInNationalImportExport -eNutrBalancePSF -eNutrBalanceASF/
      CFSNutrBal       /all -eReferenceCroplandArea -eReferenceGrasslandArea -eCropFertReference -eProcOutCPRegionExportReference -eProcOutCPRegionExportReferenceSW -eAniNumReference  -eCropCO2eqTotalReference -eAniCO2eqTotalReferenece -eProcOutHNationalImportExport -eProcInNationalImportExport/
;
SET
UseCat                                                                                                                 "Reporting category for product use"                                     /Feed,Fertiliser/
Iter                                                                                                                   "Iteration counter, stop after %niter% iterations at the latest"         / 1 * %niter% /
;
ALIAS
(AllProd,AllProdA)
(FertNutr,FertNutrA)
(FeedCat,FeedCatA)
(FertCat,FertCatA)
;
PARAMETERS //Reporting
ScenarioResults                                 (*,Scenario)                                                           "Summary of scenario results e.g. GHG emissions, land use"
EmissionResults                                 (Scenario,*)                                                           "Summary of emission results e.g. GHG emissions, nitrogen emissions"
DailyIntake                                     (Country,ProcOutH,Month,Scenario)                                      "Daily intake of food per month, capita per day in grams"
DailyIntakeFamily                               (Country,Fam,Scenario)                                                 "Daily intake per food familiy per capita per day in grams"
DailyIntakeAnnual                               (Country,ProcOutH,Scenario)                                            "Daily intake of food per capita per day, annual average in grams"
DailyIntakeAnnualReport                         (Scenario,Country,ProcOutH)                                            "Daily intake of food per capita per day, annual average in grams, different set order"
NutrIntake                                      (Country,HumNutr,*,Scenario)                                           "Nutrients intake from ASF or PSF per capita per day, various units"
NutrIntakePerc                                  (Country,HumNutr,*,Scenario)                                           "Percentage of nutrient intake from ASF or PSF"
NutrIntakeFam                                   (Country,HumNutr,Fam,Scenario)                                         "Nutrient intake per food family per capita per day"
NutrIntakeFamPerc                               (Country,HumNutr,Fam,Scenario)                                         "Percenatge of nutrient intake per food family"
NutrIntakeASF                                   (Scenario,ProcOutH,Country,HumNutr)                                    "Intake of nutrients from ASF per capita per day"
AverageNutrReq                                  (Country,HumNutr,MinMax)                                               "Average nutrient requirements per capita"
NutrIntakePercReq                               (Scenario,Country,MinMax,HumNutr)                                      "Percentage of nutrient intake relaitive to requirments"
LandSoilCrophaR                                 (LandType,SoilType,Crop,Country,Region,Scenario)                       "Number of ha per region land type, soil type and crop"
CrophaC                                         (Crop,Country,Scenario)                                                "Number of ha crop per country"
CrophaR                                         (Crop,Country,Region,Scenario)                                         "Number of ha crop per region"
CrophaCFam                                      (Country,Fam,Scenario)                                                 "Number of ha crop per country and crop family"
LandhaR                                         (LandType,Country,Region,Scenario)                                     "Number of ha per land type per region"
LandhaC                                         (LandType,Country,Scenario)                                            "Number of ha per land type per country"
NumHerdTypeC                                    (APS,Level,HerdType,Country,Scenario)                                  "Number of animals per herd type per country"
NumHerdTypeCReport                              (Scenario,Country,APS,level,HerdType)                                  "Number of animals per herd type per country, different set order"
NumHerdTypeR                                    (APS,Level,HerdType,Country,Region,Scenario)                           "Number of animals per herd type per region"
NumProducerC                                    (APS,Level,HerdType,Country,Scenario)                                  "Number of producing animals per country"
NumProducerR                                    (APS,Level,HerdType,Country,Region,Scenario)                           "Number of prodcuing animals per region"
NumAPSC                                         (Country,APS,Scenario)
NutrAppliedLandSoilCropProdPercN0               (Country,LandType,SoilType,Crop,FertNutr,FertCat,AllProd,Scenario)     "Nutrients applied in fertiliser product per crop, soil type and land type. Percentage of total nutrient applied"
NutrAppliedLandSoilCropCatPercN0                (Country,LandType,SoilType,Crop,FertNutr,FertCat,Scenario)             "Nutrients applied in fertiliser category per crop, soil type and land type. Percenage of total nutrient applied"
NutrAppliedCropProdPercN0                       (Country,Crop,FertNutr,FertCat,AllProd,Scenario)                       "Nutrients applied in fertiliser product per crop. Percentage of total nutrient applied"
NutrAppliedCropCatPercN0                        (Country,Crop,FertNutr,FertCat,Scenario)                               "Nutrients applied in fertiliser category per crop. Percenage of total nutrient applied"
NutrAppliedProdPercN0                           (Country,FertNutr,FertCat,AllProd,Scenario)                            "Nutrients applied in fertiliser products. Percentage of total applied"
NutrAppliedCatPercN0                            (Country,FertNutr,FertCat,Scenario)                                    "Nutrients applied in fertiliser category. Percentage of total applied"
NutrAppliedRequiredRation                       (Scenario,Country,Region,LandType,SoilType,Crop,FertNutr)              "Ration of nutrients applied realtive to nutrients required"
IntakeAPSLevelHerdTypekgDMDay                   (Country,APS,level,HerdType,FeedCat,AllProd,Scenario)                  "Feed intake per aps, level, herd type in kg DM per day"
IntakeAPSLevelHerdTypeDMProdPercN0              (Country,APS,Level,HerdType,FeedCat,AllProd,Scenario)                  "Feed intake per aps, level, herd type in percentage of total intake"
IntakeAPSLevelHerdTypeDMCatPercN0               (Country,APS,Level,HerdType,FeedCat,Scenario)                          "Feed intake per aps, level, herd type in percentage of total intake per feed category"
IntakeAPSLevelDMProdPercN0                      (Country,APS,Level,FeedCat,AllProd,Scenario)                           "Feed intake per aps, level, in percentage of total intake"
IntakeAPSLevelDMCatPercN0                       (Country,APS,Level,FeedCat,Scenario)                                   "Feed intake per aps, level, in percentage of total intake per feed category"
IntakeAPSDMProdPercN0                           (Country,APS,FeedCat,AllProd,Scenario)                                 "Feed intake per aps, in percentage of total intake"
IntakeAPSDMCatPercN0                            (Country,APS,FeedCat,Scenario)                                         "Feed intake per aps, in percentage of total intake per feed category"
AniIntakeKgDMDay                                (Scenario,Country,APS,level,HerdType,FeedCat,AllProd)                  "Intake in kg DM day, aps, level, herd type"
AniIntakeTotalkgDMDay                           (Scenario,Country,APS,level,HerdType)                                  "Total intake in kg DM day, aps, level, herd type"
AniIntakeFeedCatDMDay                           (Scenario,Country,APS,level,HerdType,FeedCat)                          "Feed cat intake in kg DM day, aps, level, herd type"
AniNutrIntake                                   (Scenario,Country,RegionA,APS,level,HerdType,AniNutr)                  "Nutrient intake per aps, level, herd type"
AniNutrIntakeDay                                (Scenario,Country,APS,level,HerdType,AniNutr)                          "Nutrient intake per day aps, level, herd type"
AniNutrIntakeProd                               (Scenario,Country,RegionA,APS,level,HerdType,FeedCat,AllProd,AniNutr)  "Nutrient intake per product, aps, level, herd type"
AniNutrIntakeProdDay                            (Scenario,Country,APS,level,HerdType,FeedCat,AllProd,AniNutr)          "Nutrient intake per product day, aps, level, herd type"
AniNutrByFWC                                    (Scenario,Country,FWCat,Waste,AniNutr)                                 "Nutrient content of food waste mixes"
AniNutrSurplus                                  (Scenario,Country,APS,level,HerdType,AniNutr)                          "Surplus nnutrients"
AniIntakeAPSkgDM                                (Scenario,Country,APS,FeedCat)                                         "Feed intake per animal per day kg DM"
FeedAvailableMgDM                               (Scenario,Country,AllCat,AllProd)                                      "Total products available as feed or fertiliser"
AniFeedConsumedMgDM                             (Scenario,Country,AllCat,AllProd)                                      "Total feed consumed"
ProductUsePercN0                                (Country,AllCat,UseCat,Scenario)                                       "Desintation of each product type (e.g. co products) in percentage of product available"
AnimalCO2eqCapita                               (APS,Level,*,Scenario)                                                 "Animal emissions in kg per human capita"
CroplandEmissionsCapita                         (SoilType,Crop,*,Scenario)                                             "Cropland emissions in kg per human capita"
ImportExportProcInMg                            (Country,ProcIn,ImportExport,Scenario)                                "Import and export quantities of crop fish meat milk and eggs"
ImportExportProcOutHMg                          (Country,ProcOutH,ImportExport,Scenario)                               "Import and export quantities of processed food"
ImportExportFamMg                               (Country,Fam,ImportExport,Scenario)                                    "Import and export per food family"
NutrientImportDiet                              (Scenario,Country,HumNutrR)                                            "Percentage of human diet imported per nutrient"
//Other parameters
NutrAppliedLandSoilCropMgNPK                    (Country,Region,LandType,SoilType,Crop,AllCat,AllProd,FertNutr)        "Absolute NPK applied per crop"
NutrAppliedLandSoilCropProdPerc                 (Country,Region,LandType,SoilType,Crop,FertNutr,AllCat,AllProd)        "Nutrients applied in fertiliser product per crop, soil type and land type. Percentage of total nutrient applied"
NutrAppliedLandSoilCropCatPerc                  (Country,Region,LandType,SoilType,Crop,FertNutr,AllCat)                "Nutrients applied in fertiliser category per crop, soil type and land type. Percenage of total nutrient applied"
NutrAppliedCropProdPerc                         (Country,Region,Crop,FertNutr,AllCat,AllProd)                          "Nutrients applied in fertiliser product per crop. Percentage of total nutrient applied"
NutrAppliedCropCatPerc                          (Country,Region,Crop,FertNutr,AllCat)                                  "Nutrients applied in fertiliser category per crop. Percenage of total nutrient applied"
NutrAppliedProdPerc                             (Country,Region,FertNutr,AllCat,AllProd)                               "Nutrients applied in fertiliser products. Percentage of total applied"
NutrAppliedCatPerc                              (Country,Region,FertNutr,AllCat)                                       "Nutrients applied in fertiliser category. Percentage of total applied"
CropFertBalanceReport                           (Scenario,*,*,LandType,SoilType,Crop) 
IntakeHerdTypekgDM                              (Country,Region,APS,Level,HerdType,FeedCat,AllProd)                    "Feed intake per aps, level, herd type in kg DM per day"
IntakeAPSLevelHerdTypeDMProdPerc                (Country,Region,APS,Level,HerdType,FeedCat,AllProd)                    "Feed intake per aps, level, herd type in percentage of total intake"
IntakeAPSLevelHerdTypeDMCatPerc                 (Country,Region,APS,Level,HerdType,FeedCat)                            "Feed intake per aps, level, herd type in percentage of total intake per feed category"
IntakeAPSLevelDMProdPerc                        (Country,Region,APS,Level,FeedCat,AllProd)                             "Feed intake per aps, level, in percentage of total intake"
IntakeAPSLevelDMCatPerc                         (Country,Region,APS,Level,FeedCat)                                     "Feed intake per aps, level, in percentage of total intake per feed category"
IntakeAPSDMProdPerc                             (Country,Region,APS,FeedCat,AllProd)                                   "Feed intake per aps, in percentage of total intake"
IntakeAPSDMCatPerc                              (Country,Region,APS,FeedCat)                                           "Feed intake per aps, in percentage of total intake per feed category"
ProductUsePerc                                  (Country,Region,AllCat,UseCat)                                         "Desintation of each product type (e.g. co products) per region in percentage of product available"
CO2eqCompostkg                                  (Country,Region,Compost)                                               "CO2eq emissions per kg of compost"

ReportingGHG                                    (Scenario)                                                             "GHG emission results reporting gdx/R"
ReportingLU                                     (Scenario)                                                             "Land use results reporting gdx/R"
ReportingDiet                                   (Scenario,Fam)                                                         "Human diet per food group results reporting gdx/R"
ReportingExport                                 (Scenario,ImportExport,Fam)                                            "Import and export per food group reporting gdx/R"
ReportingLUFamily                               (Scenario,Fam)                                                         "Land use per food group reporting gdx/R"
ReportingAnimal                                 (Scenario,APS)                                                         "Animal numbers reporting gdx/R"
ReportingTradeNPK                               (Scenario,ImportExport,FertNutr)                                       "Nutrients imported and exported reporting gdx/R"

ReferenceImportExportFamMg                      (Country,Fam,ImportExport)                                             "Import and export per food group in reference scenario"
ReferenceImportExportNPKMg                      (Country,ImportExport,FertNutr)                                        "Nutrients imported and exported in reference scenario"
;

*** Solve and reporting loop start ***
Option limrow = 5;
Option limcol = 5;
Option reslim = 100000;
Option solver = CPLEX;
Option threads = 0;
option solveopt = Clear;
option bratio = 1;
;
CFSNutrBal.optfile = 1;
CFS.optfile = 1;
CFSReference.optfile = 1;
;

SCALARS     DeltaEPS                                                                                                    "Delta stopping criterion: stop iteration if largest relative parameter update < DeltaEPS"             / 0.1 /
            CurrentIter                                                                                                 "Last iteration"
;
PARAMETERS  Delta                               (Scenario,Iter)                                                         "% of values that changed more then Delta EPS"
            DeltaVariation                      (Scenario,Iter)                                                         "The number of values that changed more then Delta EPS"
            DeltaTotal                          (Scenario,Iter)                                                         "The total number of values that could change"
            AniNutrByProductCRIter              (Scenario,Iter,Country,Region,FWCat,Waste,AllNutr)                      "CR: Animal nutrients in aggregated waste in nutrient units per kg DM (per iteration)"
            GrazingProportionCRIter             (Scenario,Iter,Country,Region,APS,level,HerdType)                       "CR: Grazing proportion per APS level herd type (per iteration)"
            ManureTanCRIter                     (Scenario,Iter,Country,Region,AllCat,ManureProd)                        "CR: TAN fraction in manure APS level herd type (per iteration)"
            NPKFractionCRIter                   (Scenario,Iter,Country,Region,AllCat,AllProd,FertNutr)                  "CR: NPK fraction in manure compost and feed waste (per iteration)"
            AniNutrByProductCIter               (Scenario,Iter,Country,FWCat,Waste,AllNutr)                             "C: Animal nutrients in aggregated waste in nutrient units per kg DM (per iteration)"
            GrazingProportionCIter              (Scenario,Iter,Country,APS,level,HerdType)                              "C: Grazing proportion per APS level herd type (per iteration)"
            ManureTanCIter                      (Scenario,Iter,Country,AllCat,ManureProd)                               "C: TAN fraction in manure APS level herd type (per iteration)"
            NPKFractionCIter                    (Scenario,Iter,Country,AllCat,AllProd,FertNutr)                         "C: NPK fraction in manure compost and feed waste (per iteration)"
            NPKFractionCRVar                    (Scenario,Iter,Country,Region,AllCat,AllProd,FertNutr)                  "Variation between previous and current iteration NPK fraction"
            ManureTanCRVar                      (Scenario,Iter,Country,Region,AllCat,ManureProd)                        "Variation between previous and current iteration TAN fraction"
            GrazingProportionVar                (Scenario,Iter,Country,Region,Rumi,level,HerdType)                      "Variation between previous and current iteration grazing propotion"
            AniNutrByProductCRVar               (Scenario,Iter,Country,Region,FWCat,Waste,AllNutr)                      "Variation between previous and current iteration food waste animal nutrients"
            GrazingProportionAPSCIter           (Scenario,Iter,Country,Rumi,HerdType)                                   "APS: Grazing proportion per APS level herd type (per iteration)"
; 
FILE ofile  /'ofile.txt' / ;
FILE rfile  /'rfile.txt' / ;
FILE afile  /'afile.txt' / ;
FILE aafile /'aafile.txt'/;
FILE opt   /iter_rep.tmp/ ;
ofile.lw=0;
rfile.lw=0;
afile.lw=0;
;

Loop(Scenario,
*Reset iterative values to 0
AniNutrByProductCR              (CR,FWCat,Waste,AniNutr)                                   = 0;
GrazingProportionCR             (CR,Rumi,level,HerdType)                                   = 0;
ManureTanCR                     (CR,FertOthCat,ManureProd)                                 = 0;
ManureTanCR                     (CR,FertManCat,ManureProd)                                 = 0;
NPKRatioCR                      (CR,FertOthCat,FertProd,FertNutr)                          = 0;
NPKRatioCR                      (CR,FertManCat,FertProd,FertNutr)                          = 0;
ScenarioControlsModel           (Controls)                                                 = 0;
CurrentIter                                                                                = 0; // Reset iteration counter
*Calculate/set new starting values for iterative values
WeightedAverage                 (FWCat,ProcOutH)                                           = 1;
AniNutrByProductCR              (CR,FWCat,Waste,AniNutr)$FeedCatAllProd(FWCat,Waste)       = SUM((ProcOutH)$WasteTypeCatProd(FWCat,ProcOutH,Waste), WeightedAverage(FWCat,ProcOutH) * AniNutrByProduct('PPLoss',ProcOutH,AniNutr)) / SUM((ProcOutH)$WasteTypeCatProd(FWCat,ProcOutH,Waste), WeightedAverage(FWCat,ProcOutH)); // Starting value
AniNutrByProductCR              (CR,FWCat,Waste,'FM')$FeedCatAllProd(FWCat,Waste)          = (1000/(AniNutrByProductCR(CR,FWCat,Waste,'DM')/1000))$AniNutrByProductCR(CR,FWCat,Waste,'DM');
ScenarioControlsModel           (Controls)                                                 = ScenarioControls(Scenario,Controls);
GrazingProportionCR             (CR,Rumi,Level,Herdtype)$(not HerdType('VealCalf'))        = (ScenarioControlsModel('MinGrazingProportion')  + ScenarioControlsModel('MaxGrazingProportion')) / 2 ; // Starting value
*Reset scenario values to 0
ProcInImportExportMg            (CR,ImportExport,ProcIn)                                   = 0;
ProcOutHImportExportMg          (CR,ImportExport,ProcOutH)                                 = 0;
HumIntakeReq                    (Country,ProdTot,MinMax,Fam)                               = 0;
HumNutrReq                      (Country,Gender,Age,MinMax,HumNutrR)                       = 0;
AniFeedSuitability              (APS,FeedCat,AllProd)                                      = 0;
CropFertSuitability             (Crop,AllCat)                                              = 0;
ProcInImportExportSuitability   (CR,ImportExport,ProcIn)                                   = 0;
ProcOutHImportExportSuitability (CR,ImportExport,ProcOutH)                                 = 0;
ImportBoundary                  (CR,CRA,AllCat)                                            = 0;
FRVByProduct                    (FertCat,AllProd,FertNutr)                                 = 0;
CropFertManureha                (CR,SoilType,Derogation)                                   = 0;
AniHerdTypePerPU                (APSHerdType)                                              = 0;
CropYieldha                     (CR,LandSoilCrop)                                          = 0;
AniHerdTypePerPU                (APSHerdType)                                              = 0;
FRVByProduct                    (FertCat,FertProd,FertNutr)                                = 0;
ReferenceManure                 (FertNutr)                                                 = 0;
*Reset scenario values to default
CropYieldha                     (CR,LandSoilCrop)                                          = ImportCropYieldha(CR,LandSoilCrop);
AniHerdTypePerPU                (APSHerdType)                                              = ImportAniData(APSHerdType,'HerdTypesPerPu');
FRVByProduct                    (FertCat,FertProd,FertNutr)                                = ImportFRVByProduct(FertCat,FertProd,FertNutr);
ReferenceManure                 (FertNutr)                                                 = 1;
*Reset variable bounds
vNumHerdType.UP                 (CR,APSHerdType)                                           = inf;
vAniFeedIntakeHerdTypeMgDM.UP   (CR,APSHerdType,FeedCat,FeedProd)                          = inf;
vAniFeedIntakeHerdTypeMgDM.FX   (CR,APSHerdType,FWCat,Waste)                               = 0;
;

*Specific reference controls
if     (ScenarioControls(Scenario,'Reference') = 0,
        CropFertManureha                 (CR,SoilType,Derogation)                          = ImportCropFertManureha(CR,SoilType,Derogation,'New') * 0.001;
elseif  ScenarioControls(Scenario,'Reference') = 1,
        vNumHerdType.UP                  (CR,APS,'Low',HerdType)                           = 0;
        vNumHerdType.UP                  (CR,'Broiler','Mid',HerdType)                     = 0;
        vNumHerdType.UP                  (CR,'Pig','Mid',HerdType)                         = 0;
        vNumHerdType.UP                  (CR,'Beef',Level,HerdType)                        = inf;
        AniHerdTypePerPU                 ('Dairy',level,'VealCalf')                        = 0.6;
        CropYieldha                      (CR,'Grassland',SoilType,Crop)                    = ImportCropYieldha(CR,'Grassland',SoilType,Crop) * 0.85;
        CropFertManureha                 (CR,SoilType,Derogation)                          = ImportCropFertManureha(CR,SoilType,Derogation,'Old') * 0.001;   
        ReferenceManure                  ('N')                                             = 0.8;
        ReferenceManure                  ('P')                                             = 0.747;
);
*Diet Controls and scale diet requirements
if     (ScenarioControls(Scenario,'Diet') = 1, // Reference
        HumIntakeReq                     (Country,ProdTot,MinMax,Fam)                      = ImportHumIntakeReq('Current',Country,ProdTot,MinMax,Fam)        * 0.001;
        HumNutrReq                       (Country,Gender,Age,MinMax,HumNutrR)              = 0;
elseif  ScenarioControls(Scenario,'Diet') = 2, // Cureent diet
        HumIntakeReq                     (Country,ProdTot,MinMax,Fam)                      = ImportHumIntakeReq('Current',Country,ProdTot,MinMax,Fam)        * 0.001;
        HumNutrReq                       (Country,Gender,Age,MinMax,HumNutrR)              = ImportHumNutrReq('Current',Country,Gender,Age,MinMax,HumNutrR)  * 0.001;
elseif  ScenarioControls(Scenario,'Diet') = 3, // Circular diet
        HumIntakeReq                     (Country,ProdTot,MinMax,Fam)                      = ImportHumIntakeReq('Circular',Country,ProdTot,MinMax,Fam)       * 0.001;
        HumNutrReq                       (Country,Gender,Age,MinMax,HumNutrR)              = ImportHumNutrReq('Circular',Country,Gender,Age,MinMax,HumNutrR) * 0.001;
);
*Import Controls
if     (ScenarioControls(Scenario,'National.Import') = 1, // Reference
        ProcInImportExportMg             (CR,'Import',ProcIn)                              $(ImportImportExport('Reference','ProcIn',CR,ProcIn,'Import')     - ImportImportExport('Reference','ProcIn',CR,ProcIn,'Export')>0)  =  ImportImportExport('Reference','ProcIn',CR,ProcIn,'Import')  - ImportImportExport('Reference','ProcIn',CR,ProcIn,'Export');
        ProcInImportExportMg             (CR,'Export',ProcIn)                              $(ImportImportExport('Reference','ProcIn',CR,ProcIn,'Export')     - ImportImportExport('Reference','ProcIn',CR,ProcIn,'Import')>0)  =  ImportImportExport('Reference','ProcIn',CR,ProcIn,'Export')  - ImportImportExport('Reference','ProcIn',CR,ProcIn,'Import');                             
        ProcOutHImportExportMg           (CR,'Import',ProcOutH)                            $(ImportImportExport('Reference','ProcOutH',CR,ProcOutH,'Import') - ImportImportExport('Reference','ProcOutH',CR,ProcOutH,'Export')>0) =  ImportImportExport('Reference','ProcOutH',CR,ProcOutH,'Import') - ImportImportExport('Reference','ProcOutH',CR,ProcOutH,'Export');
        ProcOutHImportExportMg           (CR,'Export',ProcOutH)                            $(ImportImportExport('Reference','ProcOutH',CR,ProcOutH,'Export') - ImportImportExport('Reference','ProcOutH',CR,ProcOutH,'Import')>0) =  ImportImportExport('Reference','ProcOutH',CR,ProcOutH,'Export') - ImportImportExport('Reference','ProcOutH',CR,ProcOutH,'Import');
        ProcInImportExportSuitability    (Country,RegionIE,ImportExport,ProcIn)            = ImportExportSuitability('Reference',ImportExport,'ProcIn',ProcIn);
        ProcOutHImportExportSuitability  (Country,RegionIE,ImportExport,ProcOutH)          = ImportExportSuitability('Reference',ImportExport,'ProcOutH',ProcOutH);
elseif  ScenarioControls(Scenario,'National.Import') = 2, // Only import and export of food items
        ProcInImportExportMg             (CR,ImportExport,ProcIn)                          = 0;
        ProcOutHImportExportMg           (CR,ImportExport,ProcOutH)                        = 0;
        ProcInImportExportSuitability    (Country,RegionIE,ImportExport,ProcIn)            = ImportExportSuitability('ImportExport1',ImportExport,'ProcIn',ProcIn);
        ProcOutHImportExportSuitability  (Country,RegionIE,ImportExport,ProcOutH)          = ImportExportSuitability('ImportExport1',ImportExport,'ProcOutH',ProcOutH);
elseif  ScenarioControls(Scenario,'National.Import') = 3, // No import and export (self sufficient)
        ProcInImportExportMg             (CR,ImportExport,ProcIn)                          = 0;
        ProcOutHImportExportMg           (CR,ImportExport,ProcOutH)                        = 0;
        ProcInImportExportSuitability    (Country,RegionIE,ImportExport,ProcIn)            = 0;
        ProcOutHImportExportSuitability  (Country,RegionIE,ImportExport,ProcOutH)          = 0;
);
*Circularity Controls
if     (ScenarioControls(Scenario,'Circular') = 1, // Reference
        AniFeedSuitability               (APS,FeedCat,FeedProd)                            = ImportAniFeedSuitability('Reference',FeedCat,FeedProd,APS);
        CropFertSuitability              (Crop,AllCat)                                     = ImportFertsuitability('Reference',Crop,AllCat);
elseif  ScenarioControls(Scenario,'Circular') = 2, // Current resource use
        AniFeedSuitability               (APS,FeedCat,FeedProd)                            = ImportAniFeedSuitability('Current',FeedCat,FeedProd,APS);
        CropFertSuitability              (Crop,AllCat)                                     = ImportFertsuitability('Current',Crop,AllCat);
elseif  ScenarioControls(Scenario,'Circular') = 3, // Circular resource use
        AniFeedSuitability               (APS,FeedCat,FeedProd)                            = ImportAniFeedSuitability('Circular',FeedCat,FeedProd,APS);
        CropFertSuitability              (Crop,AllCat)                                     = ImportFertsuitability('Circular',Crop,AllCat);
elseif  ScenarioControls(Scenario,'Circular') = 4, // No feed-food competition
        AniFeedSuitability               (APS,FeedCat,FeedProd)                            = ImportAniFeedSuitability('Circular',FeedCat,FeedProd,APS);
        CropFertSuitability              (Crop,AllCat)                                     = ImportFertsuitability('Circular',Crop,AllCat);
        vAniFeedIntakeHerdTypeMgDM.UP    (CR,APSHerdType,'FoodFeed',ProcOutFF)             = 0;
);
* Fertiliser replacement value controls
if     (ScenarioControls(Scenario,'FRV') = 1, // Legislation values
        FRVByProduct                     (FertCat,FertProd,'N')                            = FRVReference(FertCat,FertProd,'FRV','N');
        FRVByProduct                     ('Artificial',FertProd,'N')                       = 1;
        FRVByProduct                     ('Residue',FertProd,'N')                          = 0.1;
elseif  ScenarioControls(Scenario,'FRV') = 2, // Default values (1) except residue N  
        FRVByProduct                     ('Residue',FertProd,'N')                          = 0;
);
* Synthetic amino acid and animal feed controls
if     (ScenarioControls(Scenario,'AminoAcid') = 0, // No synthetic amino acids in animal diets
        vAniFeedIntakeHerdTypeMgDM.UP(CR,APSHerdType,'AminoAcid',ProcOutAA)                = 0;
);
* Control which products can be imported and exported between regions
if     (ScenarioControls(Scenario,'Regional.Import') = 0, // all products can be imported and exported everywhere
        ImportBoundary(CR,CRA,AllCat)                                                      = CR(CR) + CR(CRA);      
elseif  ScenarioControls(Scenario,'Regional.Import') = 1, // food can be exported everywhere, rest in own region        
        ImportBoundary(CR,CRA,FertManCat)                                                  = CRCRA(CR,CRA);
        ImportBoundary(CR,CRA,WasteCat)                                                    = CRCRA(CR,CRA);
        ImportBoundary(CR,CRA,'ProcOutH')                                                  = CR(CR) + CR(CRA);
        ImportBoundary(CR,CRA,'FoodFeed')                                                  = CRCRA(CR,CRA);
        ImportBoundary(CR,CRA,'CoProduct')                                                 = CRCRA(CR,CRA);
elseif  ScenarioControls(Scenario,'Regional.Import') = 2, // food can be exported everywhere, manure and compost to neigboruing region 
        ImportBoundary(CR,CRA,FertManCat)                                                  = RegionNeighbour(CR,CRA); 
        ImportBoundary(CR,CRA,WasteCat)                                                    = RegionNeighbour(CR,CRA);
        ImportBoundary(CR,CRA,'ProcOutH')                                                  = CR(CR) + CR(CRA);
        ImportBoundary(CR,CRA,'FoodFeed')                                                  = CR(CR) + CR(CRA);
        ImportBoundary(CR,CRA,'CoProduct')                                                 = CR(CR) + CR(CRA);
);

Loop(Iter,

CurrentIter = CurrentIter + 1;

* Model solve statements
if     (ScenarioControls(Scenario,'Model') = 1,
        Solve CFSReference minimizing vPenaltyReference using lp;
elseif  ScenarioControls(Scenario,'Model') = 3,
        Solve CFS minimizing vTotalLandUse using lp;
elseif  ScenarioControls(Scenario,'Model') = 2,
        Solve CFSNutrBal minimizing vTotalLandUse using lp;
elseif  ScenarioControls(Scenario,'Model') = 5,
        Solve CFS minimizing vTotalCO2eq using lp;
elseif  ScenarioControls(Scenario,'Model') = 4,
        Solve CFSNutrBal minimizing vTotalCO2eq using lp;
);




****** Values for reporting ******
*** Human nutrient intake ***

DailyIntake(Country,ProcOutH,Month,Scenario)$HumFoodMonth(ProcOutH,Month)                                            = SUM((Region)$(CR(Country,Region)), vProcoutHMonthMg.L(Country,Region,ProcOutH,Month) * 1000) /  SUM((Region,Gender,Age)$CR(Country,Region), Population(Country,Region,Gender,Age)) * 1000 / (365/12);
DailyIntake(Country,ProcOutH,Month,Scenario)$(DailyIntake(Country,ProcOutH,Month,Scenario)<1)                        = 0;
DailyIntakeAnnual(Country,ProcOutH,Scenario)                                                                         = SUM((Region,Month)$(CR(Country,Region) AND HumFoodMonth(ProcOutH,Month)), vProcoutHMonthMg.L(Country,Region,ProcOutH,Month) * 1000)  / SUM((Region,Gender,Age)$CR(Country,Region), Population(Country,Region,Gender,Age)) * 1000 / 365;
DailyIntakeFamily(Country,Fam,Scenario)                                                                              = SUM((ProcOutH)$FamProcOutH(Fam,ProcOutH), DailyIntakeAnnual(Country,ProcOutH,Scenario));
DailyIntakeAnnualReport(Scenario,Country,ProcOutH)                                                                   = SUM((Region,Month)$(CR(Country,Region) AND HumFoodMonth(ProcOutH,Month)), vProcoutHMonthMg.L(Country,Region,ProcOutH,Month) * 1000)  / SUM((Region,Gender,Age)$CR(Country,Region), Population(Country,Region,Gender,Age)) * 1000 / 365;


AverageNutrReq(Country,HumNutrR,'Min') = SUM((Region,Gender,Age)$CR(Country,Region), Population(Country,Region,Gender,Age) * HumNutrReq(Country,Gender,Age,'Min',HumNutrR) * 1000) / SUM((Region,Gender,Age)$CR(Country,Region), Population(Country,Region,Gender,Age));
AverageNutrReq(Country,HumNutrR,'Max') = SUM((Region,Gender,Age)$CR(Country,Region), Population(Country,Region,Gender,Age) * HumNutrReq(Country,Gender,Age,'Max',HumNutrR) * 1000) / SUM((Region,Gender,Age)$CR(Country,Region), Population(Country,Region,Gender,Age));

NutrIntake(Country,HumNutr,'Total',Scenario)      =  SUM((Region,ProcOutH,Month)$(CR(Country,Region) AND HumFoodMonth(ProcOutH,Month)),                               vProcoutHMonthMg.L(Country,Region,ProcOutH,Month)    * 1000 * HumNutrByProduct(ProcOutH,HumNutr)    * 10) / SUM((Region,Gender,Age)$CR(Country,Region), Population(Country,Region,Gender,Age)) /365;
NutrIntake(Country,HumNutr,'ASF',Scenario)        =  SUM((Region,ProcOutHASF,Month)$(CR(Country,Region) AND HumFoodMonth(ProcOutHASF,Month)),                         vProcoutHMonthMg.L(Country,Region,ProcOutHASF,Month) * 1000 * HumNutrByProduct(ProcOutHASF,HumNutr) * 10) / SUM((Region,Gender,Age)$CR(Country,Region), Population(Country,Region,Gender,Age)) /365;
NutrIntake(Country,HumNutr,'PSF',Scenario)        =  NutrIntake(Country,HumNutr,'Total',Scenario) - NutrIntake(Country,HumNutr,'ASF',Scenario);
NutrIntakePerc(Country,HumNutr,'ASF',Scenario)    = (SUM((Region,ProcOutHASF,Month)$(CR(Country,Region) AND HumFoodMonth(ProcOutHASF,Month)),                         vProcoutHMonthMg.L(Country,Region,ProcOutHASF,Month) * 1000 * HumNutrByProduct(ProcOutHASF,HumNutr) * 10) / SUM((Region,ProcOutH,Month)$(CR(Country,Region) AND HumFoodMonth(ProcOutH,Month)), vProcoutHMonthMg.L(Country,Region,ProcOutH,Month) * 1000 * HumNutrByProduct(ProcOutH,HumNutr) * 10))$SUM((Region,ProcOutH,Month)$(CR(Country,Region) AND HumFoodMonth(ProcOutH,Month)), vProcoutHMonthMg.L(Country,Region,ProcOutH,Month) * 1000 * HumNutrByProduct(ProcOutH,HumNutr) * 10);
NutrIntakePerc(Country,HumNutr,'PSF',Scenario)    = (SUM((Region,ProcOutHPSF,Month)$(CR(Country,Region) AND HumFoodMonth(ProcOutHPSF,Month)),                         vProcoutHMonthMg.L(Country,Region,ProcOutHPSF,Month) * 1000 * HumNutrByProduct(ProcOutHPSF,HumNutr) * 10) / SUM((Region,ProcOutH,Month)$(CR(Country,Region) AND HumFoodMonth(ProcOutH,Month)), vProcoutHMonthMg.L(Country,Region,ProcOutH,Month) * 1000 * HumNutrByProduct(ProcOutH,HumNutr) * 10))$SUM((Region,ProcOutH,Month)$(CR(Country,Region) AND HumFoodMonth(ProcOutH,Month)), vProcoutHMonthMg.L(Country,Region,ProcOutH,Month) * 1000 * HumNutrByProduct(ProcOutH,HumNutr) * 10);
NutrIntakeFam(Country,HumNutr,Fam,Scenario)       =  SUM((Region,ProcOutH,Month)$(CR(Country,Region) AND FamProcOutH(Fam,ProcOutH) AND HumFoodMonth(ProcOutH,Month)), vProcoutHMonthMg.L(Country,Region,ProcOutH,Month)    * 1000 * HumNutrByProduct(ProcOutH,HumNutr)    * 10) / SUM((Region,Gender,Age)$CR(Country,Region), Population(Country,Region,Gender,Age)) / 365;
NutrIntakeFamPerc(Country,HumNutr,Fam,Scenario)   = (SUM((Region,ProcOutH,Month)$(CR(Country,Region) AND FamProcOutH(Fam,ProcOutH) AND HumFoodMonth(ProcOutH,Month)), vProcoutHMonthMg.L(Country,Region,ProcOutH,Month)    * 1000 * HumNutrByProduct(ProcOutH,HumNutr)    * 10) / SUM((Region,ProcOutH,Month)$(CR(Country,Region) AND HumFoodMonth(ProcOutH,Month)), vProcoutHMonthMg.L(Country,Region,ProcOutH,Month) * 1000 * HumNutrByProduct(ProcOutH,HumNutr) * 10))$SUM((Region,ProcOutH,Month)$(CR(Country,Region) AND HumFoodMonth(ProcOutH,Month)), vProcoutHMonthMg.L(Country,Region,ProcOutH,Month) * 1000 * HumNutrByProduct(ProcOutH,HumNutr) * 10);

NutrIntakeASF(Scenario,ProcOutHASF,Country,HumNutr) =  SUM((Region,Month)$(CR(Country,Region) AND HumFoodMonth(ProcOutHASF,Month)),                         vProcoutHMonthMg.L(Country,Region,ProcOutHASF,Month) * 1000 * HumNutrByProduct(ProcOutHASF,HumNutr) * 10) / SUM((Region,Gender,Age)$CR(Country,Region), Population(Country,Region,Gender,Age)) /365;

NutrIntakePercReq(Scenario,Country,MinMax,HumNutr)  = (NutrIntake(Country,HumNutr,'Total',Scenario) / AverageNutrReq(Country,HumNutr,MinMax))$AverageNutrReq(Country,HumNutr,MinMax);

****** Crop Reporting ******

LandSoilCrophaR(LandSoilCrop,CR,Scenario) =                                                                                                                vNumha.L(CR,LandSoilCrop)                          $vNumha.L(CR,LandSoilCrop);
CrophaC(Crop,Country,Scenario)            =  SUM((Region,LandSoil)$(CR(Country,Region) AND CropSuitability(Country,Region,LandSoil,Crop)),                 vNumha.L(Country,Region,LandSoil,Crop)             $vNumha.L(Country,Region,LandSoil,Crop));
CrophaR(Crop,CR,Scenario)                 =  SUM((LandSoil)$CropSuitability(CR,LandSoil,Crop),                                                             vNumha.L(CR,LandSoil,Crop)                         $vNumha.L(CR,LandSoil,Crop));
LandhaC(LandType,Country,Scenario)        =  SUM((Region,Crop,SoilType)$(CR(Country,Region) AND CropSuitability(Country,Region,LandType,SoilType,Crop)),   vNumha.L(Country,Region,LandType,SoilType,Crop)    $vNumha.L(Country,Region,LandType,SoilType,Crop));
LandhaR(LandType,CR,Scenario)             =  SUM((Crop,SoilType)$CropSuitability(CR,LandType,SoilType,Crop),                                               vNumha.L(CR,LandType,SoilType,Crop)                $vNumha.L(CR,LandType,SoilType,Crop));
CrophaCFam(Country,Fam,Scenario)          =  SUM((Crop)$FamAllProd('Crop',Fam,Crop), CrophaC(Crop,Country,Scenario));

*** Nutrients Applied ***

NutrAppliedLandSoilCropMgNPK(CR,LandSoilCrop,FertProdCat,AllProd,FertNutr)                  = vFertProdMg.L(CR,LandSoilCrop,FertProdCat,AllProd)$vFertProdMg.L(CR,LandSoilCrop,FertProdCat,AllProd)       * FertNutrByProductMgMg(FertProdCat,AllProd,FertNutr);
NutrAppliedLandSoilCropMgNPK(CR,LandSoilCrop,FertOthCat,AllProd,FertNutr)                   = vFertOtherNPKMg.L(CR,LandSoilCrop,FertOthCat,AllProd)$vFertOtherNPKMg.L(CR,LandSoilCrop,FertOthCat,AllProd) * NPKRatioCR(CR,FertOthCat,AllProd,FertNutr);
NutrAppliedLandSoilCropMgNPK(CRA,LandSoilCrop,FertManCat,AllProd,FertNutr)               = SUM((CR), vFertManureCompostNPKMg.L(CR,CRA,LandSoilCrop,FertManCat,AllProd)$vFertManureCompostNPKMg.L(CR,CRA,LandSoilCrop,FertManCat,AllProd) * NPKRatioCR(CR,FertManCat,AllProd,FertNutr));

NutrAppliedLandSoilCropProdPerc(CR,LandSoilCrop,FertNutr,FertCat,AllProd)                       = (NutrAppliedLandSoilCropMgNPK(CR,LandSoilCrop,FertCat,AllProd,FertNutr)                                                                         / SUM((FertCatA,AllProdA),                                        NutrAppliedLandSoilCropMgNPK(CR,LandSoilCrop,FertCatA,AllProdA,FertNutr)))             $SUM((FertCatA,AllProdA),                                        NutrAppliedLandSoilCropMgNPK(CR,LandSoilCrop,FertCatA,AllProdA,FertNutr));
NutrAppliedLandSoilCropProdPercN0(Country,LandSoilCrop,FertNutr,FertCat,AllProd,Scenario)       = (SUM((Region)$CR(Country,Region),                        NutrAppliedLandSoilCropMgNPK(Country,Region,LandSoilCrop,FertCat,AllProd,FertNutr))    / SUM((Region,FertCatA,AllProdA)$CR(Country,Region),              NutrAppliedLandSoilCropMgNPK(Country,Region,LandSoilCrop,FertCatA,AllProdA,FertNutr))) $SUM((Region,FertCatA,AllProdA)$CR(Country,Region),              NutrAppliedLandSoilCropMgNPK(Country,Region,LandSoilCrop,FertCatA,AllProdA,FertNutr));
NutrAppliedLandSoilCropCatPerc(CR,LandSoilCrop,FertNutr,FertCat)                                = (SUM(AllProd,                                            NutrAppliedLandSoilCropMgNPK(CR,LandSoilCrop,FertCat,AllProd,FertNutr))                / SUM((FertCatA,AllProdA),                                        NutrAppliedLandSoilCropMgNPK(CR,LandSoilCrop,FertCatA,AllProdA,FertNutr)))             $SUM((FertCatA,AllProdA),                                        NutrAppliedLandSoilCropMgNPK(CR,LandSoilCrop,FertCatA,AllProdA,FertNutr));
NutrAppliedLandSoilCropCatPercN0(Country,LandSoilCrop,FertNutr,FertCat,Scenario)                = (SUM((Region,AllProd)$CR(Country,Region),                NutrAppliedLandSoilCropMgNPK(Country,Region,LandSoilCrop,FertCat,AllProd,FertNutr))    / SUM((Region,FertCatA,AllProdA)$CR(Country,Region),              NutrAppliedLandSoilCropMgNPK(Country,Region,LandSoilCrop,FertCatA,AllProdA,FertNutr))) $SUM((Region,FertCatA,AllProdA)$CR(Country,Region),              NutrAppliedLandSoilCropMgNPK(Country,Region,LandSoilCrop,FertCatA,AllProdA,FertNutr));
NutrAppliedCropProdPerc(CR,Crop,FertNutr,FertCat,AllProd)                                       = (SUM(LandSoil,                                           NutrAppliedLandSoilCropMgNPK(CR,LandSoil,Crop,FertCat,AllProd,FertNutr))               / SUM((LandSoil,FertCatA,AllProdA),                               NutrAppliedLandSoilCropMgNPK(CR,LandSoil,Crop,FertCatA,AllProdA,FertNutr)))            $SUM((LandSoil,FertCatA,AllProdA),                               NutrAppliedLandSoilCropMgNPK(CR,LandSoil,Crop,FertCatA,AllProdA,FertNutr));
NutrAppliedCropProdPercN0(Country,Crop,FertNutr,FertCat,AllProd,Scenario)                       = (SUM((Region,LandSoil)$CR(Country,Region),               NutrAppliedLandSoilCropMgNPK(Country,Region,LandSoil,Crop,FertCat,AllProd,FertNutr))   / SUM((Region,LandSoil,FertCatA,AllProdA)$CR(Country,Region),     NutrAppliedLandSoilCropMgNPK(Country,Region,LandSoil,Crop,FertCatA,AllProdA,FertNutr)))$SUM((Region,LandSoil,FertCatA,AllProdA)$CR(Country,Region),     NutrAppliedLandSoilCropMgNPK(Country,Region,LandSoil,Crop,FertCatA,AllProdA,FertNutr));
NutrAppliedCropCatPerc(CR,Crop,FertNutr,FertCat)                                                = (SUM((LandSoil,AllProd),                                 NutrAppliedLandSoilCropMgNPK(CR,LandSoil,Crop,FertCat,AllProd,FertNutr))               / SUM((LandSoil,FertCatA,AllProdA),                               NutrAppliedLandSoilCropMgNPK(CR,LandSoil,Crop,FertCatA,AllProdA,FertNutr)))            $SUM((LandSoil,FertCatA,AllProdA),                               NutrAppliedLandSoilCropMgNPK(CR,LandSoil,Crop,FertCatA,AllProdA,FertNutr));
NutrAppliedCropCatPercN0(Country,Crop,FertNutr,FertCat,Scenario)                                = (SUM((Region,LandSoil,AllProd)$CR(Country,Region),       NutrAppliedLandSoilCropMgNPK(Country,Region,LandSoil,Crop,FertCat,AllProd,FertNutr))   / SUM((Region,LandSoil,FertCatA,AllProdA)$CR(Country,Region),     NutrAppliedLandSoilCropMgNPK(Country,Region,LandSoil,Crop,FertCatA,AllProdA,FertNutr)))$SUM((Region,LandSoil,FertCatA,AllProdA)$CR(Country,Region),     NutrAppliedLandSoilCropMgNPK(Country,Region,LandSoil,Crop,FertCatA,AllProdA,FertNutr));
NutrAppliedProdPerc(CR,FertNutr,FertCat,AllProd)                                                = (SUM(LandSoilCrop,                                       NutrAppliedLandSoilCropMgNPK(CR,LandSoilCrop,FertCat,AllProd,FertNutr))                / SUM((LandSoilCrop,FertCatA,AllProdA),                           NutrAppliedLandSoilCropMgNPK(CR,LandSoilCrop,FertCatA,AllProdA,FertNutr)))             $SUM((LandSoilCrop,FertCatA,AllProdA),                           NutrAppliedLandSoilCropMgNPK(CR,LandSoilCrop,FertCatA,AllProdA,FertNutr));
NutrAppliedProdPercN0(Country,FertNutr,FertCat,AllProd,Scenario)                                = (SUM((Region,LandSoilCrop)$CR(Country,Region),           NutrAppliedLandSoilCropMgNPK(Country,Region,LandSoilCrop,FertCat,AllProd,FertNutr))    / SUM((Region,LandSoilCrop,FertCatA,AllProdA)$CR(Country,Region), NutrAppliedLandSoilCropMgNPK(Country,Region,LandSoilCrop,FertCatA,AllProdA,FertNutr))) $SUM((Region,LandSoilCrop,FertCatA,AllProdA)$CR(Country,Region), NutrAppliedLandSoilCropMgNPK(Country,Region,LandSoilCrop,FertCatA,AllProdA,FertNutr));
NutrAppliedCatPerc(CR,FertNutr,FertCat)                                                         = (SUM((LandSoilCrop,AllProd),                             NutrAppliedLandSoilCropMgNPK(CR,LandSoilCrop,FertCat,AllProd,FertNutr))                / SUM((LandSoilCrop,FertCatA,AllProdA),                           NutrAppliedLandSoilCropMgNPK(CR,LandSoilCrop,FertCatA,AllProdA,FertNutr)))             $SUM((LandSoilCrop,FertCatA,AllProdA),                           NutrAppliedLandSoilCropMgNPK(CR,LandSoilCrop,FertCatA,AllProdA,FertNutr));
NutrAppliedCatPercN0(Country,FertNutr,FertCat,Scenario)                                         = (SUM((Region,LandSoilCrop,AllProd)$CR(Country,Region),   NutrAppliedLandSoilCropMgNPK(Country,Region,LandSoilCrop,FertCat,AllProd,FertNutr))    / SUM((Region,LandSoilCrop,FertCatA,AllProdA)$CR(Country,Region), NutrAppliedLandSoilCropMgNPK(Country,Region,LandSoilCrop,FertCatA,AllProdA,FertNutr))) $SUM((Region,LandSoilCrop,FertCatA,AllProdA)$CR(Country,Region), NutrAppliedLandSoilCropMgNPK(Country,Region,LandSoilCrop,FertCatA,AllProdA,FertNutr));

NutrAppliedRequiredRation(Scenario,CR,LandSoilCrop,FertNutr) = (vCropFertMg.L(CR,LandSoilCrop,FertNutr) / (CropFertha(CR,LandSoilCrop,FertNutr) * vNumha.L(CR,LandSoilCrop)))$(CropFertha(CR,LandSoilCrop,FertNutr) * vNumha.L(CR,LandSoilCrop));

CropFertBalanceReport(Scenario,'Input','Artificial',LandSoil,Crop)                 = (SUM((CR,AllProd),     NutrAppliedLandSoilCropMgNPK(CR,LandSoil,Crop,'Artificial',AllProd,'N') * 1000)        / SUM(CR, vNumha.L(CR,LandSoil,Crop)))$SUM(CR, vNumha.L(CR,LandSoil,Crop));
CropFertBalanceReport(Scenario,'Input','Manure',LandSoil,Crop)                     = (SUM((CR,AllProd),     NutrAppliedLandSoilCropMgNPK(CR,LandSoil,Crop,'Manure',AllProd,'N') * 1000)            / SUM(CR, vNumha.L(CR,LandSoil,Crop)))$SUM(CR, vNumha.L(CR,LandSoil,Crop));
CropFertBalanceReport(Scenario,'Input','Compost',LandSoil,Crop)                    = (SUM((CR,AllProd),     NutrAppliedLandSoilCropMgNPK(CR,LandSoil,Crop,'Compost',AllProd,'N') * 1000)           / SUM(CR, vNumha.L(CR,LandSoil,Crop)))$SUM(CR, vNumha.L(CR,LandSoil,Crop));
CropFertBalanceReport(Scenario,'Input','Grazing_Manure',LandSoil,Crop)             = (SUM((CR,AllProd),     NutrAppliedLandSoilCropMgNPK(CR,LandSoil,Crop,'GrazingManure',AllProd,'N') * 1000)     / SUM(CR, vNumha.L(CR,LandSoil,Crop)))$SUM(CR, vNumha.L(CR,LandSoil,Crop));
CropFertBalanceReport(Scenario,'Input','Crop_Residue',LandSoil,Crop)               = (SUM((CR,AllProd),     NutrAppliedLandSoilCropMgNPK(CR,LandSoil,Crop,'Residue',AllProd,'N') * 1000)           / SUM(CR, vNumha.L(CR,LandSoil,Crop)))$SUM(CR, vNumha.L(CR,LandSoil,Crop));
CropFertBalanceReport(Scenario,'Input','Feed_Waste',LandSoil,Crop)                 = (SUM((CR,AllProd),     NutrAppliedLandSoilCropMgNPK(CR,LandSoil,Crop,'FLoss',AllProd,'N') * 1000)             / SUM(CR, vNumha.L(CR,LandSoil,Crop)))$SUM(CR, vNumha.L(CR,LandSoil,Crop));
CropFertBalanceReport(Scenario,'Input','Fixation',LandSoil,Crop)                   = (SUM((CR),             vNumha.L(CR,LandSoil,Crop) * CropOther(Crop,'NFix') * 1000)                            / SUM(CR, vNumha.L(CR,LandSoil,Crop)))$SUM(CR, vNumha.L(CR,LandSoil,Crop));
CropFertBalanceReport(Scenario,'Input','Mineralisation',LandType,SoilType,Crop)    = (SUM((Country,Region), vNumha.L(Country,Region,LandType,SoilType,Crop) * NMineralha(Country,Soiltype) * 1000) / SUM(CR, vNumha.L(CR,LandType,SoilType,Crop)))$SUM(CR, vNumha.L(CR,LandType,SoilType,Crop)); 
CropFertBalanceReport(Scenario,'Input','Deposition',LandSoil,Crop)                 = (SUM((CR),             vNumha.L(CR,LandSoil,Crop) * NDepositionha(CR) * 1000)                                 / SUM(CR, vNumha.L(CR,LandSoil,Crop)))$SUM(CR, vNumha.L(CR,LandSoil,Crop));

CropFertBalanceReport(Scenario,'Output','NH3_Loss',LandSoil,Crop)                  = (SUM((CR),             vCropNH3NMg.L(CR,LandSoil,Crop) * 1000)                                                / SUM(CR, vNumha.L(CR,LandSoil,Crop)))$SUM(CR, vNumha.L(CR,LandSoil,Crop));
CropFertBalanceReport(Scenario,'Output','NOX_Loss',LandSoil,Crop)                  = (SUM((CR),             vCropNOXNMg.L(CR,LandSoil,Crop) * 1000)                                                / SUM(CR, vNumha.L(CR,LandSoil,Crop)))$SUM(CR, vNumha.L(CR,LandSoil,Crop));
CropFertBalanceReport(Scenario,'Output','N2O_Loss',LandSoil,Crop)                  = (SUM((CR),             vCropN2ONDMg.L(CR,LandSoil,Crop) * 1000)                                               / SUM(CR, vNumha.L(CR,LandSoil,Crop)))$SUM(CR, vNumha.L(CR,LandSoil,Crop));
CropFertBalanceReport(Scenario,'Output','N2_Loss',LandSoil,Crop)                   = (SUM((CR),             vCropN2Mg.L(CR,LandSoil,Crop) * 1000)                                                  / SUM(CR, vNumha.L(CR,LandSoil,Crop)))$SUM(CR, vNumha.L(CR,LandSoil,Crop));
CropFertBalanceReport(Scenario,'Output','Leach',LandSoil,Crop)                     = (SUM((CR),             vCropNLeachMg.L(CR,LandSoil,Crop) * 1000)                                              / SUM(CR, vNumha.L(CR,LandSoil,Crop)))$SUM(CR, vNumha.L(CR,LandSoil,Crop));
CropFertBalanceReport(Scenario,'Output','Harvested_N',LandSoil,Crop)               = (SUM((CR),             vNumha.L(CR,LandSoil,Crop) * CropHarvestedNha(CR,LandSoil,Crop,'N') * 1000)            / SUM(CR, vNumha.L(CR,LandSoil,Crop)))$SUM(CR, vNumha.L(CR,LandSoil,Crop));
CropFertBalanceReport(Scenario,'Other','Gebruiksnormen',LandSoil,Crop)             = (SUM((CR),             vNumha.L(CR,LandSoil,Crop) * CropFertNReferenceha(CR,LandSoil,Crop,'N'))               / SUM(CR, vNumha.L(CR,LandSoil,Crop)))$SUM(CR, vNumha.L(CR,LandSoil,Crop));
CropFertBalanceReport(Scenario,'Other','Hectares',LandSoil,Crop)                   =  SUM((CR),             vNumha.L(CR,LandSoil,Crop));



*** Crop Emissions ***

*CO2eqCompostkg(CR,Compost)                                        = ((vCompostCO2eqMg.L(CR,Compost) * 1000) / SUM(LandSoilCrop, vFertOtherNPKMg.L(CR,LandSoilCrop,'Compost',Compost) * 1000))$ SUM(LandSoilCrop, vFertOtherNPKMg.L(CR,LandSoilCrop,'Compost',Compost) * 1000);

CroplandEmissionsCapita(SoilType,Crop,'N2OCO2Cropland',Scenario)  = SUM((CR,LandType), (vCropN2ONDMg.L(CR,LandType,SoilType,Crop) $vCropN2ONDMg.L(CR,LandType,SoilType,Crop) +  vCropN2ONLMg.L(CR,LandType,SoilType,Crop)$vCropN2ONLMg.L(CR,LandType,SoilType,Crop) +  vCropN2ONLMg.L(CR,LandType,SoilType,Crop)$vCropN2ONLMg.L(CR,LandType,SoilType,Crop)) * 1000 * 44/28 * GHGOther('N2O','eqv'))  / SUM((CR,Gender,Age), Population(CR,Gender,Age));
CroplandEmissionsCapita(SoilType,Crop,'NH3Cropland',Scenario)     = SUM((CR,LandType), (vCropNH3NMg.L(CR,LandType,SoilType,Crop)  $vCropNH3NMg.L(CR,LandType,SoilType,Crop)  * 14/30 * 1000))        / SUM((CR,Gender,Age), Population(CR,Gender,Age));
CroplandEmissionsCapita(SoilType,Crop,'NOxCropland',Scenario)     = SUM((CR,LandType), (vCropNOXNMg.L(CR,LandType,SoilType,Crop)  $vCropNOXNMg.L(CR,LandType,SoilType,Crop)  * 17/14 * 1000))        / SUM((CR,Gender,Age), Population(CR,Gender,Age));
CroplandEmissionsCapita(SoilType,Crop,'Leaching',Scenario)        = SUM((CR,LandType), (vCropNLeachMg.L(CR,LandType,SoilType,Crop)$vCropNLeachMg.L(CR,LandType,SoilType,Crop) * 1000))               / SUM((CR,Gender,Age), Population(CR,Gender,Age));
*CroplandEmissionsCapita(SoilType,Crop,'Compost',Scenario)         = SUM((CR,LandType,Compost), (CO2eqCompostkg(CR,Compost) * vFertOtherNPKMg.L(CR,LandType,SoilType,Crop,'Compost',Compost) * 1000)) / SUM((CR,Gender,Age), Population(CR,Gender,Age));


****** Animal Reporting *******
*** Animal Numbers ***
NumHerdTypeC(APSHerdType,Country,Scenario)                                = SUM((Region)$CR(Country,Region), vNumHerdType.L(Country,Region,APSHerdType));
NumHerdTypeR(APSHerdType,CR,Scenario)                                     = vNumHerdType.L(CR,APSHerdType); 
NumProducerC(APS,Level,HerdType,Country,Scenario)$Producer(APS,HerdType)  = NumHerdTypeC(APS,level,HerdType,Country,Scenario)$Producer(APS,HerdType); 
NumProducerR(APS,Level,HerdType,CR,Scenario)$Producer(APS,HerdType)       = NumHerdTypeR(APS,Level,HerdType,CR,Scenario)$Producer(APS,HerdType);
NumAPSC(Country,APS,Scenario)                                             = SUM((Level,HerdType)$APSHerdType(APS,Level,HerdType), NumHerdTypeC(APS,level,HerdType,Country,Scenario));
NumHerdTypeCReport(Scenario,Country,APSHerdType)                          = NumHerdTypeC(APSHerdType,Country,Scenario);

*** Animal Intake APS Level HerdType ***

IntakeHerdTypekgDM(CR,APSHerdType,FeedCat,AllProd)                                  = (vAniFeedIntakeHerdTypeMgDM.L(CR,APSHerdType,FeedCat,AllProd) * 1000)$vAniFeedIntakeHerdTypeMgDM.L(CR,APSHerdType,FeedCat,AllProd);
IntakeHerdTypekgDM(CRA,APSHerdType,FeedCat,Waste)                                   = SUM((CR), (vAniFoodWasteIntakeHerdTypeMgDM.L(CR,CRA,APSHerdType,FeedCat,Waste) * 1000)$vAniFoodWasteIntakeHerdTypeMgDM.L(CR,CRA,APSHerdType,FeedCat,Waste));

IntakeAPSLevelHerdTypeDMProdPerc(CR,APSHerdType,FeedCat,AllProd)                    = (IntakeHerdTypekgDM(CR,APSHerdType,FeedCat,AllProd)                                                                             / SUM((FeedCatA,ALLProdA),                                          IntakeHerdTypekgDM(CR,APSHerdType,FeedCatA,AllProdA)))                    $SUM((FeedCatA,ALLProdA),                                          IntakeHerdTypekgDM(CR,APSHerdType,FeedCatA,AllProdA));
IntakeAPSLevelHerdTypeDMProdPercN0(Country,APSHerdType,FeedCat,AllProd,Scenario)    = (SUM((Region)$CR(Country,Region),                        IntakeHerdTypekgDM(Country,Region,APSHerdType,FeedCat,AllProd))        / SUM((Region,FeedCatA,ALLProdA)$CR(Country,Region),                IntakeHerdTypekgDM(Country,Region,APSHerdType,FeedCatA,AllProdA)))        $SUM((Region,FeedCatA,ALLProdA)$CR(Country,Region),                IntakeHerdTypekgDM(Country,Region,APSHerdType,FeedCatA,AllProdA));
IntakeAPSLevelHerdTypeDMCatPerc(CR,APSHerdType,FeedCat)                             = (SUM(AllProd,                                            IntakeHerdTypekgDM(CR,APSHerdType,FeedCat,AllProd))                    / SUM((FeedCatA,ALLProdA),                                          IntakeHerdTypekgDM(CR,APSHerdType,FeedCatA,AllProdA)))                    $SUM((FeedCatA,ALLProdA),                                          IntakeHerdTypekgDM(CR,APSHerdType,FeedCatA,AllProdA));
IntakeAPSLevelHerdTypeDMCatPercN0(Country,APSHerdType,FeedCat,Scenario)             = (SUM((Region,AllProd)$CR(Country,Region),                IntakeHerdTypekgDM(Country,Region,APSHerdType,FeedCat,AllProd))        / SUM((Region,FeedCatA,ALLProdA)$CR(Country,Region),                IntakeHerdTypekgDM(Country,Region,APSHerdType,FeedCatA,AllProdA)))        $SUM((Region,FeedCatA,ALLProdA)$CR(Country,Region),                IntakeHerdTypekgDM(Country,Region,APSHerdType,FeedCatA,AllProdA));
IntakeAPSLevelDMProdPerc(CR,APSLevel,FeedCat,AllProd)                               = (SUM(HerdType,                                           IntakeHerdTypekgDM(CR,APSLevel,HerdType,FeedCat,AllProd))              / SUM((HerdType,FeedCatA,ALLProdA),                                 IntakeHerdTypekgDM(CR,APSLevel,HerdType,FeedCatA,AllProdA)))              $SUM((HerdType,FeedCatA,ALLProdA),                                 IntakeHerdTypekgDM(CR,APSLevel,HerdType,FeedCatA,AllProdA));
IntakeAPSLevelDMProdPercN0(Country,APSLevel,FeedCat,AllProd,Scenario)               = (SUM((Region,HerdType)$CR(Country,Region),               IntakeHerdTypekgDM(Country,Region,APSLevel,HerdType,FeedCat,AllProd))  / SUM((Region,HerdType,FeedCatA,ALLProdA)$CR(Country,Region),       IntakeHerdTypekgDM(Country,Region,APSLevel,HerdType,FeedCatA,AllProdA)))  $SUM((Region,HerdType,FeedCatA,ALLProdA)$CR(Country,Region),       IntakeHerdTypekgDM(Country,Region,APSLevel,HerdType,FeedCatA,AllProdA));
IntakeAPSLevelDMCatPerc(CR,APSLevel,FeedCat)                                        = (SUM((HerdType,AllProd),                                 IntakeHerdTypekgDM(CR,APSLevel,HerdType,FeedCat,AllProd))              / SUM((HerdType,FeedCatA,ALLProdA),                                 IntakeHerdTypekgDM(CR,APSLevel,HerdType,FeedCatA,AllProdA)))              $SUM((HerdType,FeedCatA,ALLProdA),                                 IntakeHerdTypekgDM(CR,APSLevel,HerdType,FeedCatA,AllProdA));
IntakeAPSLevelDMCatPercN0(Country,APSLevel,FeedCat,Scenario)                        = (SUM((Region,HerdType,AllProd)$CR(Country,Region),       IntakeHerdTypekgDM(Country,Region,APSLevel,HerdType,FeedCat,AllProd))  / SUM((Region,HerdType,FeedCatA,ALLProdA)$CR(Country,Region),       IntakeHerdTypekgDM(Country,Region,APSLevel,HerdType,FeedCatA,AllProdA)))  $SUM((Region,HerdType,FeedCatA,ALLProdA)$CR(Country,Region),       IntakeHerdTypekgDM(Country,Region,APSLevel,HerdType,FeedCatA,AllProdA));
IntakeAPSDMProdPerc(CR,APS,FeedCat,AllProd)                                         = (SUM((Level,HerdType),                                   IntakeHerdTypekgDM(CR,APS,Level,HerdType,FeedCat,AllProd))             / SUM((Level,HerdType,FeedCatA,ALLProdA),                           IntakeHerdTypekgDM(CR,APS,Level,HerdType,FeedCatA,AllProdA)))             $SUM((Level,HerdType,FeedCatA,ALLProdA),                           IntakeHerdTypekgDM(CR,APS,Level,HerdType,FeedCatA,AllProdA));
IntakeAPSDMProdPercN0(Country,APS,FeedCat,AllProd,Scenario)                         = (SUM((Region,Level,HerdType)$CR(Country,Region),         IntakeHerdTypekgDM(Country,Region,APS,Level,HerdType,FeedCat,AllProd)) / SUM((Region,Level,HerdType,FeedCatA,ALLProdA)$CR(Country,Region), IntakeHerdTypekgDM(Country,Region,APS,Level,HerdType,FeedCatA,AllProdA))) $SUM((Region,Level,HerdType,FeedCatA,ALLProdA)$CR(Country,Region), IntakeHerdTypekgDM(Country,Region,APS,Level,HerdType,FeedCatA,AllProdA));
IntakeAPSDMCatPerc(CR,APS,FeedCat)                                                  = (SUM((HerdType,Level,AllProd),                           IntakeHerdTypekgDM(CR,APS,Level,HerdType,FeedCat,AllProd))             / SUM((Level,HerdType,FeedCatA,ALLProdA),                           IntakeHerdTypekgDM(CR,APS,Level,HerdType,FeedCatA,AllProdA)))             $SUM((Level,HerdType,FeedCatA,ALLProdA),                           IntakeHerdTypekgDM(CR,APS,Level,HerdType,FeedCatA,AllProdA));
IntakeAPSDMCatPercN0(Country,APS,FeedCat,Scenario)                                  = (SUM((Region,Level,HerdType,AllProd)$CR(Country,Region), IntakeHerdTypekgDM(Country,Region,APS,Level,HerdType,FeedCat,AllProd)) / SUM((Region,Level,HerdType,FeedCatA,ALLProdA)$CR(Country,Region), IntakeHerdTypekgDM(Country,Region,APS,Level,HerdType,FeedCatA,AllProdA))) $SUM((Region,Level,HerdType,FeedCatA,ALLProdA)$CR(Country,Region), IntakeHerdTypekgDM(Country,Region,APS,Level,HerdType,FeedCatA,AllProdA));

AniIntakeKgDMDay(Scenario,Country,APSHerdType,FeedCat,AllProd) = (SUM(Region, IntakeHerdTypekgDM(Country,Region,APSHerdType,FeedCat,AllProd)$IntakeHerdTypekgDM(Country,Region,APSHerdType,FeedCat,AllProd) / 365) / SUM((Region)$CR(Country,Region), vNumHerdType.L(Country,Region,APSHerdType)))$SUM((Region)$CR(Country,Region), vNumHerdType.L(Country,Region,APSHerdType));

AniIntakeFeedCatDMDay(Scenario,Country,APSHerdType,FeedCat)  = SUM(AllProd, AniIntakeKgDMDay(Scenario,Country,APSHerdType,FeedCat,AllProd));

AniIntakeTotalkgDMDay(Scenario,Country,APSHerdType)            = SUM((FeedCat,AllProd), AniIntakeKgDMDay(Scenario,Country,APSHerdType,FeedCat,AllProd));

AniNutrIntake(Scenario,CRA,APSHerdType,AniNutr)$AniNutrReq(APSHerdType,AniNutr) = SUM((FeedCat,AllProd)$AniNutrByProduct(FeedCat,AllProd,AniNutr),     vAniFeedIntakeHerdTypeMgDM.L(CRA,APSHerdType,FeedCat,AllProd) * 1000 * AniNutrByProduct(FeedCat,AllProd,AniNutr)) +
                                                                                  SUM((CR,FeedCat,Waste)$AniNutrByProductCR(CR,FeedCat,Waste,AniNutr), vAniFoodWasteIntakeHerdTypeMgDM.L(CR,CRA,APSHerdType,FeedCat,Waste)$vAniFoodWasteIntakeHerdTypeMgDM.L(CR,CRA,APSHerdType,FeedCat,Waste) * 1000 * AniNutrByProductCR(CR,FeedCat,Waste,AniNutr));

AniNutrIntakeProd(Scenario,CRA,APSHerdType,FeedCat,AllProd,AniNutr)$AniNutrReq(APSHerdType,AniNutr) = (vAniFeedIntakeHerdTypeMgDM.L(CRA,APSHerdType,FeedCat,AllProd) * 1000 * AniNutrByProduct(FeedCat,AllProd,AniNutr)) +
                                                                                                                   SUM((CR), vAniFoodWasteIntakeHerdTypeMgDM.L(CR,CRA,APSHerdType,FeedCat,AllProd)$vAniFoodWasteIntakeHerdTypeMgDM.L(CR,CRA,APSHerdType,FeedCat,AllProd) * 1000 * AniNutrByProductCR(CR,FeedCat,AllProd,AniNutr));

AniNutrIntakeDay(Scenario,Country,APSHerdType,AniNutr)$AniNutrReq(APSHerdType,AniNutr) = (SUM(Region, AniNutrIntake(Scenario,Country,Region,APSHerdType,AniNutr) / 365) / SUM((Region)$CR(Country,Region), vNumHerdType.L(Country,Region,APSHerdType)))$SUM((Region)$CR(Country,Region), vNumHerdType.L(Country,Region,APSHerdType));

AniNutrIntakeProdDay(Scenario,Country,APSHerdType,FeedCat,AllProd,AniNutr) = (SUM(Region, AniNutrIntakeProd(Scenario,Country,Region,APSHerdType,FeedCat,AllProd,AniNutr) / 365) / SUM((Region)$CR(Country,Region), vNumHerdType.L(Country,Region,APSHerdType)))$SUM((Region)$CR(Country,Region), vNumHerdType.L(Country,Region,APSHerdType));

AniNutrSurplus(Scenario,Country,APSHerdType,AniNutr)$(AniNutrReq(APSHerdType,AniNutr) AND NumHerdTypeC(APSHerdType,Country,Scenario)) = (AniNutrIntakeDay(Scenario,Country,APSHerdType,AniNutr) / (AniNutrReq(APSHerdType,AniNutr) / 365))$(AniNutrReq(APSHerdType,AniNutr) / 365) / 1000;

AniIntakeAPSkgDM(Scenario,Country,APS,FeedCat) =  SUM((Region,Level,HerdType,AllProd)$(CR(Country,Region) AND APSHerdType(APS,Level,HerdType) AND FeedCatAllProd(FeedCat,AllProd)), IntakeHerdTypekgDM(Country,Region,APS,level,Herdtype,FeedCat,AllProd));

*** Animal Emissions ***

AnimalCO2eqCapita(APS,Level,'CH4Manure',Scenario)                 = SUM((CR,HerdType), vAniCH4ManureMg.L(CR,APS,Level,HerdType)    $vAniCH4ManureMg.L(CR,APS,Level,HerdType)         * GHGOther('CH4','eqv') * 1000) / SUM((CR,Gender,Age), Population(CR,Gender,Age));
AnimalCO2eqCapita(Rumi,Level,'CH4Enteric',Scenario)               = SUM((CR,HerdType), vAnivCH4EntericMg.L(CR,Rumi,Level,HerdType) $vAnivCH4EntericMg.L(CR,Rumi,Level,HerdType)      * GHGOther('CH4','eqv') * 1000) / SUM((CR,Gender,Age), Population(CR,Gender,Age));
AnimalCO2eqCapita(APS,Level,'N2ODManure',Scenario)                = SUM((CR,HerdType), vAniN2ONDMMMg.L(CR,APS,Level,HerdType)      $vAniN2ONDMMMg.L(CR,APS,Level,HerdType)   * 44/28 * GHGOther('N2O','eqv') * 1000) / SUM((CR,Gender,Age), Population(CR,Gender,Age));
AnimalCO2eqCapita(APS,Level,'N2OIDManure',Scenario)               = SUM((CR,HerdType), vAniN2ONIDMMMg.L(CR,APS,Level,HerdType)     $vAniN2ONIDMMMg.L(CR,APS,Level,HerdType)  * 44/28 * GHGOther('N2O','eqv') * 1000) / SUM((CR,Gender,Age), Population(CR,Gender,Age));

****** Product use *******

ProductUsePerc(CR,FLCat,'Feed')                           = (SUM((CRA,APWaste),                            vFLWFeedRegionImportExportMg.L(CR,CRA,FLCat,APWaste))              / SUM(APWaste,                             vFoodLossMg.L(CR,FLCat,APWaste)             * ScenarioControlsModel('Util.FoodLoss')))$SUM(APWaste,                             vFoodLossMg.L(CR,FLCat,APWaste)             * ScenarioControlsModel('Util.FoodLoss'));
ProductUsePerc(CR,FLCat,'Fertiliser')                     = (SUM(APWaste,                                  vCompostFoodLossMg.L(CR,FLCat,APWaste))                            / SUM(APWaste,                             vFoodLossMg.L(CR,FLCat,APWaste)             * ScenarioControlsModel('Util.FoodLoss')))$SUM(APWaste,                             vFoodLossMg.L(CR,FLCat,APWaste)             * ScenarioControlsModel('Util.FoodLoss'));
ProductUsePercN0(Country,FLCat,'Feed',Scenario)           = (SUM((Region,CRA,APWaste)$CR(Country,Region),  vFLWFeedRegionImportExportMg.L(Country,Region,CRA,FLCat,APWaste))  / SUM((Region,APWaste)$CR(Country,Region), vFoodLossMg.L(Country,Region,FLCat,APWaste) * ScenarioControlsModel('Util.FoodLoss')))$SUM((Region,APWaste)$CR(Country,Region), vFoodLossMg.L(Country,Region,FLCat,APWaste) * ScenarioControlsModel('Util.FoodLoss'));
ProductUsePercN0(Country,FLCat,'Fertiliser',Scenario)     = (SUM((Region,APWaste)$CR(Country,Region),      vCompostFoodLossMg.L(Country,Region,FLCat,APWaste))                / SUM((Region,APWaste)$CR(Country,Region), vFoodLossMg.L(Country,Region,FLCat,APWaste) * ScenarioControlsModel('Util.FoodLoss')))$SUM((Region,APWaste)$CR(Country,Region), vFoodLossMg.L(Country,Region,FLCat,APWaste) * ScenarioControlsModel('Util.FoodLoss'));

ProductUsePerc(CR,FWCat,'Feed')                           = (SUM((CRA,Waste),                              vFLWFeedRegionImportExportMg.L(CR,CRA,FWCat,Waste))                / SUM((ProcOutH),                        vFoodWasteMg.L(CR,FWCat,ProcOutH)             * ScenarioControlsModel('Util.FoodWaste')))$SUM((ProcOutH),                         vFoodWasteMg.L(CR,FWCat,ProcOutH)             * ScenarioControlsModel('Util.FoodWaste'));
ProductUsePerc(CR,FWCat,'Fertiliser')                     = (SUM((Waste),                                  vCompostFoodWasteMg.L(CR,FWCat,Waste))                             / SUM((ProcOutH),                        vFoodWasteMg.L(CR,FWCat,ProcOutH)             * ScenarioControlsModel('Util.FoodWaste')))$SUM((ProcOutH),                         vFoodWasteMg.L(CR,FWCat,ProcOutH)             * ScenarioControlsModel('Util.FoodWaste'));
ProductUsePercN0(Country,FWCat,'Feed',Scenario)           = (SUM((Region,CRA,Waste)$CRA(Country,Region),   vFLWFeedRegionImportExportMg.L(Country,Region,CRA,FWCat,Waste))    / SUM((Region,ProcOutH)$CR(Country,Region), vFoodWasteMg.L(Country,Region,FWCat,ProcOutH) * ScenarioControlsModel('Util.FoodWaste')))$SUM((Region,ProcOutH)$CR(Country,Region),  vFoodWasteMg.L(Country,Region,FWCat,ProcOutH) * ScenarioControlsModel('Util.FoodWaste'));
ProductUsePercN0(Country,FWCat,'Fertiliser',Scenario)     = (SUM((Region,Waste)$CR(Country,Region),        vCompostFoodWasteMg.L(Country,Region,FWCat,Waste))                 / SUM((Region,ProcOutH)$CR(Country,Region), vFoodWasteMg.L(Country,Region,FWCat,ProcOutH) * ScenarioControlsModel('Util.FoodWaste')))$SUM((Region,ProcOutH)$CR(Country,Region),  vFoodWasteMg.L(Country,Region,FWCat,ProcOutH) * ScenarioControlsModel('Util.FoodWaste'));

ProductUsePerc(CR,'CoProduct','Feed')                       = (SUM((APS,level,HerdType,ProcOutCP)$(APSHerdType(APS,Level,HerdType) AND AniFeedSuitability(APS,'CoProduct',ProcOutCP)), ((vAniFeedIntakeHerdTypeMgDM.L(CR,APS,Level,HerdType,'CoProduct',ProcOutCP) * (1 + AniFeedLossFrac('CoProduct',FeedLoss,ProcOutCP))) / (AniNutrByProduct('CoProduct',ProcOutCP,'DM') * 0.001))) / SUM(ProcOutCP, vProcOutMg.L(CR,ProcOutCP) * ScenarioControlsModel('Util.CoProduct')))
                                                              $SUM((ProcOutCP), vProcOutMg.L(CR,ProcOutCP) * ScenarioControlsModel('Util.CoProduct'));
ProductUsePerc(CR,'CoProduct','Fertiliser')                 = (SUM(ProcOutCP,  vCompostCoProductMg.L(CR,'CoProduct',ProcOutCP))                                / SUM(ProcOutCP, vProcOutMg.L(CR,ProcOutCP) * ScenarioControlsModel('Util.CoProduct')))
                                                              $SUM((ProcOutCP), vProcOutMg.L(CR,ProcOutCP) * ScenarioControlsModel('Util.CoProduct'));
ProductUsePercN0(Country,'CoProduct','Feed',Scenario)       = (SUM((Region,APS,level,HerdType,ProcOutCP)$(CR(Country,Region) AND APSHerdType(APS,Level,HerdType) AND AniFeedSuitability(APS,'CoProduct',ProcOutCP)), ((vAniFeedIntakeHerdTypeMgDM.L(Country,Region,APS,Level,HerdType,'CoProduct',ProcOutCP) * (1 + AniFeedLossFrac('CoProduct',FeedLoss,ProcOutCP))) / (AniNutrByProduct('CoProduct',ProcOutCP,'DM') * 0.001))) / SUM((Region,ProcOutCP)$CR(Country,Region), vProcOutMg.L(Country,Region,ProcOutCP) * ScenarioControlsModel('Util.CoProduct')))
                                                              $SUM((Region,ProcOutCP)$CR(Country,Region), vProcOutMg.L(Country,Region,ProcOutCP) * ScenarioControlsModel('Util.CoProduct'));
ProductUsePercN0(Country,'CoProduct','Fertiliser',Scenario) = (SUM((Region,ProcOutCP)$CR(Country,Region), vCompostCoProductMg.L(Country,Region,'CoProduct',ProcOutCP)) / SUM((Region,ProcOutCP)$CR(Country,Region), vProcOutMg.L(Country,Region,ProcOutCP) * ScenarioControlsModel('Util.CoProduct')))$SUM((Region,ProcOutCP)$CR(Country,Region), vProcOutMg.L(Country,Region,ProcOutCP) * ScenarioControlsModel('Util.CoProduct'));




****** Feed Available ******

AniFeedConsumedMgDM(Scenario,CountryA,FeedCat,AllProd) = SUM((RegionA,APSHerdType)$CR(CountryA,RegionA), vAniFeedIntakeHerdTypeMgDM.L(CountryA,RegionA,APSHerdType,FeedCat,AllProd)$vAniFeedIntakeHerdTypeMgDM.L(CountryA,RegionA,APSHerdType,FeedCat,AllProd) +
                                                         SUM((CR)$CRA(CountryA,RegionA), vAniFoodWasteIntakeHerdTypeMgDM.L(CR,CountryA,RegionA,APSHerdType,FeedCat,AllProd)$vAniFoodWasteIntakeHerdTypeMgDM.L(CR,CountryA,RegionA,APSHerdType,FeedCat,AllProd)));

FeedAvailableMgDM(Scenario,Country,FLCat,APWaste)          = SUM((Region)$CR(Country,Region),                                   vFoodLossMg.L(Country,Region,FLCat,APWaste)                              $vFoodLossMg.L(Country,Region,FLCat,APWaste)                              * ScenarioControlsModel('Util.FoodLoss')  * 1000 * AniNutrByProduct(FLCat,APWaste,'DM') * 0.001);
FeedAvailableMgDM(Scenario,Country,FWCat,Waste)            = SUM((Region)$CR(Country,Region),                                   SUM((ProcOutH)$WasteTypeCatProd(FWCat,ProcOutH,Waste), vFoodWasteMg.L(Country,Region,FWCat,ProcOutH))                                              * ScenarioControlsModel('Util.FoodWaste') * AniNutrByProductCR(Country,Region,FWCat,Waste,'DM') * 0.001);
FeedAvailableMgDM(Scenario,Country,'CoProduct',ProcOutCP)  = SUM((Region)$CR(Country,Region),                                   vProcOutMg.L(Country,Region,ProcOutCP)                                   $vProcOutMg.L(Country,Region,ProcOutCP)                                   * ScenarioControlsModel('Util.CoProduct')   * AniNutrByProduct('CoProduct',ProcOutCP,'DM') * 0.001);
FeedAvailableMgDM(Scenario,Country,'FoodFeed',ProcOutFF)   = SUM((Region)$CR(Country,Region),                                   vProcOutRegionImportMg.L(Country,Region,ProcOutFF)                       $vProcOutRegionImportMg.L(Country,Region,ProcOutFF)                                                                   * AniNutrByProduct('FoodFeed',ProcOutFF,'DM') * 0.001);

AniNutrByFWC(Scenario,Country,FWCat,Waste,AniNutr) = (SUM((Region,ProcOutH)$(CR(Country,Region) AND WasteTypeCatProd(FWCat,ProcOutH,Waste)), (vFoodWasteMg.L(Country,Region,FWCat,ProcOutH) * AniNutrByProduct('PPLoss',ProcOutH,AniNutr))) / SUM((Region,ProcOutH)$(CR(Country,Region) AND WasteTypeCatProd(FWCat,ProcOutH,Waste)),  vFoodWasteMg.L(Country,Region,FWCat,ProcOutH)))$SUM((Region,ProcOutH)$(CR(Country,Region) AND WasteTypeCatProd(FWCat,ProcOutH,Waste)),  vFoodWasteMg.L(Country,Region,FWCat,ProcOutH));

***** Scenario Results ******
ScenarioResults('Crop',Scenario)        = SUM((CR,LandSoilCrop),   vCropCO2eqTotalMg.L(CR,LandSoilCrop)$vCropCO2eqTotalMg.L(CR,LandSoilCrop) * 1000) / SUM((CR,Gender,Age), Population(CR,Gender,Age));
ScenarioResults('Livestock',Scenario)   = SUM((CR,APSHerdType),    vAniCO2eqTotalMg.L(CR,APSHerdType)  $vAniCO2eqTotalMg.L(CR,APSHerdType)   * 1000) / SUM((CR,Gender,Age), Population(CR,Gender,Age));
ScenarioResults('CO2eqTrans',Scenario)  = SUM((CR),                vTransCO2Mg.L(CR)                   $vTransCO2Mg.L(CR)                    * 1000) / SUM((CR,Gender,Age), Population(CR,Gender,Age));
ScenarioResults('CO2eqCFish',Scenario)  = SUM((CR),                vMarineFishCO2Mg.L(CR)              $vMarineFishCO2Mg.L(CR)               * 1000) / SUM((CR,Gender,Age), Population(CR,Gender,Age));
ScenarioResults('CO2eqTotal',Scenario)  = SUM((CR),                vGHGRegionCO2eqMg.L(CR)             $vGHGRegionCO2eqMg.L(CR)              * 1000) / SUM((CR,Gender,Age), Population(CR,Gender,Age));
ScenarioResults('LandUse',Scenario)     = ((SUM((CR,LandSoilCrop), vNumha.L(CR,LandSoilCrop)) + SUM((CR,ProcOutAA), vAminoAcidMg.L(CR,'AminoAcid',ProcOutAA) * AniAminoAcidLU(ProcOutAA))) /
                                           SUM((CR,Gender,Age), Population(CR,Gender,Age)))$SUM((CR,Gender,Age), Population(CR,Gender,Age));

EmissionResults(Scenario,'CH4_Ani')   = SUM((CR,APSHerdType),  vAniCH4ManureMg.L(CR,APSHerdType) + vAnivCH4EntericMg.L(CR,APSHerdType));
EmissionResults(Scenario,'NH3_Crop')  = SUM((CR,LandSoilCrop), vCropNH3NMg.L(CR,LandSoilCrop))   * 17/14;
EmissionResults(Scenario,'NH3_Ani')   = SUM((CR,APSHerdType),  vAniNH3NMMMg.L(CR,APSHerdType))   * 17/14;
EmissionResults(Scenario,'NOX_Crop')  = SUM((CR,LandSoilCrop), vCropNOXNMg.L(CR,LandSoilCrop))   * 30/14;
EmissionResults(Scenario,'NOX_Ani')   = SUM((CR,APSHerdType),  vAniNOXNMMMg.L(CR,APSHerdType))   * 30/14;
EmissionResults(Scenario,'N2O_Crop')  = SUM((CR,LandSoilCrop), vCropN2ONDMg.L(CR,LandSoilCrop)   +
                                                               vCropN2ONLMg.L(CR,LandSoilCrop)   +
                                                               vCropN2ONVMg.L(CR,LandSoilCrop))  * 44/28;
EmissionResults(Scenario,'N2O_Ani') =  SUM((CR,APSHerdType),   vAniN2ONDMMMg.L(CR,APSHerdType)         +                           
                                                               vAniN2ONIDMMMg.L(CR,APSHerdType)) * 44/28;
;                                                               
EmissionResults(Scenario,'Acidification_Crop') = EmissionResults(Scenario,'NH3_Crop') * 1.88 + EmissionResults(Scenario,'NOX_Crop') * 0.7;
EmissionResults(Scenario,'GWP_Crop')           = EmissionResults(Scenario,'N2O_Crop') * GHGOther('N2O','eqv');
EmissionResults(Scenario,'Acidification_Ani')  = EmissionResults(Scenario,'NH3_Ani') * 1.88 + EmissionResults(Scenario,'NOX_Ani') * 0.7;
EmissionResults(Scenario,'GWP_Ani')            = EmissionResults(Scenario,'N2O_Ani') * GHGOther('N2O','eqv') + EmissionResults(Scenario,'CH4_Ani') * GHGOther('CH4','eqv');                             
EmissionResults(Scenario,'Acidification')      = EmissionResults(Scenario,'Acidification_Crop') + EmissionResults(Scenario,'Acidification_Ani');
EmissionResults(Scenario,'GWP')                = EmissionResults(Scenario,'GWP_Ani') + EmissionResults(Scenario,'GWP_Crop');
;
***** Import and Export ******

ImportExportProcInMg(Country,ProcIn,'Import',Scenario)   = SUM((Region)$CR(Country,Region), vProcInNationalImportExportMg.L(Country,Region,'Import',ProcIn));
ImportExportProcInMg(Country,ProcIn,'Export',Scenario)   = SUM((Region)$CR(Country,Region), vProcInNationalImportExportMg.L(Country,Region,'Export',ProcIn));

ImportExportProcOutHMg(Country,ProcOutH,'Import',Scenario) = SUM((Region)$CR(Country,Region), vProcOutHNationalImportExportMg.L(Country,Region,'Import',ProcOutH));
ImportExportProcOutHMg(Country,ProcOutH,'Export',Scenario) = SUM((Region)$CR(Country,Region), vProcOutHNationalImportExportMg.L(Country,Region,'Export',ProcOutH));

ImportExportFamMg(Country,Fam,ImportExport,Scenario) = SUM((Region,ProcOutH)$(CR(Country,Region) AND ImportExportProdFam('ProcOutH',Fam,ProcOutH)),   vProcOutHNationalImportExportMg.L(Country,Region,ImportExport,ProcOutH)) +
                                                       SUM((Region,ProcIn)$(CR(Country,Region)  AND ImportExportProdFam('ProcIn',Fam,ProcIn)),     vProcInNationalImportExportMg.L(Country,Region,ImportExport,ProcIn));
 
NutrientImportDiet(Scenario,Country,HumNutrR) = (SUM((Region,ProcOutH)$CR(Country,Region), vProcOutHNationalImportExportMg.L(Country,Region,'Import',ProcOutH)* (1 - FoodLossWasteFrac('ProcOutH',DWaste,ProcOutH)) * (1 - FoodLossWasteFrac('ProcOutH',CWaste,ProcOutH))  * HumNutrByProduct(ProcOutH,HumNutrR)) /     
                                                 SUM((Region,ProcOutH)$CR(Country,Region), vProcOutHMg.L(Country,Region,ProcOutH) * (1 - FoodLossWasteFrac('ProcOutH',DWaste,ProcOutH)) * (1 - FoodLossWasteFrac('ProcOutH',CWaste,ProcOutH)) * HumNutrByProduct(ProcOutH,HumNutrR)))$
                                                 SUM((Region,ProcOutH)$CR(Country,Region), vProcOutHMg.L(Country,Region,ProcOutH) * (1 - FoodLossWasteFrac('ProcOutH',DWaste,ProcOutH)) * (1 - FoodLossWasteFrac('ProcOutH',CWaste,ProcOutH)) * HumNutrByProduct(ProcOutH,HumNutrR))
;
ReferenceImportExportFamMg(Country,Fam,ImportExport) = SUM((Region,ProcOutH)$(CR(Country,Region) AND ImportExportProdFam('ProcOutH',Fam,ProcOutH)), ImportImportExport('Reference','ProcOutH',Country,Region,ProcOutH,ImportExport)) +
                                                       SUM((Region,ProcIn)$(CR(Country,Region)  AND ImportExportProdFam('ProcIn',Fam,ProcIn)), ImportImportExport('Reference','ProcIn',Country,Region,ProcIn,ImportExport))
;
ReferenceImportExportNPKMg(Country,ImportExport,FertNutr) = SUM((Region,ProcIn),  ImportImportExport('Reference','ProcIn',Country,Region,ProcIn,ImportExport)   * FertNutrByProductMgMg('PHLoss',ProcIn,FertNutr)) +
                                                            SUM((Region,ProcOutH), ImportImportExport('Reference','ProcOutH',Country,Region,ProcOutH,ImportExport) * FertNutrByProductMgMg('ProcOutH',ProcOutH,FertNutr));


******* Scenario Reports for R ******
ReportingGHG(Scenario)                              = EPS;
ReportingLU(Scenario)                               = EPS;
ReportingDiet(Scenario,Fam)                         = EPS;
ReportingExport(Scenario,ImportExport,Fam)          = EPS;
ReportingLUFamily(Scenario,Fam)                     = EPS;
ReportingAnimal(Scenario,APS)                       = EPS;

ReportingGHG(Scenario)$(ScenarioResults('CO2eqTotal',Scenario))                                                 = ScenarioResults('CO2eqTotal',Scenario);
ReportingLU(Scenario)$(SUM((Country,Fam), CrophaCFam(Country,Fam,Scenario)))                                    = SUM((Country,Fam), CrophaCFam(Country,Fam,Scenario)) +
                                                                                                                  SUM((CR,APS,level,HerdType,ProcOutAA)$(APSHerdType(APS,Level,HerdType) AND AniFeedSuitability(APS,'AminoAcid',ProcOutAA)), vAniFeedIntakeHerdTypeMgDM.L(CR,APS,level,HerdType,'AminoAcid',ProcOutAA) * AniAminoAcidLU(ProcOutAA)) ;
ReportingDiet(Scenario,Fam)$(SUM(Country, DailyIntakeFamily(Country,Fam,Scenario)))                             = SUM(Country, DailyIntakeFamily(Country,Fam,Scenario));
ReportingExport(Scenario,ImportExport,Fam)$(SUM(Country, ImportExportFamMg(Country,Fam,ImportExport,Scenario))) = SUM(Country, ImportExportFamMg(Country,Fam,ImportExport,Scenario));
ReportingLUFamily(Scenario,Fam)$(SUM(Country, CrophaCFam(Country,Fam,Scenario)))                                = SUM(Country, CrophaCFam(Country,Fam,Scenario));
ReportingAnimal(Scenario,APS)$(SUM(Country, NumAPSC(Country,APS,Scenario)))                                     = SUM(Country, NumAPSC(Country,APS,Scenario));

ReportingTradeNPK(Scenario,ImportExport,FertNutr) = SUM((Country,ProcIn), ImportExportProcInMg(Country,ProcIn,ImportExport,Scenario) * FertNutrByProductMgMg('PHLoss',ProcIn,FertNutr)) +
                                                    SUM((Country,ProcOutH), ImportExportProcOutHMg(Country,ProcOutH,ImportExport,Scenario) * FertNutrByProductMgMg('ProcOutH',ProcOutH,FertNutr));


****** Values to be re-calculated for next model run ******
if (ord(iter) <= card(iter),
* Calculate values at a regional level
GrazingProportionCRIter(Scenario,Iter,CRA,Rumi,level,HerdType)$(vNumHerdType.L(CRA,Rumi,Level,HerdType)>1)=
                                                    ((SUM((FreshGrass)$AniFeedSuitability(Rumi,'GrassFresh',FreshGrass), vAniFeedIntakeHerdTypeMgDM.L(CRA,Rumi,level,HerdType,'GrassFresh',FreshGrass))            /
                                                     (SUM((FeedCat,AllProd)$AniFeedSuitability(Rumi,FeedCat,AllProd),    vAniFeedIntakeHerdTypeMgDM.L(CRA,Rumi,level,HerdType,FeedCat,AllProd))             +
                                                      SUM((CR,FWCat,Waste)$AniFeedSuitability(Rumi,FWCat,Waste),         vAniFoodWasteIntakeHerdTypeMgDM.L(CR,CRA,Rumi,level,HerdType,FWCat,Waste)))))
                                                    $(SUM((FeedCat,AllProd)$AniFeedSuitability(Rumi,FeedCat,AllProd),    vAniFeedIntakeHerdTypeMgDM.L(CRA,Rumi,level,HerdType,FeedCat,AllProd))             +
                                                      SUM((CR,FWCat,Waste)$AniFeedSuitability(Rumi,FWCat,Waste),         vAniFoodWasteIntakeHerdTypeMgDM.L(CR,CRA,Rumi,level,HerdType,FWCat,Waste)))
;
AniNutrByProductCRIter(Scenario,Iter,CR,FWCat,Waste,AniNutr)$FeedCatAllProd(FWCat,Waste) =
         (SUM((ProcOutH)$WasteTypeCatProd(FWCat,ProcOutH,Waste), (vFoodWasteMg.L(CR,FWCat,ProcOutH) * AniNutrByProduct('PPLoss',ProcOutH,AniNutr))) / SUM((ProcOutH)$WasteTypeCatProd(FWCat,ProcOutH,Waste),  vFoodWasteMg.L(CR,FWCat,ProcOutH)))$SUM((ProcOutH)$WasteTypeCatProd(FWCat,ProcOutH,Waste),  vFoodWasteMg.L(CR,FWCat,ProcOutH))
;
AniNutrByProductCRIter(Scenario,Iter,CR,FWCat,Waste,'FM')$FeedCatAllProd(FWCat,Waste) = (1000 / (AniNutrByProductCRIter(Scenario,Iter,CR,FWCat,Waste,'DM') * 0.001))$AniNutrByProductCRIter(Scenario,Iter,CR,FWCat,Waste,'DM')
;
ManureTanCRIter(Scenario,Iter,CR,'Manure',ManureProd)  =
         (SUM((APSManure,Level,HerdType)$(APSManureProd(ManureProd,APSManure) AND (vNumHerdType.L(CR,APSManure,Level,HerdType)>1)), vAniTANExcMMMg.L(CR,APSManure,Level,HerdType)) / SUM((APSManure,Level,HerdType)$(APSManureProd(ManureProd,APSManure) AND (vNumHerdType.L(CR,APSManure,Level,HerdType)>1)), vAniManureMMMg.L(CR,APSManure,Level,HerdType,'N')))$SUM((APSManure,Level,HerdType)$(APSManureProd(ManureProd,APSManure) AND (vNumHerdType.L(CR,APSManure,Level,HerdType)>1)), vAniManureMMMg.L(CR,APSManure,Level,HerdType,'N'))
;
ManureTanCRIter(Scenario,Iter,CR,'GrazingManure',ManureProd)  =
         (SUM((Rumi,Level,HerdType)$(APSManureProd(ManureProd,Rumi) AND (vNumHerdType.L(CR,Rumi,Level,HerdType)>1)), vAniTANExcGMg.L(CR,Rumi,Level,HerdType)) /  SUM((Rumi)$APSManureProd(ManureProd,Rumi), vAniManureGMg.L(CR,Rumi,'N')))$SUM((Rumi)$APSManureProd(ManureProd,Rumi), vAniManureGMg.L(CR,Rumi,'N'))
;
NPKFractionCRIter(Scenario,Iter,CR,'Manure',ManureProd,FertNutr) =
         (SUM((APSManure)$APSManureProd(ManureProd,APSManure),           SUM((Level,Herdtype)$(vNumHerdType.L(CR,APSManure,Level,HerdType)>1), vAniManureMMMg.L(CR,APSManure,Level,HerdType,FertNutr)  - vManureNLossesMg.L(CR,APSManure,Level,HerdType,FertNutr))) /
          SUM((APSManure,FertNutrA)$APSManureProd(ManureProd,APSManure), SUM((Level,Herdtype)$(vNumHerdType.L(CR,APSManure,Level,HerdType)>1), vAniManureMMMg.L(CR,APSManure,Level,HerdType,FertNutrA) - vManureNLossesMg.L(CR,APSManure,Level,HerdType,FertNutrA))))
         $SUM((APSManure,FertNutrA)$APSManureProd(ManureProd,APSManure), SUM((Level,Herdtype)$(vNumHerdType.L(CR,APSManure,Level,HerdType)>1), vAniManureMMMg.L(CR,APSManure,Level,HerdType,FertNutrA) - vManureNLossesMg.L(CR,APSManure,Level,HerdType,FertNutrA)))
;
NPKFractionCRIter(Scenario,Iter,CR,'GrazingManure',ManureProd,FertNutr) =
         (SUM((Rumi)$APSManureProd(ManureProd,Rumi),           vAniManureGMg.L(CR,Rumi,FertNutr)) /
          SUM((Rumi,FertNutrA)$APSManureProd(ManureProd,Rumi), vAniManureGMg.L(CR,Rumi,FertNutrA)))
         $SUM((Rumi,FertNutrA)$APSManureProd(ManureProd,Rumi), vAniManureGMg.L(CR,Rumi,FertNutrA))
;
NPKFractionCRIter(Scenario,Iter,Country,Region,'Compost',Compost,FertNutr)$CRA(Country,Region)=
       (((SUM((FLCat,APWaste)$CompostTypeCatProd(FLCat,APWaste,Compost),        vCompostFoodLossMg.L(Country,Region,FLCat,APWaste)          * (AniNutrByProduct(FLCat,APWaste,'DM')    * 0.001)                  * (AniNutrByProduct(FLCat,APWaste,FertNutr)     * 0.001)) +
          SUM((FWCat,Waste)$CompostTypeCatProd(FWCat,Waste,Compost),            vCompostFoodWasteMg.L(Country,Region,FWCat,Waste)           * (AniNutrByProductCR(Country,Region,FWCat,Waste,'DM') * 0.001)      * (AniNutrByProductCR(Country,Region,FWCat,Waste,FertNutr)  * 0.001)) +
          SUM((ProcOutCP)$CompostTypeCatProd('CoProduct',ProcOutCP,Compost),    vCompostCoProductMg.L(Country,Region,'CoProduct',ProcOutCP) * (AniNutrByProduct('CoProduct',ProcOutCP,'DM') * 0.001)             * (AniNutrByProduct('CoProduct',ProcOutCP,FertNutr) * 0.001))) *  (1-EFC(Country,Compost,FertNutr))) /
          SUM(FertNutrA,
         (SUM((FLCat,APWaste)$CompostTypeCatProd(FLCat,APWaste,Compost),        vCompostFoodLossMg.L(Country,Region,FLCat,APWaste)          * (AniNutrByProduct(FLCat,APWaste,'DM')    * 0.001)                  * (AniNutrByProduct(FLCat,APWaste,FertNutrA)    * 0.001)) +
          SUM((FWCat,Waste)$CompostTypeCatProd(FWCat,Waste,Compost),            vCompostFoodWasteMg.L(Country,Region,FWCat,Waste)           * (AniNutrByProductCR(Country,Region,FWCat,Waste,'DM') * 0.001)      * (AniNutrByProductCR(Country,Region,FWCat,Waste,FertNutrA) * 0.001)) +
          SUM((ProcOutCP)$CompostTypeCatProd('CoProduct',ProcOutCP,Compost),    vCompostCoProductMg.L(Country,Region,'CoProduct',ProcOutCP) * (AniNutrByProduct('CoProduct',ProcOutCP,'DM') * 0.001)             * (AniNutrByProduct('CoProduct',ProcOutCP,FertNutrA) * 0.001))) *  (1-EFC(Country,Compost,FertNutrA))))
        $ SUM(FertNutrA,
         (SUM((FLCat,APWaste)$CompostTypeCatProd(FLCat,APWaste,Compost),        vCompostFoodLossMg.L(Country,Region,FLCat,APWaste)          * (AniNutrByProduct(FLCat,APWaste,'DM')    * 0.001)                  * (AniNutrByProduct(FLCat,APWaste,FertNutrA)    * 0.001)) +
          SUM((FWCat,Waste)$CompostTypeCatProd(FWCat,Waste,Compost),            vCompostFoodWasteMg.L(Country,Region,FWCat,Waste)           * (AniNutrByProductCR(Country,Region,FWCat,Waste,'DM') * 0.001)      * (AniNutrByProductCR(Country,Region,FWCat,Waste,FertNutrA) * 0.001)) +
          SUM((ProcOutCP)$CompostTypeCatProd('CoProduct',ProcOutCP,Compost),    vCompostCoProductMg.L(Country,Region,'CoProduct',ProcOutCP) * (AniNutrByProduct('CoProduct',ProcOutCP,'DM') * 0.001)             * (AniNutrByProduct('CoProduct',ProcOutCP,FertNutrA) * 0.001))) *  (1-EFC(Country,Compost,FertNutrA)))
;
NPKFractionCRIter(Scenario,Iter,CRA,'FLoss','FeedWaste',FertNutr)=
         (SUM((APS,Level,HerdType)$(vNumHerdType.L(CRA,APS,Level,HerdType)>1),
          SUM((FeedCat,FeedProd)$AniFeedSuitability(APS,FeedCat,FeedProd),                                      vAniFeedIntakeHerdTypeMgDM.L(CRA,APS,Level,HerdType,FeedCat,FeedProd)        * AniFeedLossFrac(FeedCat,FeedLoss,FeedProd)  * (AniNutrByProduct(FeedCat,FeedProd,FertNutr) * 0.001))   +
          SUM((CR,FWCat,Waste)$(AniFeedSuitability(APS,FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)),          vAniFoodWasteIntakeHerdTypeMgDM.L(CR,CRA,APS,Level,HerdType,FWCat,Waste)     * AniFeedLossFrac(FWCat,FeedLoss,Waste)       * (AniNutrByProductCR(CR,FWCat,Waste,FertNutr) * 0.001)))  /
          SUM((APS,Level,HerdType,FertNutrA)$APSHerdType(APS,Level,HerdType),
          SUM((FeedCat,FeedProd)$AniFeedSuitability(APS,FeedCat,FeedProd),                                      vAniFeedIntakeHerdTypeMgDM.L(CRA,APS,Level,HerdType,FeedCat,FeedProd)        * AniFeedLossFrac(FeedCat,FeedLoss,FeedProd)  * (AniNutrByProduct(FeedCat,FeedProd,FertNutrA) * 0.001))   +
          SUM((CR,FWCat,Waste)$(AniFeedSuitability(APS,FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)),          vAniFoodWasteIntakeHerdTypeMgDM.L(CR,CRA,APS,Level,HerdType,FWCat,Waste)     * AniFeedLossFrac(FWCat,FeedLoss,Waste)       * (AniNutrByProductCR(CR,FWCat,Waste,FertNutrA) * 0.001))))
         $SUM((APS,Level,HerdType,FertNutrA)$APSHerdType(APS,Level,HerdType),
          SUM((FeedCat,FeedProd)$AniFeedSuitability(APS,FeedCat,FeedProd),                                      vAniFeedIntakeHerdTypeMgDM.L(CRA,APS,Level,HerdType,FeedCat,FeedProd)        * AniFeedLossFrac(FeedCat,FeedLoss,FeedProd)  * (AniNutrByProduct(FeedCat,FeedProd,FertNutrA) * 0.001))   +
          SUM((CR,FWCat,Waste)$(AniFeedSuitability(APS,FWCat,Waste) AND ImportBoundary(CR,CRA,FWCat)),          vAniFoodWasteIntakeHerdTypeMgDM.L(CR,CRA,APS,Level,HerdType,FWCat,Waste)     * AniFeedLossFrac(FWCat,FeedLoss,Waste)       * (AniNutrByProductCR(CR,FWCat,Waste,FertNutrA) * 0.001)))
;
* Calculate values at a national level
GrazingProportionCIter(Scenario,Iter,CountryA,Rumi,level,HerdType)=
                                                    ((SUM((RegionA,FreshGrass)$(CRA(CountryA,RegionA) AND AniFeedSuitability(Rumi,'GrassFresh',FreshGrass) AND (vNumHerdType.L(CountryA,RegionA,Rumi,Level,HerdType)>1)), vAniFeedIntakeHerdTypeMgDM.L(CountryA,RegionA,Rumi,level,HerdType,'GrassFresh',FreshGrass))      /
                                                     (SUM((RegionA,FeedCat,FeedProd)$(CRA(CountryA,RegionA) AND AniFeedSuitability(Rumi,FeedCat,FeedProd) AND (vNumHerdType.L(CountryA,RegionA,Rumi,Level,HerdType)>1)),  vAniFeedIntakeHerdTypeMgDM.L(CountryA,RegionA,Rumi,level,HerdType,FeedCat,FeedProd))             +
                                                      SUM((RegionA,CR,FWCat,Waste)$(CRA(CountryA,RegionA) AND AniFeedSuitability(Rumi,FWCat,Waste) AND (vNumHerdType.L(CountryA,RegionA,Rumi,Level,HerdType)>1)),         vAniFoodWasteIntakeHerdTypeMgDM.L(CR,CountryA,RegionA,Rumi,level,HerdType,FWCat,Waste)))))
                                                    $(SUM((RegionA,FeedCat,FeedProd)$(CRA(CountryA,RegionA) AND AniFeedSuitability(Rumi,FeedCat,FeedProd) AND (vNumHerdType.L(CountryA,RegionA,Rumi,Level,HerdType)>1)),  vAniFeedIntakeHerdTypeMgDM.L(CountryA,RegionA,Rumi,level,HerdType,FeedCat,FeedProd))             +
                                                      SUM((RegionA,CR,FWCat,Waste)$(CRA(CountryA,RegionA) AND AniFeedSuitability(Rumi,FWCat,Waste) AND (vNumHerdType.L(CountryA,RegionA,Rumi,Level,HerdType)>1)),         vAniFoodWasteIntakeHerdTypeMgDM.L(CR,CountryA,RegionA,Rumi,level,HerdType,FWCat,Waste)))
;
AniNutrByProductCIter(Scenario,Iter,Country,FWCat,Waste,AniNutr)$FeedCatAllProd(FWCat,Waste) =
         (SUM((Region,ProcOutH)$(CR(Country,Region) AND WasteTypeCatProd(FWCat,ProcOutH,Waste)), (vFoodWasteMg.L(Country,Region,FWCat,ProcOutH) * AniNutrByProduct('PPLoss',ProcOutH,AniNutr))) / SUM((Region,ProcOutH)$(CR(Country,Region) AND WasteTypeCatProd(FWCat,ProcOutH,Waste)),  vFoodWasteMg.L(Country,Region,FWCat,ProcOutH)))$SUM((Region,ProcOutH)$(CR(Country,Region) AND WasteTypeCatProd(FWCat,ProcOutH,Waste)),  vFoodWasteMg.L(Country,Region,FWCat,ProcOutH))
;
AniNutrByProductCIter(Scenario,Iter,Country,FWCat,Waste,'FM')$FeedCatAllProd(FWCat,Waste) = (1000 / (AniNutrByProductCIter(Scenario,Iter,Country,FWCat,Waste,'DM') * 0.001))$AniNutrByProductCIter(Scenario,Iter,Country,FWCat,Waste,'DM')
;
ManureTanCIter(Scenario,Iter,Country,'Manure',ManureProd)  =
         (SUM((Region,APSManure,Level,HerdType)$(CR(Country,Region) AND APSManureProd(ManureProd,APSManure) AND (vNumHerdType.L(Country,Region,APSManure,Level,HerdType)>1)), vAniTANExcMMMg.L(Country,Region,APSManure,Level,HerdType)) / SUM((Region,APSManure,Level,HerdType)$(CR(Country,Region) AND APSManureProd(ManureProd,APSManure) AND (vNumHerdType.L(Country,Region,APSManure,Level,HerdType)>1)), vAniManureMMMg.L(Country,Region,APSManure,Level,HerdType,'N')))$SUM((Region,APSManure,Level,HerdType)$(CR(Country,Region) AND APSManureProd(ManureProd,APSManure) AND (vNumHerdType.L(Country,Region,APSManure,Level,HerdType)>1)), vAniManureMMMg.L(Country,Region,APSManure,Level,HerdType,'N'))
;
ManureTanCIter(Scenario,Iter,Country,'GrazingManure',ManureProd)  =
         (SUM((Region,Rumi,Level,HerdType)$(CR(Country,Region) AND APSManureProd(ManureProd,Rumi) AND (vNumHerdType.L(Country,Region,Rumi,Level,HerdType)>1)), vAniTANExcGMg.L(Country,Region,Rumi,Level,HerdType)) /  SUM((Region,Rumi)$(CR(Country,Region) AND APSManureProd(ManureProd,Rumi)), vAniManureGMg.L(Country,Region,Rumi,'N')))$SUM((Region,Rumi)$(CR(Country,Region) AND APSManureProd(ManureProd,Rumi)), vAniManureGMg.L(Country,Region,Rumi,'N'))
;
NPKFractionCIter(Scenario,Iter,Country,'Manure',ManureProd,FertNutr) =
         (SUM((Region,APSManure)$(CR(Country,Region) AND APSManureProd(ManureProd,APSManure)),           SUM((Level,Herdtype)$(vNumHerdType.L(Country,Region,APSManure,Level,HerdType)>1), vAniManureMMMg.L(Country,Region,APSManure,Level,HerdType,FertNutr)  - vManureNLossesMg.L(Country,Region,APSManure,Level,HerdType,FertNutr))) /
          SUM((Region,APSManure,FertNutrA)$(CR(Country,Region) AND APSManureProd(ManureProd,APSManure)), SUM((Level,Herdtype)$(vNumHerdType.L(Country,Region,APSManure,Level,HerdType)>1), vAniManureMMMg.L(Country,Region,APSManure,Level,HerdType,FertNutrA) - vManureNLossesMg.L(Country,Region,APSManure,Level,HerdType,FertNutrA))))
         $SUM((Region,APSManure,FertNutrA)$(CR(Country,Region) AND APSManureProd(ManureProd,APSManure)), SUM((Level,Herdtype)$(vNumHerdType.L(Country,Region,APSManure,Level,HerdType)>1), vAniManureMMMg.L(Country,Region,APSManure,Level,HerdType,FertNutrA) - vManureNLossesMg.L(Country,Region,APSManure,Level,HerdType,FertNutrA)))
;
NPKFractionCIter(Scenario,Iter,Country,'GrazingManure',ManureProd,FertNutr) =
         (SUM((Region,Rumi)$(CR(Country,Region) AND APSManureProd(ManureProd,Rumi)),           vAniManureGMg.L(Country,Region,Rumi,FertNutr)) /
          SUM((Region,Rumi,FertNutrA)$(CR(Country,Region) AND APSManureProd(ManureProd,Rumi)), vAniManureGMg.L(Country,Region,Rumi,FertNutrA)))
         $SUM((Region,Rumi,FertNutrA)$(CR(Country,Region) AND APSManureProd(ManureProd,Rumi)), vAniManureGMg.L(Country,Region,Rumi,FertNutrA))
;
NPKFractionCIter(Scenario,Iter,Country,'Compost',Compost,FertNutr)=
         (SUM((Region)$CR(Country,Region),
         (SUM((FLCat,APWaste)$CompostTypeCatProd(FLCat,APWaste,Compost),                          vCompostFoodLossMg.L(Country,Region,FLCat,APWaste)                * (AniNutrByProduct(FLCat,APWaste,'DM')    * 0.001)             * (AniNutrByProduct(FLCat,APWaste,FertNutr)     * 0.001)) +
          SUM((FWCat,Waste)$CompostTypeCatProd(FWCat,Waste,Compost),                              vCompostFoodWasteMg.L(Country,Region,FWCat,Waste)                 * (AniNutrByProductCR(Country,Region,FWCat,Waste,'DM') * 0.001) * (AniNutrByProductCR(Country,Region,FWCat,Waste,FertNutr)  * 0.001)) +
          SUM((ProcOutCP)$CompostTypeCatProd('CoProduct',ProcOutCP,Compost),                      vCompostCoProductMg.L(Country,Region,'CoProduct',ProcOutCP)       * (AniNutrByProduct('CoProduct',ProcOutCP,'DM') * 0.001)        * (AniNutrByProduct('CoProduct',ProcOutCP,FertNutr) * 0.001))) * (1 - EFC(Country,Compost,FertNutr))) /
          SUM((Region,FertNutrA)$CR(Country,Region),
         (SUM((FLCat,APWaste)$CompostTypeCatProd(FLCat,APWaste,Compost),                          vCompostFoodLossMg.L(Country,Region,FLCat,APWaste)                * (AniNutrByProduct(FLCat,APWaste,'DM')    * 0.001)             * (AniNutrByProduct(FLCat,APWaste,FertNutrA)    * 0.001)) +
          SUM((FWCat,Waste)$CompostTypeCatProd(FWCat,Waste,Compost),                              vCompostFoodWasteMg.L(Country,Region,FWCat,Waste)                 * (AniNutrByProductCR(Country,Region,FWCat,Waste,'DM') * 0.001) * (AniNutrByProductCR(Country,Region,FWCat,Waste,FertNutrA) * 0.001)) +
          SUM((ProcOutCP)$CompostTypeCatProd('CoProduct',ProcOutCP,Compost),                      vCompostCoProductMg.L(Country,Region,'CoProduct',ProcOutCP)       * (AniNutrByProduct('CoProduct',ProcOutCP,'DM') * 0.001)        * (AniNutrByProduct('CoProduct',ProcOutCP,FertNutrA) * 0.001))) *  (1 - EFC(Country,Compost,FertNutrA))))
         $SUM((Region,FertNutrA)$CR(Country,Region),
         (SUM((FLCat,APWaste)$CompostTypeCatProd(FLCat,APWaste,Compost),                          vCompostFoodLossMg.L(Country,Region,FLCat,APWaste)                * (AniNutrByProduct(FLCat,APWaste,'DM')    * 0.001)             * (AniNutrByProduct(FLCat,APWaste,FertNutrA)    * 0.001)) +
          SUM((FWCat,Waste)$CompostTypeCatProd(FWCat,Waste,Compost),                              vCompostFoodWasteMg.L(Country,Region,FWCat,Waste)                 * (AniNutrByProductCR(Country,Region,FWCat,Waste,'DM') * 0.001) * (AniNutrByProductCR(Country,Region,FWCat,Waste,FertNutrA) * 0.001)) +
          SUM((ProcOutCP)$CompostTypeCatProd('CoProduct',ProcOutCP,Compost),                      vCompostCoProductMg.L(Country,Region,'CoProduct',ProcOutCP)       * (AniNutrByProduct('CoProduct',ProcOutCP,'DM') * 0.001)        * (AniNutrByProduct('CoProduct',ProcOutCP,FertNutrA) * 0.001)))* (1 - EFC(Country,Compost,FertNutrA)))
;
NPKFractionCIter(Scenario,Iter,CountryA,FeedLoss,FeedWaste,FertNutr)=
         (SUM((RegionA,APS,Level,HerdType)$(CRA(CountryA,RegionA) AND (vNumHerdType.L(CountryA,RegionA,APS,Level,HerdType)>1)),
          SUM((FeedCat,FeedProd)$AniFeedSuitability(APS,FeedCat,FeedProd),                                                   vAniFeedIntakeHerdTypeMgDM.L(CountryA,RegionA,APS,Level,HerdType,FeedCat,FeedProd)        * AniFeedLossFrac(FeedCat,FeedLoss,FeedProd)  * (AniNutrByProduct(FeedCat,FeedProd,FertNutr) * 0.001))   +
          SUM((CR,FWCat,Waste)$(AniFeedSuitability(APS,FWCat,Waste) AND ImportBoundary(CR,CountryA,RegionA,FWCat)),          vAniFoodWasteIntakeHerdTypeMgDM.L(CR,CountryA,RegionA,APS,Level,HerdType,FWCat,Waste)     * AniFeedLossFrac(FWCat,FeedLoss,Waste)       * (AniNutrByProductCR(CR,FWCat,Waste,FertNutr) * 0.001)))  /
          SUM((RegionA,APS,Level,HerdType,FertNutrA)$(CRA(CountryA,RegionA) AND (vNumHerdType.L(CountryA,RegionA,APS,Level,HerdType)>1)),
          SUM((FeedCat,FeedProd)$AniFeedSuitability(APS,FeedCat,FeedProd),                                                   vAniFeedIntakeHerdTypeMgDM.L(CountryA,RegionA,APS,Level,HerdType,FeedCat,FeedProd)        * AniFeedLossFrac(FeedCat,FeedLoss,FeedProd)  * (AniNutrByProduct(FeedCat,FeedProd,FertNutrA) * 0.001))  +
          SUM((CR,FWCat,Waste)$(AniFeedSuitability(APS,FWCat,Waste) AND ImportBoundary(CR,CountryA,RegionA,FWCat)),          vAniFoodWasteIntakeHerdTypeMgDM.L(CR,CountryA,RegionA,APS,Level,HerdType,FWCat,Waste)     * AniFeedLossFrac(FWCat,FeedLoss,Waste)       * (AniNutrByProductCR(CR,FWCat,Waste,FertNutrA) * 0.001))))
         $SUM((RegionA,APS,Level,HerdType,FertNutrA)$(CRA(CountryA,RegionA) AND (vNumHerdType.L(CountryA,RegionA,APS,Level,HerdType)>1)),
          SUM((FeedCat,FeedProd)$AniFeedSuitability(APS,FeedCat,FeedProd),                                                   vAniFeedIntakeHerdTypeMgDM.L(CountryA,RegionA,APS,Level,HerdType,FeedCat,FeedProd)        * AniFeedLossFrac(FeedCat,FeedLoss,FeedProd)  * (AniNutrByProduct(FeedCat,FeedProd,FertNutrA) * 0.001))  +
          SUM((CR,FWCat,Waste)$(AniFeedSuitability(APS,FWCat,Waste) AND ImportBoundary(CR,CountryA,RegionA,FWCat)),          vAniFoodWasteIntakeHerdTypeMgDM.L(CR,CountryA,RegionA,APS,Level,HerdType,FWCat,Waste)     * AniFeedLossFrac(FWCat,FeedLoss,Waste)       * (AniNutrByProductCR(CR,FWCat,Waste,FertNutrA) * 0.001)))
;
* Calculate grazing proportion averaged across levels
GrazingProportionAPSCIter(Scenario,Iter,CountryA,Rumi,HerdType) =
                                                    ((SUM((RegionA,Level,FreshGrass)$(CRA(CountryA,RegionA) AND APSHerdType(Rumi,Level,HerdType) AND AniFeedSuitability(Rumi,'GrassFresh',FreshGrass)),   vAniFeedIntakeHerdTypeMgDM.L(CountryA,RegionA,Rumi,level,HerdType,'GrassFresh',FreshGrass))      /
                                                     (SUM((RegionA,Level,FeedCat,FeedProd)$(CRA(CountryA,RegionA) AND APSHerdType(Rumi,Level,HerdType)  AND AniFeedSuitability(Rumi,FeedCat,FeedProd)),   vAniFeedIntakeHerdTypeMgDM.L(CountryA,RegionA,Rumi,level,HerdType,FeedCat,FeedProd))             +
                                                      SUM((RegionA,Level,CR,FWCat,Waste)$(CRA(CountryA,RegionA) AND APSHerdType(Rumi,Level,HerdType)  AND AniFeedSuitability(Rumi,FWCat,Waste)),          vAniFoodWasteIntakeHerdTypeMgDM.L(CR,CountryA,RegionA,Rumi,level,HerdType,FWCat,Waste)))))
                                                    $(SUM((RegionA,Level,FeedCat,FeedProd)$(CRA(CountryA,RegionA) AND APSHerdType(Rumi,Level,HerdType)  AND AniFeedSuitability(Rumi,FeedCat,FeedProd)),   vAniFeedIntakeHerdTypeMgDM.L(CountryA,RegionA,Rumi,level,HerdType,FeedCat,FeedProd))             +
                                                      SUM((RegionA,CR,Level,FWCat,Waste)$(CRA(CountryA,RegionA) AND APSHerdType(Rumi,Level,HerdType)  AND AniFeedSuitability(Rumi,FWCat,Waste)),          vAniFoodWasteIntakeHerdTypeMgDM.L(CR,CountryA,RegionA,Rumi,level,HerdType,FWCat,Waste)))
;
NPKFractionCRVar(Scenario,Iter,CR,FeedLoss,FeedWaste,FertNutr)          = abs(NPKFractionCRIter(Scenario,Iter,CR,FeedLoss,FeedWaste,FertNutr)          / NPKRatioCR(CR,FeedLoss,FeedWaste,FertNutr) - 1)          $(NPKRatioCR(CR,FeedLoss,FeedWaste,FertNutr));
NPKFractionCRVar(Scenario,Iter,CR,'Compost',Compost,FertNutr)           = abs(NPKFractionCRIter(Scenario,Iter,CR,'Compost',Compost,FertNutr)           / NPKRatioCR(CR,'Compost',Compost,FertNutr) - 1)           $(NPKRatioCR(CR,'Compost',Compost,FertNutr));
NPKFractionCRVar(Scenario,Iter,CR,'Manure',ManureProd,FertNutr)         = abs(NPKFractionCRIter(Scenario,Iter,CR,'Manure',ManureProd,FertNutr)         / NPKRatioCR(CR,'Manure',ManureProd,FertNutr) - 1)         $(NPKRatioCR(CR,'Manure',ManureProd,FertNutr));
NPKFractionCRVar(Scenario,Iter,CR,'GrazingManure',ManureProd,FertNutr)  = abs(NPKFractionCRIter(Scenario,Iter,CR,'GrazingManure',ManureProd,FertNutr)  / NPKRatioCR(CR,'GrazingManure',ManureProd,FertNutr) - 1)  $(NPKRatioCR(CR,'GrazingManure',ManureProd,FertNutr));
ManureTanCRVar(Scenario,Iter,CR,'Manure',ManureProd)                    = abs(ManureTanCRIter(Scenario,Iter,CR,'Manure',ManureProd)                    / ManureTanCR(CR,'Manure',ManureProd) - 1)                 $(ManureTanCR(CR,'Manure',ManureProd));
ManureTanCRVar(Scenario,Iter,CR,'GrazingManure',ManureProd)             = abs(ManureTanCRIter(Scenario,Iter,CR,'GrazingManure',ManureProd)             / ManureTanCR(CR,'GrazingManure',ManureProd) - 1)          $(ManureTanCR(CR,'GrazingManure',ManureProd));
GrazingProportionVar(Scenario,Iter,CR,Rumi,level,HerdType)              = abs(GrazingProportionCRIter(Scenario,Iter,CR,Rumi,level,HerdType)            / GrazingProportionCR(CR,Rumi,level,HerdType) - 1)         $(GrazingProportionCR(CR,Rumi,level,HerdType));
AniNutrByProductCRVar(Scenario,Iter,CR,FWCat,Waste,AniNutr)             = abs(AniNutrByProductCRIter(Scenario,Iter,CR,FWCat,Waste,AniNutr)             / AniNutrByProductCR(CR,FWCat,Waste,AniNutr) - 1)          $(AniNutrByProductCR(CR,FWCat,Waste,AniNutr) > EPS);
;
DeltaTotal(Scenario,Iter) =
          SUM((CR,FeedLoss,FeedWaste,FertNutr) $(NPKFractionCRVar(Scenario,Iter,CR,FeedLoss,FeedWaste,FertNutr))  ,1) +
          SUM((CR,Compost,FertNutr)            $(NPKFractionCRVar(Scenario,Iter,CR,'Compost',Compost,FertNutr))   ,1) +
          SUM((CR,ManureProd,FertNutr)         $(NPKFractionCRVar(Scenario,Iter,CR,'Manure',ManureProd,FertNutr)) ,1) +
          SUM((CR,ManureProd)                  $(ManureTanCRVar(Scenario,Iter,CR,'Manure',ManureProd))            ,1) +
          SUM((CR,ManureProd)                  $(ManureTanCRVar(Scenario,Iter,CR,'GrazingManure',ManureProd))     ,1) +
          SUM((CR,FWCat,Waste,AniNutr)         $(AniNutrByProductCRVar(Scenario,Iter,CR,FWCat,Waste,AniNutr))     ,1) +
          SUM((CR,Rumi,level,HerdType)         $(GrazingProportionVar(Scenario,Iter,CR,Rumi,level,HerdType))      ,1);
DeltaVariation(Scenario,Iter) =
          SUM((CR,FeedLoss,FeedWaste,FertNutr) $(NPKFractionCRVar(Scenario,Iter,CR,FeedLoss,FeedWaste,FertNutr)   > DeltaEPS),1) +
          SUM((CR,Compost,FertNutr)            $(NPKFractionCRVar(Scenario,Iter,CR,'Compost',Compost,FertNutr)    > DeltaEPS),1) +
          SUM((CR,ManureProd,FertNutr)         $(NPKFractionCRVar(Scenario,Iter,CR,'Manure',ManureProd,FertNutr)  > DeltaEPS),1) +
          SUM((CR,ManureProd)                  $(ManureTanCRVar(Scenario,Iter,CR,'Manure',ManureProd)             > DeltaEPS),1) +
          SUM((CR,ManureProd)                  $(ManureTanCRVar(Scenario,Iter,CR,'GrazingManure',ManureProd)      > DeltaEPS),1) +
          SUM((CR,FWCat,Waste,AniNutr)         $(AniNutrByProductCRVar(Scenario,Iter,CR,FWCat,Waste,AniNutr)      > DeltaEPS),1) +
          SUM((CR,Rumi,level,HerdType)         $(GrazingProportionVar(Scenario,Iter,CR,Rumi,level,HerdType)       > DeltaEPS),1);
;
Delta(Scenario,Iter) = (DeltaVariation(Scenario,Iter) / DeltaTotal(Scenario,Iter))$DeltaTotal(Scenario,Iter);
;
if(((CurrentIter > 10 AND Delta(Scenario,Iter) > Delta(Scenario,Iter-1)) AND (CFSNutrBal.solvestat = 1    OR
                                                                              CFS.solvestat = 1           OR
                                                                              CFSReference.solvestat = 1)), break)
;
*Update values at a regional level
AniNutrByProductCR(CR,FWCat,Waste,AniNutr)                $AniNutrByProductCRIter(Scenario,Iter,CR,FWCat,Waste,AniNutr)                                                                                          = round(AniNutrByProductCRIter(Scenario,Iter,CR,FWCat,Waste,AniNutr),5);
GrazingProportionCR(CR,Rumi,level,HerdType)               $((GrazingProportionCRIter(Scenario,Iter,CR,Rumi,level,HerdType)    <= 1) AND (GrazingProportionCRIter(Scenario,Iter,CR,Rumi,level,HerdType)    >= 0)) = round(GrazingProportionCRIter(Scenario,Iter,CR,Rumi,level,HerdType),5);
ManureTanCR(CR,FertOthCat,ManureProd)                     $((ManureTanCRIter(Scenario,Iter,CR,FertOthCat,ManureProd)          <= 1) AND (ManureTanCRIter(Scenario,Iter,CR,FertOthCat,ManureProd)          >= 0)) = round(ManureTanCRIter(Scenario,Iter,CR,FertOthCat,ManureProd),5);
ManureTanCR(CR,FertManCat,ManureProd)                     $((ManureTanCRIter(Scenario,Iter,CR,FertManCat,ManureProd)          <= 1) AND (ManureTanCRIter(Scenario,Iter,CR,FertManCat,ManureProd)          >= 0)) = round(ManureTanCRIter(Scenario,Iter,CR,FertManCat,ManureProd),5);
NPKRatioCR(CR,FertOthCat,FertProd,FertNutr)               $((NPKFractionCRIter(Scenario,Iter,CR,FertOthCat,FertProd,FertNutr) <= 1) AND (NPKFractionCRIter(Scenario,Iter,CR,FertOthCat,FertProd,FertNutr) >= 0)) = round(NPKFractionCRIter(Scenario,Iter,CR,FertOthCat,FertProd,FertNutr),5);
NPKRatioCR(CR,FertManCat,FertProd,FertNutr)               $((NPKFractionCRIter(Scenario,Iter,CR,FertManCat,FertProd,FertNutr) <= 1) AND (NPKFractionCRIter(Scenario,Iter,CR,FertManCat,FertProd,FertNutr) >= 0)) = round(NPKFractionCRIter(Scenario,Iter,CR,FertManCat,FertProd,FertNutr),5);
*Update values in regions with national average when no regional values exist
GrazingProportionCR(Country,Region,Rumi,level,HerdType)   $(not ((GrazingProportionCRIter(Scenario,Iter,Country,Region,Rumi,level,HerdType)    <= 1) AND (GrazingProportionCRIter(Scenario,Iter,Country,Region,Rumi,level,HerdType)    > 0))) = round(GrazingProportionCIter(Scenario,Iter,Country,Rumi,level,HerdType),5)   $((GrazingProportionCIter(Scenario,Iter,Country,Rumi,level,HerdType)    < 1) AND (GrazingProportionCIter(Scenario,Iter,Country,Rumi,level,HerdType)    > 0));
ManureTanCR(Country,Region,FertOthCat,ManureProd)         $(not ((ManureTanCRIter(Scenario,Iter,Country,Region,FertOthCat,ManureProd)          <= 1) AND (ManureTanCRIter(Scenario,Iter,Country,Region,FertOthCat,ManureProd)          > 0))) = round(ManureTanCIter(Scenario,Iter,Country,FertOthCat,ManureProd),5)         $((ManureTanCIter(Scenario,Iter,Country,FertOthCat,ManureProd)          < 1) AND (ManureTanCIter(Scenario,Iter,Country,FertOthCat,ManureProd)          > 0));
ManureTanCR(Country,Region,FertManCat,ManureProd)         $(not ((ManureTanCRIter(Scenario,Iter,Country,Region,FertManCat,ManureProd)          <= 1) AND (ManureTanCRIter(Scenario,Iter,Country,Region,FertManCat,ManureProd)          > 0))) = round(ManureTanCIter(Scenario,Iter,Country,FertManCat,ManureProd),5)         $((ManureTanCIter(Scenario,Iter,Country,FertManCat,ManureProd)          < 1) AND (ManureTanCIter(Scenario,Iter,Country,FertManCat,ManureProd)          > 0));
NPKRatioCR(Country,Region,FertOthCat,FertProd,FertNutr)   $(not ((NPKFractionCRIter(Scenario,Iter,Country,Region,FertOthCat,FertProd,FertNutr) <= 1) AND (NPKFractionCRIter(Scenario,Iter,Country,Region,FertOthCat,FertProd,FertNutr) > 0))) = round(NPKFractionCIter(Scenario,Iter,Country,FertOthCat,FertProd,FertNutr),5)$((NPKFractionCIter(Scenario,Iter,Country,FertOthCat,FertProd,FertNutr) < 1) AND (NPKFractionCIter(Scenario,Iter,Country,FertOthCat,FertProd,FertNutr) > 0));
NPKRatioCR(Country,Region,FertManCat,FertProd,FertNutr)   $(not ((NPKFractionCRIter(Scenario,Iter,Country,Region,FertManCat,FertProd,FertNutr) <= 1) AND (NPKFractionCRIter(Scenario,Iter,Country,Region,FertManCat,FertProd,FertNutr) > 0))) = round(NPKFractionCIter(Scenario,Iter,Country,FertManCat,FertProd,FertNutr),5)$((NPKFractionCIter(Scenario,Iter,Country,FertManCat,FertProd,FertNutr) < 1) AND (NPKFractionCIter(Scenario,Iter,Country,FertManCat,FertProd,FertNutr) > 0));
*Reset missing grazing proprotion to default
GrazingProportionCR(CR,Rumi,level,HerdType)               $((GrazingProportionCR(CR,Rumi,level,HerdType)=0) AND APSHerdType(Rumi,Level,HerdType)) = (ScenarioControlsModel('MinGrazingProportion')  + ScenarioControlsModel('MaxGrazingProportion')) / 2;
GrazingProportionCR(CR,'Dairy',level,'VealCalf')          = 0;
;
);
);
);


**** Animal nutrient data ****

$OnEcho > afile.txt
textID="Number of animals per aps, level, herdtype"                                                                rng=AniNumbers!A1
par=NumHerdTypeCReport                                                                                             rng=AniNumbers!A3             cdim=0
textID="DM intake of each animal (APS, level, herd type) in kg ingredient per day"                                 rng=AniIntakeKgDMDay!A1
par=AniIntakeKgDMDay                                                                                                 rng=AniIntakeKgDMDay!A3         cdim=0
textID="Total DM intake of each animal (APS, level, herd type) in kg per day"                                      rng=AniIntakeTotalkgDMDay!A1
par=AniIntakeTotalkgDMDay                                                                                            rng=AniIntakeTotalkgDMDay!A3    cdim=0
textID="DM intake of each animal (APS, level, herd type() in kg per feed cat per day"                              rng=AniIntakeFeedCatDMDay!A1
par=AniIntakeFeedCatDMDay                                                                                          rng=AniIntakeFeedCatDMDay!A3  cdim=0
textID="Total nutrient intake of each animal (APS, level, herd type) per day (various units)"                      rng=AniNutrIntakeDay!A1
par=AniNutrIntakeDay                                                                                               rng=AniNutrIntakeDay!A3       cdim=0
textID="Nutrient intake of each animal (APS, level, herd type) per ingredient per day (various units)"             rng=AniNutrIntakeProdDay!A1
par=AniNutrIntakeProdDay                                                                                           rng=AniNutrIntakeProdDay!A3   cdim=0
textID="Human food intake in grams per human capita day (Annual average)"                                          rng=HumIntakeAnnual!A1
par=DailyIntakeAnnualReport                                                                                        rng=HumIntakeAnnual!A3        cdim=0
textID="ASF (animal-sourced food) intake nutrients per human capita day (Annual average, various units)"           rng=HumNutrIntakeASF!A1
par=NutrIntakeASF                                                                                                  rng=HumNutrIntakeASF!A3       cdim=0
textID="Total nutrient intake per human capita day (Annual average, various units)"                                rng=HumIntakeAnnual!A1
par=NutrIntake                                                                                                     rng=HumNutrIntake!A3          cdim=1
textID="Animal nutrients in feed product per kg DM (excluding food waste mixes)"                                   rng=AniNutrByProduct!A1
par=AniNutrByProduct                                                                                               rng=AniNutrByProduct!A3       cdim=1
textID="Animal nutrients in food waste mixes per kg DM"                                                            rng=AniNutrByWaste!A1
par=AniNutrByFWC                                                                                                   rng=AniNutrByWaste!A3         cdim=1
textID="Total feed consumed in kg DM per year"                                                                     rng=AniFeedConsumedMgDM!A1
par=AniFeedConsumedMgDM                                                                                              rng=AniFeedConsumedMgDM!A3      cdim=0
textID="Total feed available in kg DM per year (note not all goes to feed e.g., food waste can also be composted)" rng=FeedAvailableMgDM!A1
par=FeedAvailableMgDM                                                                                                rng=FeedAvailableMgDM!A3        cdim=0
textID="Percentage of each feed category going to feed of fertiliser"                                              rng=ProductUsePerc!A1
par=ProductUsePercN0                                                                                               rng=ProductUsePerc!A3         cdim=1
textID="Surplus nutrients i.e. the amount of nutrients fed above requirements. 1 is equal to requiremnts. Ignore SW"   rng=NutrientSurplus!A1
par=AniNutrSurplus                                                                                                 rng=NutrientSurplus!A3        cdim=0
$OffEcho

Execute_Unload 'GAMS_AniData_FOODSOM.gdx'
NumHerdTypeCReport,AniIntakeKgDMDay,AniIntakeTotalkgDMDay,AniIntakeFeedCatDMDay,AniNutrIntakeDay,AniNutrIntakeProdDay,DailyIntakeAnnualReport,AniNutrByProduct,
AniFeedConsumedMgDM,FeedAvailableMgDM,ProductUsePercN0,AniNutrByFWC,NutrIntakeASF,NutrIntake,AniNutrSurplus
;

Execute 'gdxxrw.exe GAMS_AniData_FOODSOM.gdx Output=GAMS_AniData_FOODSOM @afile.txt';

**** Ben's scale paper figures ****

Execute_Unload 'GAMS_Figures_FOODSOM.gdx'
ReportingAnimal,ReportingLUFamily,ReportingExport,ReportingDiet,ReportingLU,ReportingGHG,ReportingTradeNPK,NutrIntakePercReq,ReferenceImportExportFamMg,ReferenceImportExportNPKMg,EmissionResults
;
**** General model results ****

$OnEcho > ofile.txt
textID="Scenario summary (GHG kg CO2e per capita & LU ha per Capita)"                                            rng=ScenarioResults!A1
par=ScenarioResults                                                                                              rng=ScenarioResults!A3
textID="Human food intake grams per day (Month)"                                                                 rng=DailyIntakeMonth!A1
par=DailyIntake                                                                                                  rng=DailyIntakeMonth!A3    rdim=2
textID="Human food intake grams per day per family"                                                              rng=DailyIntakeFam!A1
par=DailyIntakeFamily                                                                                            rng=DailyIntakeFam!A3      cdim=0
textID="Human food intake grams per day (Annual)"                                                                rng=DailyIntakeAnnual!A1
par=DailyIntakeAnnual                                                                                            rng=DailyIntakeAnnual!A3   rdim=2
textID="Human nutrient intake per day (various units)"                                                           rng=NutrIntakeAnnual!A1
par=NutrIntake                                                                                                   rng=NutrIntakeAnnual!A3    rdim=2
textID="Human nutrient ASF/PSF percentage"                                                                       rng=NutrIntakePerc!A1
par=NutrIntakePerc                                                                                               rng=NutrIntakePerc!A3      rdim=2
textID="Human nutrient intake per food family per day (various units)"                                           rng=NutrIntakeFam!A1
par=NutrIntakeFam                                                                                                rng=NutrIntakeFam!A3       rdim=2
textID="Human nutrient percentage per food family"                                                               rng=NutrIntakeFamPerc!A1
par=NutrIntakeFamPerc                                                                                            rng=NutrIntakeFamPerc!A3   rdim=2
textID="Hectares of land, soil and crop combinations per region"                                                 rng=LandSoilCrophaR!A1
par=LandSoilCrophaR                                                                                              rng=LandSoilCrophaR!A3     cdim=3
textID="Hectares of crop per country"                                                                            rng=CrophaCountry!A1
par=CrophaC                                                                                                      rng=CrophaCountry!A3       cdim=2
textID="Hectares of crop per region"                                                                             rng=CrophaRegion!A1
par=CrophaR                                                                                                      rng=CrophaRegion!A3        cdim=3
textID="Hectares of crop per country and family"                                                                 rng=CrophaCountryFam!A1
par=CrophaCFam                                                                                                   rng=CrophaCountryFam!A3    cdim=0
textID="Hectares of land type per region"                                                                        rng=LandTypehaRegion!A1
par=LandhaR                                                                                                      rng=LandTypehaRegion!A3    cdim=3
textID="Hectares of land type per country"                                                                       rng=LandTypehaCountry!A1
par=LandhaC                                                                                                      rng=LandTypehaCountry!A3   cdim=2
textID="Number of animals per APS, level, herdtype per country"                                                  rng=NumHerdTypeCountry!A1
par=NumHerdTypeC                                                                                                 rng=NumHerdTypeCountry!A3  cdim=2
textID="Number of animals per APS, level, herdtype per region"                                                   rng=NumHerdTypeRegion!A1
par=NumHerdTypeR                                                                                                 rng=NumHerdTypeRegion!A3   cdim=3
textID="Number of producer animals per APS, level per country"                                                   rng=NumProducerCountry!A1
par=NumProducerC                                                                                                 rng=NumProducerCountry!A3  cdim=2
textID="Number of producer animals per APS, level per region"                                                    rng=NumProducerRegion!A1
par=NumProducerR                                                                                                 rng=NumProducerRegion!A3   cdim=3
textID="Number of APS per country"                                                                               rng=NumAPSCountry!A1
par=NumAPSC                                                                                                      rng=NumAPSCountry!A3       cdim=0
textID="NPK applied per product (e.g. Perecntage of total N applied) of each land, soil, crop combination"       rng=NPK_LSC_Prod_%!A1
par=NutrAppliedLandSoilCropProdPercN0                                                                            rng=NPK_LSC_Prod_%!A3
textID="NPK applied per category (e.g. Perecntage of total N applied) of each land, soil, crop combination"      rng=NPK_LSC_Cat_%!A1
par=NutrAppliedLandSoilCropCatPercN0                                                                             rng=NPK_LSC_Cat_%!A3       cdim=1
textID="NPK applied per product (e.g. Perecntage of total N applied) of each crop"                               rng=NPK_LC_Prod_%!A1
par=NutrAppliedCropProdPercN0                                                                                    rng=NPK_LC_Prod_%!A3       cdim=1
textID="NPK applied per category (e.g. Perecntage of total N applied) of each crop"                              rng=NPK_LC_Cat_%!A1
par=NutrAppliedCropCatPercN0                                                                                     rng=NPK_LC_Cat_%!A3        cdim=1
textID="NPK applied per product (e.g. Perecntage of total N applied) applied to land"                            rng=NPK_L_Prod_%!A1
par=NutrAppliedProdPercN0                                                                                        rng=NPK_L_Prod_%!A3        cdim=1
textID="NPK applied per category (e.g. Perecntage of total N applied) applied to land"                           rng=NPK_L_Cat_%!A1
par=NutrAppliedCatPercN0                                                                                         rng=NPK_L_Cat_%!A3         cdim=1
textID="Animal intake per product of each APS, level, herdtype in kg DM per day"                                 rng=Intake_ALH_Day!A1
par=IntakeAPSLevelHerdTypekgDMDay                                                                                rng=Intake_ALH_Day!A3      cdim=3
textID="Animal intake per product of each APS, level, herdtype in percentage"                                    rng=Intake_ALH_Prod_%!A1
par=IntakeAPSLevelHerdTypeDMProdPercN0                                                                           rng=Intake_ALH_Prod_%!A3   cdim=1
textID="Animal intake per category of each APS, level, herdtype in percentage"                                   rng=Intake_ALH_Cat_%!A1
par=IntakeAPSLevelHerdTypeDMCatPercN0                                                                            rng=Intake_ALH_Cat_%!A3    cdim=1
textID="Animal intake per product of each APS, level in percentage"                                              rng=Intake_AL_Prod_%!A1
par=IntakeAPSLevelDMProdPercN0                                                                                   rng=Intake_AL_Prod_%!A3    cdim=1
textID="Animal intake per category of each APS, level in percentage"                                             rng=Intake_AL_Cat_%!A1
par=IntakeAPSLevelDMCatPercN0                                                                                    rng=Intake_AL_Cat_%!A3     cdim=1
textID="Animal intake per product of each APS in percentage"                                                     rng=Intake_A_Prod_%!A1
par=IntakeAPSDMProdPercN0                                                                                        rng=Intake_A_Prod_%!A3     cdim=1
textID="Animal intake per category of each APS in percentage"                                                    rng=Intake_A_Cat_%!A1
par=IntakeAPSDMCatPercN0                                                                                         rng=Intake_A_Cat_%!A3      cdim=1
textID="Product destination (e.g. feed or fertiliser per category"                                               rng=ProdUse_Cat_%!A1
par=ProductUsePercN0                                                                                             rng=ProdUse_Cat_%!A3       cdim=1
textID="Animal emission sources kg CO2e per capita"                                                              rng=AniCO2eqCapita!A1
par=AnimalCO2eqCapita                                                                                            rng=AniCO2eqCapita!A3      cdim=1
textID="Crop emission sources kg CO2e per capita"                                                                rng=CropCO2eqCapita!A1
par=CroplandEmissionsCapita                                                                                      rng=CropCO2eqCapita!A3     cdim=1
textID="Quanity of processed food imported and exported"                                                         rng=ProcOutHImportExportMg!A1
par=ImportExportProcOutHMg                                                                                         rng=ProcOutHImportExportMg!A3    cdim=2
par=ImportExportFamMg                                                                                              rng=FamFoodImportExportkg!A3 cdim=0
$OffEcho

Execute_Unload 'GAMS_Results_FOODSOM.gdx'
ScenarioResults,DailyIntake,DailyIntakeAnnual,NutrIntake,NutrIntakePerc,NutrIntakeFam,NutrIntakeFamPerc,DailyIntakeFamily
LandSoilCrophaR,CrophaC,CrophaR,CrophaCFam,LandhaR,LandhaC,NumHerdTypeC,NumHerdTypeR,NumProducerC,NumProducerR
NutrAppliedLandSoilCropProdPercN0,NutrAppliedLandSoilCropCatPercN0,NutrAppliedCropProdPercN0,NutrAppliedCropCatPercN0
NutrAppliedProdPercN0,NutrAppliedCatPercN0,IntakeAPSLevelHerdTypekgDMDay,IntakeAPSLevelHerdTypeDMProdPercN0,IntakeAPSLevelHerdTypeDMCatPercN0
IntakeAPSLevelDMProdPercN0,IntakeAPSLevelDMCatPercN0,IntakeAPSDMProdPercN0,IntakeAPSDMCatPercN0,ProductUsePercN0,AnimalCO2eqCapita,CroplandEmissionsCapita
ImportExportProcInMg,ImportExportProcOutHMg,ImportExportFamMg,NumAPSC,NutrIntakePercReq,CropFertBalanceReport,EmissionResults
;
Execute 'gdxxrw.exe GAMS_Results_FOODSOM.gdx Output=GAMS_Results_FOODSOM @ofile.txt';

**** Iteration report ****

Execute_Unload 'GAMS_IterationReport_FOODSOM.gdx'
NPKFractionCRVar,ManureTanCRVar,GrazingProportionVar,AniNutrByProductCRVar
;
