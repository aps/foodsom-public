# FOODSOM: FOOD System Optimisation Model

This version of FOODSOM was used in the article: [Interventions to increase circularity and reduce environmental impacts in food systems](https://10.1007/s13280-023-01953-x)

Benjamin van Selm<sup>1,2</sup>, Hannah H.E. van Zanten<sup>3</sup>, Renske Hijbeek<sup>2</sup>, Corina E. van Middelaar<sup>1</sup>, Marijke Schop<sup>1</sup>, Martin K. van Ittersum<sup>2</sup>, Imke J.M. de Boer<sup>1</sup>

<sup>1</sup>Animal Production Systems group, Wageningen University & Research, P.O. Box 338, 6700 AH Wageningen, the Netherlands.

<sup>2</sup>Plant Production Systems group, Wageningen University & Research, P.O. Box 430, 6700 AK Wageningen, the Netherlands.

<sup>3</sup>Farming Systems Ecology group, Wageningen University & Research, P.O. Box 430, 6700 AK Wageningen, the Netherlands.

Corresponding author: Benjamin van Selm, ben.vanselm@wur.nl 

# Short model description

FOODSOM is an iterative linear optimisation model developed in GAMS to minimise environmental impacts while meeting the nutritional requirements of the Dutch population, which we will explain below. The model is not limited to the Netherlands, with the appropriate data it can be run for any country(ies) or region(s). 

Agricultural land and marine fisheries form the basis of FOODSOM. Within the model 1.7 million hectares of agricultural land, 49 representative crops (one productivity level, based on current management) can be grown to produce food for humans and feed for animals, while marine fisheries only provide a source of food for humans. To make some of these crops and marine fish suitable for human consumption, processing (e.g., wheat into flour) is required, which produces co-products (e.g., wheat bran) to be used as animal feed or to be returned to the soil as source of nutrients. Moreover, food is lost (e.g., during storage or during processing) and food is wasted (e.g., during consumption or in the supermarket). These losses and wastes are again suitable for animal feed or can be used as a source of nutrients for the soil (e.g., compost). Five animal systems (dairy, beef, broiler chickens, laying hens, pigs, three productivity levels) can consume a selected number of crops, co-products and food losses/waste. Following, food suitable for human consumption is produced, together with animal manure to fertilise crops, and co-products (e.g., blood and bone meal) to be fed back to animals or applied to the soil as fertiliser. Food from crops and animals is consumed by humans to satisfy their nutritional requirements (27 nutrients). In addition, food and raw materials can be traded in scenarios that allow imports and exports, provided, nitrogen, phosphorus and potassium in imports equal nitrogen, phosphorus and potassium in exports. Balancing nitrogen, phosphorus and potassium prevents nutrient depletions or surpluses occurring between the Netherlands and the global food system.

More information on running FOODSOM can be found in the **Wiki**. 

This software is protected by GNU GENERAL PUBLIC LICENSE version 3. 

